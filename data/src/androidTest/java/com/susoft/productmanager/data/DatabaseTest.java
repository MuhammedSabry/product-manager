package com.susoft.productmanager.data;

import android.content.Context;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.room.Room;
import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

@RunWith(AndroidJUnit4.class)
public class DatabaseTest {
    private SusoftDatabase mDb;
//    private ProductDao productDao;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext().getApplicationContext();
        mDb = Room.inMemoryDatabaseBuilder(context, SusoftDatabase.class).build();
//        productDao = mDb.productDao();
    }

    @After
    public void closeDb() {
        mDb.close();
    }

    @Test
    public void writeUserAndReadInList() {
//        productDao.getAllProducts().subscribe((products) -> {
//            System.out.println(products);
//            Assert.assertNotNull(products);
//        });
//        Assert.assertNotNull(new Product());
    }

}
