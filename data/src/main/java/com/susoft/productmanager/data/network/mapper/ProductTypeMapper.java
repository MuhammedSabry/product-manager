package com.susoft.productmanager.data.network.mapper;

import no.susoft.globalone.api.client.data.product.ProductType;

public class ProductTypeMapper extends BaseNetworkMapper<com.susoft.productmanager.domain.model.ProductType, ProductType> {

    @Override
    public com.susoft.productmanager.domain.model.ProductType fromNetwork(ProductType serverModel) {
        return new com.susoft.productmanager.domain.model.ProductType(serverModel.getId(), serverModel.getNames());
    }

    @Override
    public ProductType toNetwork(com.susoft.productmanager.domain.model.ProductType model) {
        return null;
    }
}
