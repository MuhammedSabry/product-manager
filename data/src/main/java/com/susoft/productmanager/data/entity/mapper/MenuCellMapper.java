package com.susoft.productmanager.data.entity.mapper;

import com.susoft.productmanager.data.entity.Cell;

/**
 * @author Muhammed Sabry
 */
public class MenuCellMapper {

    private com.susoft.productmanager.domain.model.Cell domainCell;

    private Cell entityCell;

    public MenuCellMapper(com.susoft.productmanager.domain.model.Cell domainCell) {
        this.domainCell = domainCell;
        extractCell();
    }

    private void extractCell() {
//        entityCell = new Cell();
//        entityCell.setName(domainCell.getText());
//        entityCell.setImage(domainCell.getImage());
    }

    public Cell getEntityCell() {
        return entityCell;
    }
}
