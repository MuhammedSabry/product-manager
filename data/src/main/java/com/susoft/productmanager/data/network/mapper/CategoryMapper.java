package com.susoft.productmanager.data.network.mapper;

import com.susoft.productmanager.domain.model.Category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.susoft.globalone.api.client.data.product.ProductCategory;

/**
 * @author Muhammed Sabry
 */
public class CategoryMapper extends BaseNetworkMapper<Category, ProductCategory> {

    public Category fromNetwork(no.susoft.globalone.api.client.data.product.ProductCategory serverCategory) {

        Category domainCategory = new Category();

        domainCategory.setServerId(serverCategory.getId());
        domainCategory.setLevel(serverCategory.getLevel());
        domainCategory.setValue(serverCategory.getName());

        domainCategory.setChildren(fromNetwork(new ArrayList<>(serverCategory.getChilds().values())));

        return domainCategory;
    }

    @Override
    public ProductCategory toNetwork(Category model) {
        ProductCategory serverCategory = new ProductCategory();
        serverCategory.setId(String.valueOf(model.getId()));
        serverCategory.setLevel(model.getLevel());
        serverCategory.setName(model.getValue());
        if (!model.getChildren().isEmpty()) {

            List<Category> modelChildren = model.getChildren();

            Map<String, ProductCategory> serverChildren = new HashMap<>();

            for (Category category : modelChildren)
                serverChildren.put(category.getServerId(), toNetwork(category));

            serverCategory.setChilds(serverChildren);
        }
        return serverCategory;
    }
}
