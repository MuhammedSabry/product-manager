package com.susoft.productmanager.data.entity;

import com.susoft.productmanager.domain.DBConstants;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * @author Muhammed Sabry
 */
@Entity(tableName = DBConstants.ProductImage.TABLE)
public class ProductImage {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = DBConstants.ProductImage.ID)
    private int id;

    @ColumnInfo(name = DBConstants.ProductImage.IS_DEFAULT)
    private boolean isDefault;

    @ColumnInfo(name = DBConstants.ProductImage.IMAGE_BYTES)
    private byte[] image;

    @ColumnInfo(name = DBConstants.ProductImage.SERVER_ID)
    private String serverId;

    @ColumnInfo(name = DBConstants.ProductImage.MIME)
    private String mime;

    @ColumnInfo(name = DBConstants.ProductImage.SERVER_TABLE)
    private String table;

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }
}
