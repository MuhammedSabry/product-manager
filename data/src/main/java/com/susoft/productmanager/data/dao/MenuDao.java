package com.susoft.productmanager.data.dao;

import com.susoft.productmanager.domain.DBConstants;
import com.susoft.productmanager.domain.model.Menu;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Query;
import io.reactivex.Flowable;

/**
 * @author Muhammed Sabry
 */
@Dao
public abstract class MenuDao extends BaseDao<com.susoft.productmanager.data.entity.Menu> {

    @Query("SELECT *" +
            " FROM " + DBConstants.Menu.TABLE +
            ";")
    public abstract Flowable<List<Menu>> getAll();

    @Query("SELECT *" +
            " FROM " + DBConstants.Menu.TABLE +
            " ORDER BY " + DBConstants.Menu.ID +
            " DESC" +
            " LIMIT 1;")
    public abstract Flowable<Menu> getLastMenu();
}
