package com.susoft.productmanager.data.entity;

import com.susoft.productmanager.domain.DBConstants;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * @author Muhammed Sabry
 */
@Entity(tableName = DBConstants.Menu.TABLE)
public class Menu {

    @PrimaryKey
    @ColumnInfo(name = DBConstants.Menu.ID)
    private long id;

    @ColumnInfo(name = DBConstants.Menu.NAME)
    private String name;

    @ColumnInfo(name = DBConstants.Menu.GRID_ID)
    private long gridId;

    public Menu(long id, String name, long gridId) {
        this.id = id;
        this.name = name;
        this.gridId = gridId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getGridId() {
        return gridId;
    }

    public void setGridId(long gridId) {
        this.gridId = gridId;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof Menu)
            return ((Menu) obj).id == this.id
                    && ((Menu) obj).gridId == this.gridId;
        return false;
    }
}
