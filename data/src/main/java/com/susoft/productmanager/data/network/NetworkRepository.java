package com.susoft.productmanager.data.network;

import com.susoft.productmanager.domain.model.Cell;
import com.susoft.productmanager.domain.service.AuthenticationService;
import com.susoft.productmanager.domain.service.CategoryService;
import com.susoft.productmanager.domain.service.CellService;
import com.susoft.productmanager.domain.service.GridService;
import com.susoft.productmanager.domain.service.MenuService;
import com.susoft.productmanager.domain.service.ProductService;
import com.susoft.productmanager.domain.service.UserService;

import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public abstract class NetworkRepository implements AuthenticationService,
        ProductService,
        CategoryService,
        CellService,
        GridService,
        MenuService,
        UserService {

    @Override
    public Single<Cell> getCell(long cellId) {
        return getCell(cellId, true);
    }
}
