package com.susoft.productmanager.data.entity.mapper;

import com.susoft.productmanager.domain.model.Product;


/**
 * @author Muhammed Sabry
 */
public class ProductMapper extends BaseEntityMapper<Product, com.susoft.productmanager.data.entity.Product> {

    @Override
    public com.susoft.productmanager.data.entity.Product toEntity(Product domainProduct) {
        com.susoft.productmanager.data.entity.Product entityProduct = new com.susoft.productmanager.data.entity.Product();

        entityProduct.setId(domainProduct.getId());
        entityProduct.setTakeawayPrice(domainProduct.getTakeawayPrice());
        entityProduct.setTakeawayVat(domainProduct.getTakeawayVat());
        entityProduct.setVendorId(domainProduct.getVendorId());
        entityProduct.setVendorPrice(domainProduct.getVendorPrice());
        entityProduct.setCookingTime(domainProduct.getCookingTime());
        entityProduct.setActive(domainProduct.isActive());
        entityProduct.setPriority(domainProduct.getPriority());
        entityProduct.setDescription(domainProduct.getDescription());
        entityProduct.setBarcode(domainProduct.getBarcode());
        entityProduct.setName(domainProduct.getName());
        entityProduct.setInPrice(domainProduct.getInPrice());
        entityProduct.setListPrice(domainProduct.getListPrice());
        entityProduct.setNetPrice(domainProduct.getNetPrice());
        entityProduct.setPrintable(domainProduct.isPrintable());
        entityProduct.setProfit(domainProduct.getProfit());
        entityProduct.setRetailPrice(domainProduct.getRetailPrice());
        entityProduct.setServing(domainProduct.getServing());
        entityProduct.setSupplier(domainProduct.getSupplier());
        entityProduct.setTakeawayActive(domainProduct.isTakeawayActive());
        entityProduct.setVat(domainProduct.getVat());
        entityProduct.setImage(domainProduct.getImage());

        if (domainProduct.getCategory1() != null)
            entityProduct.setCategory1(domainProduct.getCategory1().getId());
        else
            entityProduct.setCategory1(null);

        if (domainProduct.getCategory2() != null)
            entityProduct.setCategory2(domainProduct.getCategory2().getId());
        else
            entityProduct.setCategory2(null);

        if (domainProduct.getCategory3() != null)
            entityProduct.setCategory3(domainProduct.getCategory3().getId());
        else
            entityProduct.setCategory3(null);

        if (domainProduct.getCategory4() != null)
            entityProduct.setCategory4(domainProduct.getCategory4().getId());
        else
            entityProduct.setCategory4(null);

        if (domainProduct.getCategory5() != null)
            entityProduct.setCategory5(domainProduct.getCategory5().getId());
        else
            entityProduct.setCategory5(null);

        return entityProduct;
    }
}


