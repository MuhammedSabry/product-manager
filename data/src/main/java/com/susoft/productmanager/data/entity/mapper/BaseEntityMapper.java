package com.susoft.productmanager.data.entity.mapper;

import java.util.List;

import io.reactivex.Flowable;

/**
 * @author Muhammed Sabry
 */
abstract class BaseEntityMapper<D, E> {

    public abstract E toEntity(D domainObject);

    public List<E> toEntity(List<D> domainObjects) {
        return Flowable
                .fromIterable(domainObjects)
                .map(this::toEntity)
                .toList()
                .blockingGet();
    }
}
