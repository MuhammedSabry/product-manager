package com.susoft.productmanager.data.dao;

import com.susoft.productmanager.data.entity.Product;
import com.susoft.productmanager.domain.DBConstants;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Query;
import io.reactivex.Flowable;

/**
 * @author Muhammed Sabry
 */
@Dao
public abstract class ProductDao extends BaseDao<Product> {

    private static final String EMBEDDED_CATEGORIES_JOIN =
            " LEFT OUTER JOIN " + DBConstants.Category.TABLE + " as category1 " +
                    " ON category1." + DBConstants.Category.ID + "=" + DBConstants.Product.CATEGORY_1 +
                    " LEFT OUTER JOIN " + DBConstants.Category.TABLE + " as category2 " +
                    " ON category2." + DBConstants.Category.ID + "=" + DBConstants.Product.CATEGORY_2 +
                    " LEFT OUTER JOIN " + DBConstants.Category.TABLE + " as category3 " +
                    " ON category3." + DBConstants.Category.ID + "=" + DBConstants.Product.CATEGORY_3 +
                    " LEFT OUTER JOIN " + DBConstants.Category.TABLE + " as category4 " +
                    " ON category4." + DBConstants.Category.ID + "=" + DBConstants.Product.CATEGORY_4 +
                    " LEFT OUTER JOIN " + DBConstants.Category.TABLE + " as category5 " +
                    " ON category5." + DBConstants.Category.ID + "=" + DBConstants.Product.CATEGORY_5;

    @Query("SELECT *" +
            " FROM " + DBConstants.Product.TABLE +
            EMBEDDED_CATEGORIES_JOIN +
            ";")
    public abstract Flowable<List<com.susoft.productmanager.domain.model.Product>> getAllProducts();

//    @Query("SELECT * FROM " + DBConstants.Product.TABLE +
//            " INNER JOIN " + DBConstants.Category.TABLE +
//            " on " + DBConstants.Category.ID + "=(" + DBConstants.Product.CATEGORY_1 +
//            " OR " + DBConstants.Product.CATEGORY_2 +
//            " OR " + DBConstants.Product.CATEGORY_3 +
//            " OR " + DBConstants.Product.CATEGORY_4 +
//            " OR " + DBConstants.Product.CATEGORY_5 + ")" +
//            " WHERE " + DBConstants.Category.ID + " =:categoryId" +
//            " ORDER BY " + DBConstants.Product.NAME +
//            " LIMIT 30" +
//            ";")
//    public abstract Flowable<List<com.susoft.productmanager.domain.model.Product>> getProductByCategory(int categoryId);
//
//    @Query("SELECT * FROM " + DBConstants.Product.TABLE +
//            " INNER JOIN " + DBConstants.Category.TABLE +
//            " on " + DBConstants.Category.ID + "=(" + DBConstants.Product.CATEGORY_1 +
//            " OR " + DBConstants.Product.CATEGORY_2 +
//            " OR " + DBConstants.Product.CATEGORY_3 +
//            " OR " + DBConstants.Product.CATEGORY_4 +
//            " OR " + DBConstants.Product.CATEGORY_5 + ")" +
//            " WHERE " + DBConstants.Category.ID + " =:categoryId" +
//            " AND " + DBConstants.Product.NAME + " LIKE '%' || :name || '%'" +
//            " ORDER BY " + DBConstants.Product.NAME +
//            " LIMIT 30" +
//            ";")
//    public abstract Flowable<List<com.susoft.productmanager.domain.model.Product>> getProductByNameAndCategory(String name, int categoryId);
//
//    @Query("SELECT * FROM " + DBConstants.Product.TABLE +
//            EMBEDDED_CATEGORIES_JOIN +
//            " WHERE " + DBConstants.Product.NAME + " LIKE '%' || :name || '%'" +
//            " ORDER BY " + DBConstants.Product.NAME +
//            " LIMIT 30" +
//            ";")
//    public abstract Flowable<List<com.susoft.productmanager.domain.model.Product>> getProductByName(String name);

    @Query("SELECT * FROM " + DBConstants.Product.TABLE +
            EMBEDDED_CATEGORIES_JOIN +
            " WHERE " + DBConstants.Product.ID + " LIKE '%' || :id || '%'" +
            " ORDER BY " + DBConstants.Product.ID +
            " LIMIT 30" +
            ";")
    public abstract Flowable<List<com.susoft.productmanager.domain.model.Product>> getProductsById(String id);

    @Query("SELECT * FROM " + DBConstants.Product.TABLE +
            EMBEDDED_CATEGORIES_JOIN +
            " WHERE " + DBConstants.Product.BARCODE + " LIKE '%' || :barcode || '%'" +
            " ORDER BY " + DBConstants.Product.BARCODE +
            " LIMIT 30" +
            ";")
    public abstract Flowable<List<com.susoft.productmanager.domain.model.Product>> getProductByBarcode(String barcode);

    @Query("DELETE FROM " + DBConstants.Product.TABLE)
    public abstract void deleteAll();
}
