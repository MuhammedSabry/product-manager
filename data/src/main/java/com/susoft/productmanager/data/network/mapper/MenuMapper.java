package com.susoft.productmanager.data.network.mapper;

import com.susoft.productmanager.domain.model.Menu;

import no.susoft.globalone.api.client.data.qlm.QuickLaunchMenu;

public class MenuMapper extends BaseNetworkMapper<Menu, QuickLaunchMenu> {

    @Override
    public Menu fromNetwork(QuickLaunchMenu serverModel) {
        return new Menu(serverModel.getId(), serverModel.getName(), serverModel.getRootGridId());
    }

    @Override
    public QuickLaunchMenu toNetwork(Menu model) {
        return QuickLaunchMenu.builder()
                .id(model.getId())
                .name(model.getName())
                .rootGridId(model.getGridId())
                .build();
    }

}
