package com.susoft.productmanager.data.network.mapper;

import com.susoft.productmanager.domain.model.Shop;

import no.susoft.globalone.api.client.data.info.TenantInfo;

public class ShopMapper extends BaseNetworkMapper<Shop, TenantInfo> {

    @Override
    public Shop fromNetwork(TenantInfo serverModel) {
        return new Shop(serverModel.getTenantId(), serverModel.getTenantName(), serverModel.isRestaurant());
    }

    @Override
    public TenantInfo toNetwork(Shop model) {
        return null;
    }
}
