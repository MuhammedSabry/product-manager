package com.susoft.productmanager.data.entity.mapper;

import com.susoft.productmanager.data.entity.Category;

/**
 * @author Muhammed Sabry
 */
public class CategoryMapper extends BaseEntityMapper<com.susoft.productmanager.domain.model.Category, Category> {

    @Override
    public Category toEntity(com.susoft.productmanager.domain.model.Category domainObject) {

        Category entityCategory = new Category();

        entityCategory.setId(domainObject.getId());
        entityCategory.setValue(domainObject.getValue());
        entityCategory.setLevel(domainObject.getLevel());
        entityCategory.setServerId(domainObject.getServerId());

        if (domainObject.getParentId() != 0)
            entityCategory.setParentId(domainObject.getParentId());
        else
            entityCategory.setParentId(null);

        return entityCategory;
    }
}
