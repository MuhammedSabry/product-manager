package com.susoft.productmanager.data.network.mapper;

import com.susoft.productmanager.domain.model.Product;

/**
 * @author Muhammed Sabry
 */
public class ProductMapper extends BaseNetworkMapper<Product, no.susoft.globalone.api.client.data.product.Product> {

    @Override
    public Product fromNetwork(no.susoft.globalone.api.client.data.product.Product serverModel) {

        Product product = new Product();

        product.setId(serverModel.getId());
        product.setBarcode(serverModel.getBarcode());
        product.setName(serverModel.getName());
        product.setVat(serverModel.getVatPercent());
        product.setRetailPrice(serverModel.getPrice());
        product.setTakeawayPrice(serverModel.getAlternativePrice());
        product.setTakeawayVat(serverModel.getAlternativeVatPercent());
        product.setHasImage(serverModel.getHasImage());
        product.setDescription(serverModel.getDescription());
        product.setCatServerId1(serverModel.getCategory1());
        product.setCatServerId2(serverModel.getCategory2());
        product.setCatServerId3(serverModel.getCategory3());
        product.setCatServerId4(serverModel.getCategory4());
        product.setCatServerId5(serverModel.getCategory5());
        product.setAbcCode(serverModel.getAbcCode());
        product.setProcessLocation(serverModel.getProcessLocation());
        product.setType((int) serverModel.getType());
        product.setMiscellaneous(serverModel.isMiscellaneous());
        return product;
    }

    @Override
    public no.susoft.globalone.api.client.data.product.Product toNetwork(Product model) {

        no.susoft.globalone.api.client.data.product.Product serverProduct = new no.susoft.globalone.api.client.data.product.Product();

        serverProduct.setId(model.getId());
        serverProduct.setBarcode(model.getBarcode());
        serverProduct.setName(model.getName());
        serverProduct.setVatPercent(model.getVat());
        serverProduct.setPrice(model.getRetailPrice());
        serverProduct.setAlternativePrice(model.getTakeawayPrice());
        serverProduct.setAlternativeVatPercent(model.getTakeawayVat());
        serverProduct.setDescription(model.getDescription());
        serverProduct.setCategory1(model.getCatServerId1());
        serverProduct.setCategory2(model.getCatServerId2());
        serverProduct.setCategory3(model.getCatServerId3());
        serverProduct.setCategory4(model.getCatServerId4());
        serverProduct.setCategory5(model.getCatServerId5());
        serverProduct.setAbcCode(model.getAbcCode());
        serverProduct.setProcessLocation(model.getProcessLocation());
        serverProduct.setMiscellaneous(model.isMiscellaneous());
        if (model.getType() != null)
            serverProduct.setType((byte) (int) model.getType());
        return serverProduct;
    }
}
