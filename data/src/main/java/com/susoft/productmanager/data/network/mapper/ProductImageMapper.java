package com.susoft.productmanager.data.network.mapper;

import com.susoft.productmanager.domain.model.ProductImage;

/**
 * @author Muhammed Sabry
 */
public class ProductImageMapper extends BaseNetworkMapper<ProductImage, no.susoft.globalone.api.client.data.product.ProductImage> {

    @Override
    public ProductImage fromNetwork(no.susoft.globalone.api.client.data.product.ProductImage serverModel) {

        ProductImage productImage = new ProductImage();

        productImage.setDefault(serverModel.isDefaultImage());
        productImage.setId(serverModel.getId());
        productImage.setImage(serverModel.getImage());
        productImage.setMime(serverModel.getMime());
        productImage.setTable(serverModel.getTable());
        productImage.setBarcode(serverModel.getBarcode());

        return productImage;
    }

    @Override
    public no.susoft.globalone.api.client.data.product.ProductImage toNetwork(ProductImage model) {

        no.susoft.globalone.api.client.data.product.ProductImage networkImage = new no.susoft.globalone.api.client.data.product.ProductImage();

        networkImage.setDefaultImage(model.isDefault());
        networkImage.setId(model.getId());
        networkImage.setImage(model.getImage());
        networkImage.setMime(model.getMime());
        networkImage.setTable(model.getTable());
        networkImage.setBarcode(model.getBarcode());

        return networkImage;
    }

}
