package com.susoft.productmanager.data.network.mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Muhammed Sabry
 */
public abstract class BaseNetworkMapper<D, S> {

    public abstract D fromNetwork(S serverModel);

    public List<D> fromNetwork(List<S> serverModels) {
        List<D> domainModels = new ArrayList<>();

        for (int i = 0; i < serverModels.size(); i++)
            domainModels.add(fromNetwork(serverModels.get(i)));

        return domainModels;
    }

    public abstract S toNetwork(D model);

    public List<S> toNetwork(List<D> domainModels) {
        List<S> serverModels = new ArrayList<>();

        for (int i = 0; i < domainModels.size(); i++)
            serverModels.add(toNetwork(domainModels.get(i)));

        return serverModels;
    }
}
