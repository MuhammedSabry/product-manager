package com.susoft.productmanager.data.dao;

import com.susoft.productmanager.data.entity.Category;
import com.susoft.productmanager.domain.DBConstants;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.RoomWarnings;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
@Dao
@SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
public abstract class CategoryDao extends BaseDao<Category> {

    @Query("SELECT * FROM category_table" +
            " WHERE " + DBConstants.Category.LEVEL + " !=" + "0;")
    public abstract Flowable<List<com.susoft.productmanager.domain.model.Category>> getAll();

    @Query("SELECT *" +
            " FROM " + DBConstants.Category.TABLE +
            " WHERE " + DBConstants.Category.LEVEL + "= :level;")
    public abstract Flowable<List<com.susoft.productmanager.domain.model.Category>> getLevelCategories(int level);

    @Query("SELECT *" +
            " FROM " + DBConstants.Category.TABLE +
            " WHERE " + DBConstants.Category.PARENT_ID + "= :parentId" +
            " AND " + DBConstants.Category.LEVEL + " !=" + "0;")
    public abstract Flowable<List<com.susoft.productmanager.domain.model.Category>> getChildCategories(int parentId);

    @Query("DELETE FROM " + DBConstants.Category.TABLE)
    public abstract void deleteAll();

    @Query("SELECT *" +
            " FROM " + DBConstants.Category.TABLE +
            " WHERE " + DBConstants.Category.SERVER_ID + "= :serverId" +
            " AND " + DBConstants.Category.LEVEL + "=:level")
    public abstract Single<List<com.susoft.productmanager.domain.model.Category>> getProductCategory(int level, String serverId);
}
