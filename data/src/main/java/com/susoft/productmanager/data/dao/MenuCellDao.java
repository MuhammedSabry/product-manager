package com.susoft.productmanager.data.dao;


import com.susoft.productmanager.domain.DBConstants;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Query;
import io.reactivex.Flowable;

/**
 * @author Muhammed Sabry
 */
@Dao
public abstract class MenuCellDao extends BaseDao<com.susoft.productmanager.data.entity.Cell> {

    @Query("SELECT *" +
            " FROM " + DBConstants.Cell.TABLE +
//            " LEFT OUTER JOIN " + DBConstants.Product.TABLE +
//            " ON " + DBConstants.Product.ID + "=" + DBConstants.Cell.PRODUCT_ID +
//            " LEFT OUTER JOIN " + DBConstants.Menu.TABLE +
//            " ON " + DBConstants.Menu.ID + "=" + DBConstants.Cell.CHILD_MENU_ID +
//            " WHERE " + DBConstants.Cell.PARENT_GRID_ID + "=:menuId" +
            ";")
    public abstract Flowable<List<com.susoft.productmanager.domain.model.Cell>> getMenuCells();
}
