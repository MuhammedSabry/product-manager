package com.susoft.productmanager.data.network.mapper;

import com.susoft.productmanager.domain.model.Cell;

import no.susoft.globalone.api.client.data.qlm.QuickLaunchMenuCell;

public class CellMapper extends BaseNetworkMapper<Cell, QuickLaunchMenuCell> {

    @Override
    public Cell fromNetwork(QuickLaunchMenuCell serverModel) {
        return new Cell(serverModel.getId(),
                serverModel.getIdx(),
                serverModel.getImage(),
                serverModel.getMimeType(),
                serverModel.getParentGridId(),
                serverModel.getText(),
                serverModel.getProductId());
    }

    @Override
    public QuickLaunchMenuCell toNetwork(Cell model) {
        return QuickLaunchMenuCell.builder()
                .id(model.getId())
                .text(model.getText())
                .parentGridId(model.getParentGridId())
                .idx(model.getPosition())
                .image(model.getImage())
                .mimeType(model.getMimeType())
                .productId(model.getProductId())
                .build();
    }
}
