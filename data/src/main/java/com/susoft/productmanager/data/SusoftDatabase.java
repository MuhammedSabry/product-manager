package com.susoft.productmanager.data;

import android.content.Context;

import com.susoft.productmanager.data.dao.CategoryDao;
import com.susoft.productmanager.data.dao.MenuCellDao;
import com.susoft.productmanager.data.dao.MenuDao;
import com.susoft.productmanager.data.dao.ProductDao;
import com.susoft.productmanager.data.entity.Category;
import com.susoft.productmanager.data.entity.Cell;
import com.susoft.productmanager.data.entity.Menu;
import com.susoft.productmanager.data.entity.Product;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

/**
 * @author Muhammed Sabry
 */
@Database(entities = {Product.class,
        Category.class,
        Cell.class,
        Menu.class},
        version = 1,
        exportSchema = false)
public abstract class SusoftDatabase extends RoomDatabase {

    private static final String DATABASE_SCHEMA = "word_db";

    public static SusoftDatabase getInstance(Context context) {
        return Room.databaseBuilder(context.getApplicationContext(), SusoftDatabase.class, DATABASE_SCHEMA)
                .build();
    }

    abstract ProductDao productDao();

    abstract CategoryDao categoryDao();

    abstract MenuDao menuDao();

    abstract MenuCellDao menuCellDao();
}
