package com.susoft.productmanager.data.entity;

import com.susoft.productmanager.domain.DBConstants;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.SET_NULL;
import static com.susoft.productmanager.domain.DBConstants.Cell.PRODUCT_ID;
import static com.susoft.productmanager.domain.DBConstants.Cell.TABLE;

/**
 * @author Muhammed Sabry
 */
@Entity(tableName = TABLE,
        foreignKeys = {
                @ForeignKey(entity = Product.class,
                        parentColumns = DBConstants.Product.ID,
                        childColumns = PRODUCT_ID,
                        onDelete = SET_NULL),
        })
public class Cell {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = DBConstants.Cell.ID)
    private long id;

    private int position;

    @ColumnInfo(name = DBConstants.Cell.IMAGE)
    private byte[] image;

    private String mimeType;

    @ColumnInfo(name = DBConstants.Cell.PARENT_GRID_ID)
    private long parentGridId;

    @ColumnInfo(name = DBConstants.Cell.TEXT)
    private String text;

    @ColumnInfo(name = DBConstants.Cell.PRODUCT_ID)
    private String productId;

    public Cell(long id, int position, byte[] image, String mimeType, long parentGridId, String text, String productId) {
        this.id = id;
        this.position = position;
        this.image = image;
        this.mimeType = mimeType;
        this.parentGridId = parentGridId;
        this.text = text;
        this.productId = productId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public long getParentGridId() {
        return parentGridId;
    }

    public void setParentGridId(long parentGridId) {
        this.parentGridId = parentGridId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
