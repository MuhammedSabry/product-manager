package com.susoft.productmanager.data;

import com.susoft.productmanager.data.dao.CategoryDao;
import com.susoft.productmanager.data.dao.MenuCellDao;
import com.susoft.productmanager.data.dao.MenuDao;
import com.susoft.productmanager.data.dao.ProductDao;
import com.susoft.productmanager.data.entity.mapper.CategoryMapper;
import com.susoft.productmanager.data.entity.mapper.MenuCellMapper;
import com.susoft.productmanager.data.entity.mapper.MenuMapper;
import com.susoft.productmanager.data.entity.mapper.ProductMapper;
import com.susoft.productmanager.domain.model.Category;
import com.susoft.productmanager.domain.model.Cell;
import com.susoft.productmanager.domain.model.Menu;
import com.susoft.productmanager.domain.model.Product;
import com.susoft.productmanager.domain.repository.CategoryRepository;
import com.susoft.productmanager.domain.repository.MenuCellRepository;
import com.susoft.productmanager.domain.repository.MenuRepository;
import com.susoft.productmanager.domain.repository.ProductRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public class DatabaseRepository implements ProductRepository,
        CategoryRepository,
        MenuRepository,
        MenuCellRepository {
    private ProductDao productDao;
    private CategoryDao categoryDao;
    private MenuCellDao menuCellDao;
    private MenuDao menuDao;

    @Inject
    public DatabaseRepository(SusoftDatabase susoftDatabase) {
        this.productDao = susoftDatabase.productDao();
        this.menuCellDao = susoftDatabase.menuCellDao();
        this.categoryDao = susoftDatabase.categoryDao();
        this.menuDao = susoftDatabase.menuDao();
    }

    @Override
    public Flowable<List<Category>> getCategories() {
        return categoryDao.getAll();
    }

    @Override
    public Flowable<List<Category>> getLevelCategories(int level) {
        return categoryDao.getLevelCategories(level);
    }

    @Override
    public Flowable<List<Category>> getChildCategories(int parentId) {
        return categoryDao.getChildCategories(parentId);
    }

    @Override
    public Completable deleteAllCategories() {
        return Completable.fromAction(() -> categoryDao.deleteAll());
    }

    @Override
    public Completable delete(List<Category> category) {
        return Completable.fromAction(() -> categoryDao.delete(new CategoryMapper().toEntity(category)));
    }

    @Override
    public Single<Category> getProductCategories(int level, String serverId) {
        return categoryDao.getProductCategory(level, serverId)
                .toFlowable()
                .flatMapIterable(categories -> categories)
                .firstOrError();
    }

    @Override
    public Single<Category> getProductCategories(int level, String serverId, int parentCategoryId) {
        return categoryDao.getProductCategory(level, serverId)
                .toFlowable()
                .flatMapIterable(categories -> categories)
                .filter(category -> category.getParentId() == parentCategoryId)
                .firstOrError();
    }

    @Override
    public Flowable<List<Product>> getProductsById(String id) {
        return productDao.getProductsById(id);
    }

    @Override
    public Flowable<List<Product>> getProductsByBarcode(String barcode) {
        return productDao.getProductByBarcode(barcode);
    }

    @Override
    public Flowable<List<Product>> getProductsByNameAndCategory() {
        return productDao.getAllProducts();
    }

    @Override
    public Completable deleteAllProducts() {
        return Completable.fromAction(() -> productDao.deleteAll());
    }

    @Override
    public Flowable<List<Menu>> getMenus() {
        return menuDao.getAll();
    }

    @Override
    public Flowable<Menu> getLastMenu() {
        return menuDao.getLastMenu();
    }

    @Override
    public Flowable<List<Cell>> getMenuCells(int menuId) {
        return menuCellDao.getMenuCells();
    }

    @Override
    public Flowable<Long> insert(Cell cell) {
        MenuCellMapper menuCellMapper = new MenuCellMapper(cell);
//        if (cell.getChildMenu() != null)
//            return insert(cell.getChildMenu())
//                    .concatWith(menuCellDao
//                            .insert(menuCellMapper.getEntityCell())
//                            .toFlowable());
//        else
        return menuCellDao.insert(menuCellMapper.getEntityCell())
                .toFlowable();
    }

    @Override
    public Flowable<Integer> deleteMenu(Menu menu) {
        MenuMapper menuMapper = new MenuMapper(menu);
        return menuDao.delete(menuMapper.getEntityMenu()).toFlowable();
    }

    @Override
    public Single<Integer> deleteCell(Cell cell) {
        MenuCellMapper menuCellMapper = new MenuCellMapper(cell);
        return menuCellDao.delete(menuCellMapper.getEntityCell());
    }

    @Override
    public Flowable<Long> insert(Menu menu) {
        MenuMapper menuMapper = new MenuMapper(menu);
        return menuDao
                .insert(menuMapper.getEntityMenu())
                .toFlowable()
//                .concatWith(Flowable
//                        .fromIterable(menu.getCells())
//                        .flatMap(this::insert))
                ;
    }

    @Override
    public Flowable<Long> insert(Category category) {
        return categoryDao
                .insert(new CategoryMapper().toEntity(category))
                .toFlowable()
                .flatMap(rowId -> Flowable
                        .fromIterable(category.getChildren())
                        .map(category1 -> {
                            category1.setParentId((int) (long) rowId);
                            return category1;
                        })
                        .flatMap(this::insert));
    }

    @Override
    public Flowable<Long> insert(List<Category> categories) {
        return Flowable.fromIterable(categories).flatMap(this::insert);
    }

    @Override
    public Completable insert(Product product) {
        return productDao.insert(new ProductMapper().toEntity(product))
                .ignoreElement();
    }

}
