package com.susoft.productmanager.data.network.retrofit;

import java.io.IOException;

import androidx.annotation.NonNull;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static com.susoft.productmanager.data.network.NetworkConstants.Header.ACCESS_TOKEN;

/**
 * @author Muhammed Sabry
 */
public class TokenInterceptor implements Interceptor {

    private String token;

    TokenInterceptor(String token) {
        this.token = token;
    }

    @Override
    @NonNull
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request()
                .newBuilder()
                .addHeader(ACCESS_TOKEN, getToken())
                .build();
        return chain.proceed(request);
    }

    private String getToken() {
        return token == null ? "" : token;
    }

}
