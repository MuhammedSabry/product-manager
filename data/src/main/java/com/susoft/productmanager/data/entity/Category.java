package com.susoft.productmanager.data.entity;

import com.susoft.productmanager.domain.DBConstants;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

/**
 * @author Muhammed Sabry
 */
@Entity(tableName = DBConstants.Category.TABLE,
        indices = @Index(DBConstants.Category.PARENT_ID),
        foreignKeys = @ForeignKey(entity = Category.class,
                parentColumns = DBConstants.Category.ID,
                childColumns = DBConstants.Category.PARENT_ID,
                onDelete = ForeignKey.CASCADE))
public class Category {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = DBConstants.Category.ID)
    private int id;

    @ColumnInfo(name = DBConstants.Category.SERVER_ID)
    private String serverId;

    @NonNull
    @ColumnInfo(name = DBConstants.Category.VALUE)
    private String value;

    @ColumnInfo(name = DBConstants.Category.LEVEL)
    private int level;

    @ColumnInfo(name = DBConstants.Category.PARENT_ID)
    private Integer parentId;

    public Category() {
        value = "unknown";
        level = 1;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getValue() {
        return value;
    }

    public void setValue(@NonNull String value) {
        this.value = value;
    }
}
