package com.susoft.productmanager.data.network.mapper;

import com.susoft.productmanager.domain.model.Grid;

import no.susoft.globalone.api.client.data.qlm.QuickLaunchMenuGrid;

public class GridMapper extends BaseNetworkMapper<Grid, QuickLaunchMenuGrid> {

    @Override
    public Grid fromNetwork(QuickLaunchMenuGrid serverModel) {
        return new Grid(serverModel.getId(), serverModel.getSizeX(), serverModel.getSizeY(), serverModel.getParentCellId());
    }

    @Override
    public QuickLaunchMenuGrid toNetwork(Grid model) {
        return QuickLaunchMenuGrid.builder()
                .id(model.getId())
                .parentCellId(model.getParentCellId())
                .sizeX(model.getColumns())
                .sizeY(model.getRows())
                .build();
    }

}
