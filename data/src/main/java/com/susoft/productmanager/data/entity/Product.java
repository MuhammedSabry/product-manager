package com.susoft.productmanager.data.entity;

import com.susoft.productmanager.domain.DBConstants;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

/**
 * @author Muhammed Sabry
 */
@Entity(tableName = DBConstants.Product.TABLE,
        indices = {@Index(value = DBConstants.Product.ID),
                @Index(value = DBConstants.Product.BARCODE, unique = true)},
        foreignKeys = {
                @ForeignKey(entity = Category.class,
                        parentColumns = DBConstants.Category.ID,
                        childColumns = DBConstants.Product.CATEGORY_1),
                @ForeignKey(entity = Category.class,
                        parentColumns = DBConstants.Category.ID,
                        childColumns = DBConstants.Product.CATEGORY_2),
                @ForeignKey(entity = Category.class,
                        parentColumns = DBConstants.Category.ID,
                        childColumns = DBConstants.Product.CATEGORY_3),
                @ForeignKey(entity = Category.class,
                        parentColumns = DBConstants.Category.ID,
                        childColumns = DBConstants.Product.CATEGORY_4),
                @ForeignKey(entity = Category.class,
                        parentColumns = DBConstants.Category.ID,
                        childColumns = DBConstants.Product.CATEGORY_5)
        })
public class Product implements Serializable {

    @PrimaryKey
    @ColumnInfo(name = DBConstants.Product.ID)
    @NonNull
    private String id = "";

    @ColumnInfo(name = DBConstants.Product.NAME)
    private String name;

    @ColumnInfo(name = DBConstants.Product.DESCRIPTION)
    private String description;

    @ColumnInfo(name = DBConstants.Product.SUPPLIER)
    private String supplier;

    @ColumnInfo(name = DBConstants.Product.BARCODE)
    private String barcode;

    @ColumnInfo(name = DBConstants.Product.SERVING)
    private String serving;

    @ColumnInfo(name = DBConstants.Product.LIST_PRICE)
    private double listPrice;

    @ColumnInfo(name = DBConstants.Product.VENDOR_PRICE)
    private double vendorPrice;

    @ColumnInfo(name = DBConstants.Product.RETAIL_PRICE)
    private double retailPrice;

    @ColumnInfo(name = DBConstants.Product.IN_PRICE)
    private double inPrice;

    @ColumnInfo(name = DBConstants.Product.NET_PRICE)
    private double netPrice;

    @ColumnInfo(name = DBConstants.Product.VAT)
    private double vat;

    @ColumnInfo(name = DBConstants.Product.PROFIT)
    private double profit;

    @ColumnInfo(name = DBConstants.Product.TAKEAWAY_PRICE)
    private double takeawayPrice;

    @ColumnInfo(name = DBConstants.Product.TAKEAWAY_VAT)
    private double takeawayVat;

    @ColumnInfo(name = DBConstants.Product.VENDOR_ID)
    private int vendorId;

    @ColumnInfo(name = DBConstants.Product.PRIORITY)
    private int priority;

    @ColumnInfo(name = DBConstants.Product.COOKING_TIME)
    private int cookingTime;

    @ColumnInfo(name = DBConstants.Product.IS_ACTIVE)
    private boolean isActive;

    @ColumnInfo(name = DBConstants.Product.IS_PRINTABLE)
    private boolean isPrintable;

    @ColumnInfo(name = DBConstants.Product.IS_TAKEAWAY_ACTIVE)
    private boolean isTakeawayActive;

    @ColumnInfo(name = DBConstants.Product.CATEGORY_1, index = true)
    private Integer category1;

    @ColumnInfo(name = DBConstants.Product.CATEGORY_2, index = true)
    private Integer category2;

    @ColumnInfo(name = DBConstants.Product.CATEGORY_3, index = true)
    private Integer category3;

    @ColumnInfo(name = DBConstants.Product.CATEGORY_4, index = true)
    private Integer category4;

    @ColumnInfo(name = DBConstants.Product.CATEGORY_5, index = true)
    private Integer category5;

    @ColumnInfo(name = DBConstants.Product.IMAGE, typeAffinity = ColumnInfo.BLOB)
    private byte[] image;

    public Integer getCategory1() {
        return category1;
    }

    public void setCategory1(Integer category1) {
        this.category1 = category1;
    }

    public Integer getCategory2() {
        return category2;
    }

    public void setCategory2(Integer category2) {
        this.category2 = category2;
    }

    public Integer getCategory3() {
        return category3;
    }

    public void setCategory3(Integer category3) {
        this.category3 = category3;
    }

    public Integer getCategory4() {
        return category4;
    }

    public void setCategory4(Integer category4) {
        this.category4 = category4;
    }

    public Integer getCategory5() {
        return category5;
    }

    public void setCategory5(Integer category5) {
        this.category5 = category5;
    }

    public boolean isPrintable() {
        return isPrintable;
    }

    public void setPrintable(boolean printable) {
        isPrintable = printable;
    }

    public boolean isTakeawayActive() {
        return isTakeawayActive;
    }

    public void setTakeawayActive(boolean takeawayActive) {
        isTakeawayActive = takeawayActive;
    }

    public double getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(double netPrice) {
        this.netPrice = netPrice;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTakeawayPrice() {
        return takeawayPrice;
    }

    public void setTakeawayPrice(double takeawayPrice) {
        this.takeawayPrice = takeawayPrice;
    }

    public double getTakeawayVat() {
        return takeawayVat;
    }

    public void setTakeawayVat(double takeawayVat) {
        this.takeawayVat = takeawayVat;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getServing() {
        return serving;
    }

    public void setServing(String serving) {
        this.serving = serving;
    }

    public double getListPrice() {
        return listPrice;
    }

    public void setListPrice(double listPrice) {
        this.listPrice = listPrice;
    }

    public double getVendorPrice() {
        return vendorPrice;
    }

    public void setVendorPrice(double vendorPrice) {
        this.vendorPrice = vendorPrice;
    }

    public double getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(double retailPrice) {
        this.retailPrice = retailPrice;
    }

    public double getInPrice() {
        return inPrice;
    }

    public void setInPrice(double inPrice) {
        this.inPrice = inPrice;
    }

    public double getVat() {
        return vat;
    }

    public void setVat(double vat) {
        this.vat = vat;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public int getCookingTime() {
        return cookingTime;
    }

    public void setCookingTime(int cookingTime) {
        this.cookingTime = cookingTime;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

}
