package com.susoft.productmanager.data.dao;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Update;
import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
@Dao
abstract class BaseDao<E> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract Single<Long> insert(E entity);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract Single<Integer> update(E entity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract Single<List<Long>> insert(List<E> entities);

    @Delete
    public abstract Single<Integer> delete(E entity);

    @Delete
    public abstract void delete(List<E> entities);

}
