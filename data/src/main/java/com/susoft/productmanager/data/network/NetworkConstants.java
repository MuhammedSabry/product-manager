package com.susoft.productmanager.data.network;

/**
 * @author Muhammed Sabry
 */
public class NetworkConstants {
    public static final String BASE_URL = "https://api.susoft.com:9443";
    public static final int REQUEST_TIMEOUT_IN_MILLISECONDS = 25 * 1000;

    public static class Header {
        public static final String CONTENT_TYPE_JSON = "Content-Type:application/json";
        public static final String ACCESS_TOKEN = "token";
        public static final String AUTHORIZATION = "Authorization";
    }

    public static class Mapping {
        public static final String LOGIN = "login";
        public static final String ME = "me";
        public static final String CATEGORY_TREE = "product/category/tree";
        public static final String PRODUCT_IMAGES_LIST = "product/image/list/{barcode}";
        public static final String ADD_PRODUCT_IMAGE = "product/image";
        public static final String CREATE_PRODUCT = "product";
        public static final String GET_PRODUCT_BY_ID = "product/{id}";
        public static final String SEARCH_FOR_PRODUCT = "product/list";
        public static final String QLM_CELL = "qlm/cell";
        public static final String DELETE_QLM_CELL = "qlm/cell/{cellId}";
        public static final String GET_CELL_GRID = "qlm/cell/grid/{cellId}";
        public static final String QLM_GRID = "qlm/grid";
        public static final String GET_QLM_GRID = "qlm/grid/{gridId}";
        public static final String GET_QLM_GRID_CELLS = "qlm/grid/cells/{gridId}";
        public static final String QLM_MENU_LIST = "qlm/list";
        public static final String CREATE_QLM_MENU = "qlm/menu";
        public static final String GET_QLM_MENU = "qlm/menu/{menuId}";
        public static final String ACTIVATE_MENU_FOR_SHOP = "qlm/menu/activate";
        public static final String DEACTIVATE_MENU_FOR_SHOP = "qlm/menu/deactivate";
        public static final String SET_MENU_STATUS = "qlm/menu/status";
        public static final String CATEGORY = "product/category";
        public static final String GET_PRODUCT_BY_BARCODE = "product/barcode/{barcode}";
        public static final String PRODUCT_TYPE_LIST = "product/type/list";
        public static final String GET_TENANT_INFO = "info/tenant";
    }

    public static class Param {
        public static final String TENANT = "tenant";
        public static final String SHOP = "shop";
        public static final String CATEGORY_ID1 = "id1";
        public static final String CATEGORY_ID2 = "id2";
        public static final String NAME = "name";
        public static final String BARCODE = "barcode";
        public static final String ID = "id";
        public static final String PAGE = "page";
        public static final String PAGE_SIZE = "pageSize";
        public static final String SEARCH_CRITERIA = "searchCriteria";
        public static final String CELL_ID = "cellId";
        public static final String GRID_ID = "gridId";
        public static final String WITH_IMAGE = "withImage";
        public static final String MENU_NAME = "menuName";
        public static final String ROOT_GRID_SIZE_X = "rootGridSizeX";
        public static final String ROOT_GRID_SIZE_Y = "rootGridSizeY";
        public static final String MENU_ID = "menuId";
        public static final String ACTIVE = "active";
    }
}
