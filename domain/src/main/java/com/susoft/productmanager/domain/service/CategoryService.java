package com.susoft.productmanager.domain.service;

import com.susoft.productmanager.domain.model.Category;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public interface CategoryService {

    Single<Category> getCategories();

    Completable createCategory(String id1, String id2, String name);

    Completable createCategory(String id, String name);

    Completable updateCategory(String id1, String id2, String name);

    Completable updateCategory(String id, String name);

    Completable deleteCategory(String id1, String id2);

    Completable deleteCategory(String id);
}
