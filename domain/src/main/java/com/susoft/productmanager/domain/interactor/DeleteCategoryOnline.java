package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.CompletableUseCase;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.service.CategoryService;

import javax.inject.Inject;

import io.reactivex.Completable;

/**
 * @author Muhammed Sabry
 */
public class DeleteCategoryOnline extends CompletableUseCase<Void> {

    private final CategoryService categoryService;
    private String id1;
    private String id2;

    @Inject
    DeleteCategoryOnline(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, CategoryService categoryService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.categoryService = categoryService;
    }

    public Completable execute(String id1, String id2) {
        this.id1 = id1;
        this.id2 = id2;
        return super.execute();
    }

    @Override
    protected Completable interact(Void param) {
        if (id2 != null)
            return categoryService.deleteCategory(id1, id2);
        else
            return categoryService.deleteCategory(id1);
    }
}
