package com.susoft.productmanager.domain.eventbus;

/**
 * @author Muhammed Sabry
 */
public class ConnectivityEvent extends BaseEvent {

    private final boolean hasInternetConnection;

    public ConnectivityEvent(boolean hasInternetConnection) {
        this.hasInternetConnection = hasInternetConnection;
    }

    public boolean hasInternetConnection() {
        return hasInternetConnection;
    }
}
