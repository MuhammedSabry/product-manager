package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.SingleUseCase;
import com.susoft.productmanager.domain.model.Grid;
import com.susoft.productmanager.domain.service.GridService;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public class GetGridOnline extends SingleUseCase<Grid, Long> {

    private final GridService gridService;

    @Inject
    GetGridOnline(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, GridService gridService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.gridService = gridService;
    }

    @Override
    protected Single<Grid> interact(Long gridId) {
        return gridService.getGrid(gridId);
    }
}
