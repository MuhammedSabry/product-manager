package com.susoft.productmanager.domain.service;

import com.susoft.productmanager.domain.model.Cell;
import com.susoft.productmanager.domain.model.Grid;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public interface CellService {

    Single<Cell> getCell(long cellId, boolean withImage);

    Single<Cell> getCell(long cellId);

    Single<Cell> createCell(Cell cell);

    Single<List<Cell>> getGridCells(long gridId);

    Single<Grid> getCellGrid(long cellId);

    Completable updateCell(Cell cell);

    Completable deleteCell(long cellId);
}
