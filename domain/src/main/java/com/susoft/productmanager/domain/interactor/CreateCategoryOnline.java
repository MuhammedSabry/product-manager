package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.CompletableUseCase;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.service.CategoryService;

import javax.inject.Inject;

import io.reactivex.Completable;

/**
 * @author Muhammed Sabry
 */
public class CreateCategoryOnline extends CompletableUseCase<Void> {

    private final CategoryService categoryService;
    private String id1;
    private String id2;
    private String name;

    @Inject
    CreateCategoryOnline(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, CategoryService categoryService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.categoryService = categoryService;
    }

    public Completable execute(String id1, String id2, String name) {
        this.id1 = id1;
        this.id2 = id2;
        this.name = name;
        return super.execute();
    }

    @Override
    protected Completable interact(Void param) {
        if (id2 != null)
            return categoryService.createCategory(id1, id2, name);
        else
            return categoryService.createCategory(id1, name);
    }
}
