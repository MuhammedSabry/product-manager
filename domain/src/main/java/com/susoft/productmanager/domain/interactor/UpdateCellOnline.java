package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.CompletableUseCase;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.model.Cell;
import com.susoft.productmanager.domain.service.CellService;

import javax.inject.Inject;

import io.reactivex.Completable;

/**
 * @author Muhammed Sabry
 */
public class UpdateCellOnline extends CompletableUseCase<Cell> {
    private final CellService cellService;

    @Inject
    UpdateCellOnline(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, CellService cellService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.cellService = cellService;
    }

    @Override
    protected Completable interact(Cell cell) {
        return cellService.updateCell(cell);
    }
}
