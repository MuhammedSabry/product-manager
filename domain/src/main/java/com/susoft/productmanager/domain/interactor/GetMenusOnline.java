package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.FlowableUseCase;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.model.Menu;
import com.susoft.productmanager.domain.service.MenuService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;

/**
 * @author Muhammed Sabry
 */
public class GetMenusOnline extends FlowableUseCase<List<Menu>, Void> {

    private final MenuService menuService;

    @Inject
    GetMenusOnline(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, MenuService menuService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.menuService = menuService;
    }

    @Override
    protected Flowable<List<Menu>> interact(Void param) {
        return menuService.getMenus()
                .toFlowable();
    }

}
