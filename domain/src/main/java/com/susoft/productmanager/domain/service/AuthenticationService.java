package com.susoft.productmanager.domain.service;

import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public interface AuthenticationService {

    Single<String> login(String userId, String shopId, String chain, String password);

}
