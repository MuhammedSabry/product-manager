package com.susoft.productmanager.domain.service;

import com.susoft.productmanager.domain.model.Shop;

import io.reactivex.Single;

public interface UserService {
    Single<Shop> getShopInfo();
}
