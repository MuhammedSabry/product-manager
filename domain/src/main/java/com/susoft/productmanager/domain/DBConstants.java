package com.susoft.productmanager.domain;

/**
 * @author Muhammed Sabry
 */
public class DBConstants {

    public static class Cell {
        private static final String PREFIX = "menu_cell_";
        public static final String TABLE = PREFIX + "table";
        public static final String ID = PREFIX + "id";
        public static final String PARENT_GRID_ID = PREFIX + "parent_grid_id";
        public static final String CHILD_MENU_ID = PREFIX + "parent_cell_id";
        public static final String PRODUCT_ID = PREFIX + "product_id";
        public static final String TEXT = PREFIX + "text";
        public static final String IMAGE = PREFIX + "cell_image";
    }

    public static class Product {
        private static final String PREFIX = "product_";
        public static final String CATEGORY_1 = PREFIX + "category_1";
        public static final String CATEGORY_2 = PREFIX + "category_2";
        public static final String CATEGORY_3 = PREFIX + "category_3";
        public static final String CATEGORY_4 = PREFIX + "category_4";
        public static final String CATEGORY_5 = PREFIX + "category_5";
        public static final String TABLE = PREFIX + "table";
        public static final String ID = PREFIX + "id";
        public static final String IMAGE = PREFIX + "image";
        public static final String NAME = PREFIX + "name";
        public static final String DESCRIPTION = PREFIX + "description";
        public static final String SUPPLIER = PREFIX + "supplier";
        public static final String BARCODE = PREFIX + "barcode";
        public static final String SERVING = PREFIX + "serving";
        public static final String LIST_PRICE = PREFIX + "listPrice";
        public static final String VENDOR_PRICE = PREFIX + "vendorPrice";
        public static final String RETAIL_PRICE = PREFIX + "retailPrice";
        public static final String IN_PRICE = PREFIX + "inPrice";
        public static final String NET_PRICE = PREFIX + "netPrice";
        public static final String VAT = PREFIX + "vat";
        public static final String PROFIT = PREFIX + "profit";
        public static final String TAKEAWAY_PRICE = PREFIX + "takeawayPrice";
        public static final String TAKEAWAY_VAT = PREFIX + "takeawayVat";
        public static final String VENDOR_ID = PREFIX + "vendorId";
        public static final String PRIORITY = PREFIX + "priority";
        public static final String COOKING_TIME = PREFIX + "cookingTime";
        public static final String IS_ACTIVE = PREFIX + "isActive";
        public static final String IS_PRINTABLE = PREFIX + "isPrintable";
        public static final String IS_TAKEAWAY_ACTIVE = PREFIX + "isTakeawayActive";
    }

    public static class Category {
        private static final String PREFIX = "category_";
        public static final String TABLE = PREFIX + "table";
        public static final String ID = PREFIX + "id";
        public static final String SERVER_ID = PREFIX + "server_id";
        public static final String PARENT_ID = PREFIX + "parent_id";
        public static final String VALUE = PREFIX + "value";
        public static final String LEVEL = PREFIX + "level";
    }

    public static class Menu {
        private static final String PREFIX = "menu_";
        public static final String GRID_ID = PREFIX + "grid_id";
        public static final String TABLE = PREFIX + "table";
        public static final String ID = PREFIX + "id";
        public static final String NAME = PREFIX + "name";
    }

    public static class ProductImage {
        private static final String PREFIX = "product_image_";
        public static final String ID = PREFIX + "id";
        public static final String IS_DEFAULT = PREFIX + "is_default";
        public static final String IMAGE_BYTES = PREFIX + "image_data";
        public static final String SERVER_ID = PREFIX + "server_id";
        public static final String SERVER_TABLE = PREFIX + "server_table";
        public static final String TABLE = PREFIX + "table";
        public static final String MIME = PREFIX + "mime";

    }
}
