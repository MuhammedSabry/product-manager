package com.susoft.productmanager.domain.interactor.usecase;

import io.reactivex.Scheduler;

/**
 * Interface for providing the background thread for the application
 *
 * @author Muhammed Sabry
 */
public interface BackgroundExecutionThread {
    Scheduler getScheduler();
}
