package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.CompletableUseCase;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.service.GridService;

import javax.inject.Inject;

import io.reactivex.Completable;

/**
 * @author Muhammed Sabry
 */
public class DeleteGridOnline extends CompletableUseCase<Long> {

    private final GridService gridService;

    @Inject
    DeleteGridOnline(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, GridService gridService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.gridService = gridService;
    }

    @Override
    protected Completable interact(Long gridId) {
        return gridService.deleteGrid(gridId);
    }
}
