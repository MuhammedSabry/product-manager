package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.CompletableUseCase;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.service.CellService;

import javax.inject.Inject;

import io.reactivex.Completable;

/**
 * @author Muhammed Sabry
 */
public class DeleteCellOnline extends CompletableUseCase<Long> {

    private final CellService cellService;

    @Inject
    DeleteCellOnline(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, CellService cellService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.cellService = cellService;
    }

    @Override
    protected Completable interact(Long cellId) {
        return cellService.deleteCell(cellId);
    }
}
