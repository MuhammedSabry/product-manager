package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.CompletableUseCase;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.model.Category;
import com.susoft.productmanager.domain.repository.CategoryRepository;

import javax.inject.Inject;

import io.reactivex.Completable;

/**
 * @author Muhammed Sabry
 */
public class InsertCategoryOffline extends CompletableUseCase<Category> {
    private CategoryRepository categoryRepository;

    @Inject
    InsertCategoryOffline(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, CategoryRepository categoryRepository) {
        super(backgroundExecutionThread, postExecutionThread);
        this.categoryRepository = categoryRepository;
    }

    @Override
    protected Completable interact(Category param) {

        return categoryRepository.deleteAllCategories()
                .concatWith(Completable.fromPublisher(categoryRepository.insert(param)));
    }
}
