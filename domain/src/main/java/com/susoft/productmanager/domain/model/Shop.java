package com.susoft.productmanager.domain.model;

public class Shop {
    private int id;
    private String name;
    private boolean isRestaurant;

    public Shop(int id, String name, boolean isRestaurant) {
        this.id = id;
        this.name = name;
        this.isRestaurant = isRestaurant;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isRestaurant() {
        return isRestaurant;
    }
}
