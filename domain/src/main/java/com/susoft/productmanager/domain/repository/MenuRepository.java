package com.susoft.productmanager.domain.repository;

import com.susoft.productmanager.domain.model.Menu;

import java.util.List;

import io.reactivex.Flowable;

/**
 * @author Muhammed Sabry
 */
public interface MenuRepository {

    Flowable<Long> insert(Menu menu);

    Flowable<List<Menu>> getMenus();

    Flowable<Menu> getLastMenu();

    Flowable<Integer> deleteMenu(Menu menu);
}
