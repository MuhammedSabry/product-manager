package com.susoft.productmanager.domain.service;

import com.susoft.productmanager.domain.model.Category;
import com.susoft.productmanager.domain.model.Product;
import com.susoft.productmanager.domain.model.ProductImage;
import com.susoft.productmanager.domain.model.ProductType;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public interface ProductService {
    Single<List<Product>> getProducts(int page, int pageSize, String search, Category category);

    Single<List<ProductImage>> getProductImages(String productBarcode);

    Completable addImageToProduct(ProductImage productImage);

    Completable createProduct(Product product);

    Completable updateProduct(Product product);

    Single<Product> getProductByID(String productId);

    Single<Product> getProductByBarcode(String barcode);

    Single<List<ProductType>> getProductTypes();

}
