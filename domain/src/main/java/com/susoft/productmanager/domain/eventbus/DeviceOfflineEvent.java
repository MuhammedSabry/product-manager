package com.susoft.productmanager.domain.eventbus;

public class DeviceOfflineEvent extends BaseEvent {
    private boolean isOffline;

    public DeviceOfflineEvent(boolean isOffline) {
        this.isOffline = isOffline;
    }

    public boolean isOffline() {
        return isOffline;
    }
}
