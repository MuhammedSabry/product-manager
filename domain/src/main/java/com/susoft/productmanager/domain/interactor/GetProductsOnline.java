package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.SingleUseCase;
import com.susoft.productmanager.domain.model.Category;
import com.susoft.productmanager.domain.model.Product;
import com.susoft.productmanager.domain.service.ProductService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public class GetProductsOnline extends SingleUseCase<List<Product>, Void> {

    private final ProductService productService;

    private int page;
    private int pageSize;
    private String query;
    private Category category;

    @Inject
    GetProductsOnline(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, ProductService productService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.productService = productService;
    }

    public Single<List<Product>> execute(int page, int pageSize, String query, Category category) {
        this.query = query;
        this.page = page;
        this.pageSize = pageSize;
        this.category = category;
        return super.execute();
    }

    @Override
    protected Single<List<Product>> interact(Void param) {
        return productService.getProducts(page, pageSize, query, category);
    }
}
