package com.susoft.productmanager.domain.repository;

import com.susoft.productmanager.domain.model.Cell;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public interface MenuCellRepository {
    Flowable<Long> insert(Cell cell);

    Single<Integer> deleteCell(Cell cell);

    Flowable<List<Cell>> getMenuCells(int menuId);
}
