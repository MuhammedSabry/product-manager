package com.susoft.productmanager.domain;

/**
 * @author Muhammed Sabry
 */
public interface AuthTokenStorage {

    void onTokenChanged(OnTokenChangeListener tokenChangeListener);

    String getToken();

    void setToken(String token);

    interface OnTokenChangeListener {
        void onTokenChanged(String token);
    }
}
