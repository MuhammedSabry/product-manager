package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.SingleUseCase;
import com.susoft.productmanager.domain.model.ProductType;
import com.susoft.productmanager.domain.service.ProductService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class GetProductTypesInteractor extends SingleUseCase<List<ProductType>, Void> {
    private final ProductService productService;

    @Inject
    GetProductTypesInteractor(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, ProductService productService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.productService = productService;
    }

    @Override
    protected Single<List<ProductType>> interact(Void param) {
        return productService.getProductTypes();
    }
}
