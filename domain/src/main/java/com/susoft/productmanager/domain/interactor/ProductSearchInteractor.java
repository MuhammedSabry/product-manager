package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.FlowableUseCase;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.model.Product;
import com.susoft.productmanager.domain.repository.ProductRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;

/**
 * @author Muhammed Sabry
 */
public class ProductSearchInteractor extends FlowableUseCase<List<Product>, String> {
    private final ProductRepository productRepository;

    @Inject
    ProductSearchInteractor(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, ProductRepository productRepository) {
        super(backgroundExecutionThread, postExecutionThread);
        this.productRepository = productRepository;
    }

    @Override
    protected Flowable<List<Product>> interact(String query) {
        if (query.isEmpty())
            return Flowable.just(Collections.emptyList());

        return productRepository.getProductsByNameAndCategory()
                .concatWith(productRepository.getProductsByBarcode(query))
                .concatWith(productRepository.getProductsById(query))
                .map(this::shuffleAndLimit);
    }

    private List<Product> shuffleAndLimit(List<Product> products) {

        List<Product> shuffledProducts = new ArrayList<>(products);

        Collections.shuffle(shuffledProducts);

        if (shuffledProducts.size() > 30)
            shuffledProducts = shuffledProducts.subList(0, 31);

        return shuffledProducts;
    }
}
