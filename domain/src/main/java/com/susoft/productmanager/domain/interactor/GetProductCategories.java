package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.SingleUseCase;
import com.susoft.productmanager.domain.model.Category;
import com.susoft.productmanager.domain.service.CategoryService;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public class GetProductCategories extends SingleUseCase<Category, Void> {

    private final CategoryService categoryService;

    @Inject
    GetProductCategories(BackgroundExecutionThread backgroundExecutionThread,
                         PostExecutionThread postExecutionThread,
                         CategoryService categoryService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.categoryService = categoryService;
    }

    @Override
    protected Single<Category> interact(Void param) {
        return categoryService.getCategories();
    }

}
