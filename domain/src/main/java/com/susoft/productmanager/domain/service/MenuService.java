package com.susoft.productmanager.domain.service;

import com.susoft.productmanager.domain.model.Menu;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public interface MenuService {

    Single<List<Menu>> getMenus();

    Completable createMenu(String menuName, int columns, int rows);

    Single<Menu> getMenuById(long menuId);

    Completable deleteMenu(long menuId);

}
