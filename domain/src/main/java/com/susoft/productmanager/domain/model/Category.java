package com.susoft.productmanager.domain.model;

import com.susoft.productmanager.domain.DBConstants;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Ignore;

/**
 * @author Muhammed Sabry
 */
public class Category {

    private static final Category ALL_CATEGORIES;

    private static final Category NO_CATEGORY;

    static {
        ALL_CATEGORIES = new Category("-1", "All categories", 0);
        NO_CATEGORY = new Category("-1", "No category", 0);
    }

    @ColumnInfo(name = DBConstants.Category.ID)
    private int id;

    @ColumnInfo(name = DBConstants.Category.SERVER_ID)
    private String serverId;

    @ColumnInfo(name = DBConstants.Category.VALUE)
    private String value;

    @ColumnInfo(name = DBConstants.Category.LEVEL)
    private int level;

    @ColumnInfo(name = DBConstants.Category.PARENT_ID)
    private int parentId;

    @Ignore
    private List<Category> children = new ArrayList<>();

    public Category() {
    }

    @Ignore
    public Category(String value) {
        this.value = value;
    }

    private Category(int id, String value) {
        this(value);
        this.id = id;
    }

    @Ignore
    public Category(int id, String value, int level) {
        this(id, value);
        this.level = level;
    }

    @Ignore
    private Category(String serverId, String value, int level) {
        this.serverId = serverId;
        this.value = value;
        this.level = level;
    }

    public static Category noCategory() {
        return NO_CATEGORY;
    }

    public static Category allCategories() {
        return ALL_CATEGORIES;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public List<Category> getChildren() {
        return children;
    }

    public void setChildren(List<Category> children) {
        this.children = children;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String name) {
        this.value = name;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof Category) {
            Category that = (Category) obj;
            return that.serverId != null && that.serverId.equals(this.serverId)
                    && that.value != null && that.value.equals(this.value)
                    && that.level == this.level;
        }
        return false;
    }

    @NonNull
    @Override
    public String toString() {
        return "ID: " + id + ", Value: " + value;
    }
}
