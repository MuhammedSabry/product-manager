package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.SingleUseCase;
import com.susoft.productmanager.domain.model.Shop;
import com.susoft.productmanager.domain.service.UserService;

import javax.inject.Inject;

import io.reactivex.Single;

public class GetShopInteractor extends SingleUseCase<Shop, Void> {

    private final UserService userService;

    @Inject
    GetShopInteractor(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, UserService userService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.userService = userService;
    }

    @Override
    protected Single<Shop> interact(Void param) {
        return userService.getShopInfo();
    }
}
