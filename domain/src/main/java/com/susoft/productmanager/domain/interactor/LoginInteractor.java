package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.SingleUseCase;
import com.susoft.productmanager.domain.service.AuthenticationService;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public class LoginInteractor extends SingleUseCase<String, Void> {

    private final AuthenticationService authenticationService;

    private String userId;
    private String shopId;
    private String chain;
    private String password;

    @Inject
    LoginInteractor(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, AuthenticationService authenticationService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.authenticationService = authenticationService;
    }

    public Single<String> execute(String userId, String shopId, String chain, String password) {
        this.userId = userId;
        this.shopId = shopId;
        this.chain = chain;
        this.password = password;
        return super.execute();
    }

    @Override
    protected Single<String> interact(Void param) {
        return authenticationService.login(userId, shopId, chain, password);
    }
}
