package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.FlowableUseCase;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.model.Cell;
import com.susoft.productmanager.domain.service.CellService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;

/**
 * @author Muhammed Sabry
 */
public class GetGridCellsOnline extends FlowableUseCase<List<Cell>, Long> {

    private final CellService cellService;

    @Inject
    GetGridCellsOnline(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, CellService cellService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.cellService = cellService;
    }

    @Override
    protected Flowable<List<Cell>> interact(Long gridId) {
        return cellService.getGridCells(gridId).toFlowable();
    }
}
