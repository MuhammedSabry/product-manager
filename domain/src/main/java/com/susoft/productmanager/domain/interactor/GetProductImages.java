package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.FlowableUseCase;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.model.ProductImage;
import com.susoft.productmanager.domain.service.ProductService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;

public class GetProductImages extends FlowableUseCase<List<ProductImage>, String> {

    private final ProductService productService;

    @Inject
    GetProductImages(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, ProductService productService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.productService = productService;
    }

    @Override
    protected Flowable<List<ProductImage>> interact(String productBarcode) {
        return productService.getProductImages(productBarcode)
                .toFlowable();
    }
}
