package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.SingleUseCase;
import com.susoft.productmanager.domain.model.Cell;
import com.susoft.productmanager.domain.service.CellService;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public class CreateCellOnline extends SingleUseCase<Cell, Cell> {

    private final CellService cellService;

    @Inject
    CreateCellOnline(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, CellService cellService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.cellService = cellService;
    }

    @Override
    protected Single<Cell> interact(Cell cell) {
        return cellService.createCell(cell);
    }
}
