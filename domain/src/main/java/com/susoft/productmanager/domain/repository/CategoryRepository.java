package com.susoft.productmanager.domain.repository;

import com.susoft.productmanager.domain.model.Category;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public interface CategoryRepository {
    Flowable<Long> insert(Category category);

    Flowable<Long> insert(List<Category> category);

    Flowable<List<Category>> getCategories();

    Flowable<List<Category>> getLevelCategories(int level);

    Flowable<List<Category>> getChildCategories(int parentId);

    Completable deleteAllCategories();

    Completable delete(List<Category> category);

    Single<Category> getProductCategories(int level, String serverId);

    Single<Category> getProductCategories(int level, String category2, int parentId);
}
