package com.susoft.productmanager.domain.model;

import java.util.Map;

public class ProductType {
    private int id;
    private static final String ENGLISH = "en";
    private Map<String, String> languageNameMap;

    public int getId() {
        return id;
    }

    public ProductType(int id, Map<String, String> languageNameMap) {
        this.id = id;
        this.languageNameMap = languageNameMap;
    }

    public String getName() {
        return languageNameMap.get("en");
    }

    public String getName(String languageCode) {
        String name = languageNameMap.get(languageCode.equals("nb") ? "no" : languageCode);
        if (name == null)
            name = languageNameMap.get(ENGLISH);
        return name;
    }
}
