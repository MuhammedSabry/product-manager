package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.FlowableUseCase;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.service.UserService;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Flowable;

public class CheckConnectionInteractor extends FlowableUseCase<Boolean, Void> {
    private final UserService userService;

    @Inject
    CheckConnectionInteractor(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, UserService userService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.userService = userService;
    }

    @Override
    protected Flowable<Boolean> interact(Void param) {
        return Flowable.interval(0, 10, TimeUnit.SECONDS)
                .flatMap(aLong -> userService.getShopInfo().toFlowable())
                .map(shop -> true);
    }
}
