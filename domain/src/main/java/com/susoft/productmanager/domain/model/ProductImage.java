package com.susoft.productmanager.domain.model;

import com.susoft.productmanager.domain.DBConstants;

import androidx.room.ColumnInfo;

/**
 * @author Muhammed Sabry
 */
public class ProductImage {

    @ColumnInfo(name = DBConstants.ProductImage.ID)
    private String id;

    @ColumnInfo(name = DBConstants.ProductImage.IS_DEFAULT)
    private boolean isDefault;

    @ColumnInfo(name = DBConstants.ProductImage.IMAGE_BYTES)
    private byte[] image;

    @ColumnInfo(name = DBConstants.ProductImage.MIME)
    private String mime;

    @ColumnInfo(name = DBConstants.ProductImage.SERVER_TABLE)
    private String table;

    private String barcode;

    public ProductImage() {
    }

    public ProductImage(boolean isDefault, byte[] image, String mime, String barcode) {
        this.isDefault = isDefault;
        this.image = image;
        this.mime = mime;
        this.barcode = barcode;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
