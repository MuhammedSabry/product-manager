package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.CompletableUseCase;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.model.ProductImage;
import com.susoft.productmanager.domain.service.ProductService;

import javax.inject.Inject;

import io.reactivex.Completable;

public class AddImageToProduct extends CompletableUseCase<ProductImage> {

    private final ProductService productService;

    @Inject
    AddImageToProduct(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, ProductService productService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.productService = productService;
    }

    @Override
    protected Completable interact(ProductImage image) {
        return productService.addImageToProduct(image);
    }
}
