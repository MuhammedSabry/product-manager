package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.SingleUseCase;
import com.susoft.productmanager.domain.model.Product;
import com.susoft.productmanager.domain.service.ProductService;

import javax.inject.Inject;

import io.reactivex.Single;

public class GetProductByBarcodeInteractor extends SingleUseCase<Product, String> {
    private final ProductService productService;

    @Inject
    GetProductByBarcodeInteractor(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, ProductService productService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.productService = productService;
    }

    @Override
    protected Single<Product> interact(String barcode) {
        return productService.getProductByBarcode(barcode);
    }
}
