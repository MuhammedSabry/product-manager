package com.susoft.productmanager.domain.eventbus;

import com.jakewharton.rxrelay2.BehaviorRelay;
import com.jakewharton.rxrelay2.Relay;

import io.reactivex.Observable;

/**
 * @author Muhammed Sabry
 */
public class EventBus<T extends BaseEvent> {

    private final Relay<T> busSubject = BehaviorRelay.<T>create().toSerialized();

    public Observable<T> register() {
        return busSubject.distinctUntilChanged();
    }

    public void post(T event) {
        busSubject.accept(event);
    }

}
