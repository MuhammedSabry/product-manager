package com.susoft.productmanager.domain.model;

public class Grid {

    private long id;

    private int rows;

    private int columns;

    private long parentCellId;

    public Grid(long id, int columns, int rows, long parentCellId) {
        this.id = id;
        this.rows = rows;
        this.columns = columns;
        this.parentCellId = parentCellId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public long getParentCellId() {
        return parentCellId;
    }

    public void setParentCellId(long parentCellId) {
        this.parentCellId = parentCellId;
    }
}
