package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.CompletableUseCase;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.service.ProductService;

import javax.inject.Inject;

import io.reactivex.Completable;

public class IsAvailableBarcode extends CompletableUseCase<String> {
    private final ProductService productService;

    @Inject
    IsAvailableBarcode(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, ProductService productService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.productService = productService;
    }

    @Override
    protected Completable interact(String barcode) {
        Throwable productExistsThrowable = new Throwable("Product exists");
        return productService.getProductByBarcode(barcode)
                .flatMapCompletable(product -> Completable.error(productExistsThrowable))
                .onErrorResumeNext(throwable -> throwable == productExistsThrowable ?
                        Completable.error(throwable) : Completable.complete());
    }
}
