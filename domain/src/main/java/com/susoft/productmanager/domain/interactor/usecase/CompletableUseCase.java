package com.susoft.productmanager.domain.interactor.usecase;

import io.reactivex.Completable;
import io.reactivex.CompletableTransformer;

/**
 * @author Muhammed Sabry
 */
public abstract class CompletableUseCase<Q> extends UseCase<Completable, Q> {

    private final CompletableTransformer schedulersTransformer;

    protected CompletableUseCase(final BackgroundExecutionThread backgroundExecutionThread, final PostExecutionThread postExecutionThread) {
        super(backgroundExecutionThread, postExecutionThread);
        schedulersTransformer = rObservable -> rObservable
                .subscribeOn(getBackgroundExecutionThread())
                .observeOn(getPostExecutionThread());
    }

    public Completable execute() {
        return execute(null);
    }

    @Override
    public Completable execute(Q param) {
        return interact(param).compose(getSchedulersTransformer());
    }

    @Override
    protected abstract Completable interact(Q param);

    private CompletableTransformer getSchedulersTransformer() {
        return schedulersTransformer;
    }

}
