package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.SingleUseCase;
import com.susoft.productmanager.domain.model.Grid;
import com.susoft.productmanager.domain.service.CellService;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public class GetCellGridOnline extends SingleUseCase<Grid, Long> {

    private final CellService cellService;

    @Inject
    GetCellGridOnline(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, CellService cellService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.cellService = cellService;
    }

    @Override
    protected Single<Grid> interact(Long cellId) {
        return cellService.getCellGrid(cellId);
    }
}
