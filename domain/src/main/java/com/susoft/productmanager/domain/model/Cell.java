package com.susoft.productmanager.domain.model;

import com.susoft.productmanager.domain.DBConstants;

import androidx.room.ColumnInfo;
import androidx.room.Ignore;

/**
 * @author Muhammed Sabry
 */
public class Cell {

    @ColumnInfo(name = DBConstants.Cell.ID)
    private long id;

    private int position;

    @ColumnInfo(name = DBConstants.Cell.IMAGE)
    private byte[] image;

    private String mimeType;

    @ColumnInfo(name = DBConstants.Cell.PARENT_GRID_ID)
    private long parentGridId;

    @ColumnInfo(name = DBConstants.Cell.TEXT)
    private String text;

    @ColumnInfo(name = DBConstants.Cell.PRODUCT_ID)
    private String productId;

    @Ignore
    public Cell(String text, int position, long parentGridId) {
        this.position = position;
        this.parentGridId = parentGridId;
        this.text = text;
    }

    public Cell(long id, int position, byte[] image, String mimeType, long parentGridId, String text, String productId) {
        this.id = id;
        this.position = position;
        this.image = image;
        this.mimeType = mimeType;
        this.parentGridId = parentGridId;
        this.text = text;
        this.productId = productId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public long getParentGridId() {
        return parentGridId;
    }

    public void setParentGridId(long parentGridId) {
        this.parentGridId = parentGridId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
