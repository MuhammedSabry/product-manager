package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.CompletableUseCase;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.service.MenuService;

import javax.inject.Inject;

import io.reactivex.Completable;

/**
 * @author Muhammed Sabry
 */
public class CreateMenuOnline extends CompletableUseCase<Void> {

    private final MenuService menuService;
    private String name;
    private int columns;
    private int rows;

    @Inject
    CreateMenuOnline(BackgroundExecutionThread backgroundExecutionThread, PostExecutionThread postExecutionThread, MenuService menuService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.menuService = menuService;
    }

    public Completable execute(String name, int columns, int rows) {
        this.name = name;
        this.columns = columns;
        this.rows = rows;
        return super.execute();
    }

    @Override
    protected Completable interact(Void param) {
        return menuService.createMenu(name, columns, rows);
    }
}
