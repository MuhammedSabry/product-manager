package com.susoft.productmanager.domain.model;

public class User {

    private String id;

    private boolean isRemembered;

    private String clientName;

    public User(String id, boolean isRemembered, String clientName) {
        this.id = id;
        this.isRemembered = isRemembered;
        this.clientName = clientName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isRemembered() {
        return isRemembered;
    }

    public void setRemembered(boolean remembered) {
        isRemembered = remembered;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
}
