package com.susoft.productmanager.domain.interactor;

import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.CompletableUseCase;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.model.Product;
import com.susoft.productmanager.domain.service.ProductService;

import javax.inject.Inject;

import io.reactivex.Completable;

/**
 * @author Muhammed Sabry
 */
public class InsertProductOnline extends CompletableUseCase<Product> {

    private final ProductService productService;

    @Inject
    InsertProductOnline(BackgroundExecutionThread backgroundExecutionThread,
                        PostExecutionThread postExecutionThread,
                        ProductService productService) {
        super(backgroundExecutionThread, postExecutionThread);
        this.productService = productService;
    }

    @Override
    protected Completable interact(Product param) {
        return productService.createProduct(param);
    }
}
