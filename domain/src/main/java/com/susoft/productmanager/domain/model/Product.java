package com.susoft.productmanager.domain.model;


import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Ignore;

import com.susoft.productmanager.domain.DBConstants;

import java.io.Serializable;

/**
 * @author Muhammed Sabry
 */
public class Product implements Serializable {

    @ColumnInfo(name = DBConstants.Product.ID)
    private String id;

    @ColumnInfo(name = DBConstants.Product.NAME)
    private String name;

    @ColumnInfo(name = DBConstants.Product.DESCRIPTION)
    private String description;

    @ColumnInfo(name = DBConstants.Product.SUPPLIER)
    private String supplier;

    @ColumnInfo(name = DBConstants.Product.BARCODE)
    private String barcode;

    @ColumnInfo(name = DBConstants.Product.SERVING)
    private String serving;

    @ColumnInfo(name = DBConstants.Product.LIST_PRICE)
    private double listPrice;

    @ColumnInfo(name = DBConstants.Product.VENDOR_PRICE)
    private double vendorPrice;

    @ColumnInfo(name = DBConstants.Product.RETAIL_PRICE)
    private double retailPrice;

    @ColumnInfo(name = DBConstants.Product.IN_PRICE)
    private double inPrice;

    @ColumnInfo(name = DBConstants.Product.NET_PRICE)
    private double netPrice;

    @ColumnInfo(name = DBConstants.Product.VAT)
    private double vat;

    @ColumnInfo(name = DBConstants.Product.PROFIT)
    private double profit;

    @ColumnInfo(name = DBConstants.Product.TAKEAWAY_PRICE)
    private double takeawayPrice;

    @ColumnInfo(name = DBConstants.Product.TAKEAWAY_VAT)
    private double takeawayVat;

    @ColumnInfo(name = DBConstants.Product.VENDOR_ID)
    private int vendorId;

    @ColumnInfo(name = DBConstants.Product.PRIORITY)
    private int priority;

    @ColumnInfo(name = DBConstants.Product.COOKING_TIME)
    private int cookingTime;

    @ColumnInfo(name = DBConstants.Product.IS_ACTIVE)
    private boolean isActive;

    @ColumnInfo(name = DBConstants.Product.IS_PRINTABLE)
    private boolean isPrintable;

    @ColumnInfo(name = DBConstants.Product.IS_TAKEAWAY_ACTIVE)
    private boolean isTakeawayActive;

    @ColumnInfo(name = DBConstants.Product.IMAGE, typeAffinity = ColumnInfo.BLOB)
    private byte[] image;

    private Integer type;

    @Ignore
    private boolean hasImage;

    @Embedded(prefix = "category1")
    private Category category1;

    @Embedded(prefix = "category2")
    private Category category2;

    @Embedded(prefix = "category3")
    private Category category3;

    @Embedded(prefix = "category4")
    private Category category4;

    @Embedded(prefix = "category5")
    private Category category5;

    private String catServerId1;

    private String catServerId2;

    private String catServerId3;

    private String catServerId4;

    private String catServerId5;
    private String abcCode;
    private String processLocation;
    @Ignore
    private boolean miscellaneous;

    public Product() {
    }

    @SuppressWarnings("CopyConstructorMissesField")
    public Product(Product product) {
        setId(product.getId());
        setBarcode(product.getBarcode());
        setName(product.getName());
        setVat(product.getVat());
        setRetailPrice(product.getRetailPrice());
        setTakeawayPrice(product.getTakeawayPrice());
        setTakeawayVat(product.getTakeawayVat());
        setHasImage(product.hasImage());
        setDescription(product.getDescription());
        setCatServerId1(product.getCatServerId1());
        setCatServerId2(product.getCatServerId2());
        setCatServerId3(product.getCatServerId3());
        setCatServerId4(product.getCatServerId4());
        setCatServerId5(product.getCatServerId5());
        setProcessLocation(product.getProcessLocation());
        setAbcCode(product.getAbcCode());
        setType(product.getType());
        setMiscellaneous(product.isMiscellaneous());
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getAbcCode() {
        return abcCode;
    }

    public void setAbcCode(String abcCode) {
        this.abcCode = abcCode;
    }

    public String getProcessLocation() {
        return processLocation;
    }

    public void setProcessLocation(String processLocation) {
        this.processLocation = processLocation;
    }

    public String getCatServerId1() {
        return catServerId1;
    }

    public void setCatServerId1(String catServerId1) {
        this.catServerId1 = catServerId1;
    }

    public String getCatServerId2() {
        return catServerId2;
    }

    public void setCatServerId2(String catServerId2) {
        this.catServerId2 = catServerId2;
    }

    public String getCatServerId3() {
        return catServerId3;
    }

    public void setCatServerId3(String catServerId3) {
        this.catServerId3 = catServerId3;
    }

    public String getCatServerId4() {
        return catServerId4;
    }

    public void setCatServerId4(String catServerId4) {
        this.catServerId4 = catServerId4;
    }

    public String getCatServerId5() {
        return catServerId5;
    }

    public void setCatServerId5(String catServerId5) {
        this.catServerId5 = catServerId5;
    }

    public boolean hasImage() {
        return hasImage;
    }

    public void setHasImage(boolean hasImage) {
        this.hasImage = hasImage;
    }

    public Category getCategory1() {
        return category1;
    }

    public void setCategory1(Category category1) {
        this.category1 = category1;
        if (category1 != null)
            setCatServerId1(category1.getServerId());
        else
            setCatServerId1("");
    }

    public Category getCategory2() {
        return category2;
    }

    public void setCategory2(Category category2) {
        this.category2 = category2;
        if (category2 != null)
            setCatServerId2(category2.getServerId());
        else
            setCatServerId2("");
    }

    public Category getCategory3() {
        return category3;
    }

    public void setCategory3(Category category3) {
        this.category3 = category3;
        if (category3 != null)
            setCatServerId3(category3.getServerId());
        else
            setCatServerId3("");
    }

    public Category getCategory4() {
        return category4;
    }

    public void setCategory4(Category category4) {
        this.category4 = category4;
        if (category4 != null)
            setCatServerId4(category4.getServerId());
        else
            setCatServerId4("");
    }

    public Category getCategory5() {
        return category5;
    }

    public void setCategory5(Category category5) {
        this.category5 = category5;
        if (category5 != null)
            setCatServerId5(category5.getServerId());
        else
            setCatServerId5("");
    }

    public boolean isPrintable() {
        return isPrintable;
    }

    public void setPrintable(boolean printable) {
        isPrintable = printable;
    }

    public boolean isTakeawayActive() {
        return isTakeawayActive;
    }

    public void setTakeawayActive(boolean takeawayActive) {
        isTakeawayActive = takeawayActive;
    }

    public double getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(double netPrice) {
        this.netPrice = netPrice;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTakeawayPrice() {
        return takeawayPrice;
    }

    public void setTakeawayPrice(double takeawayPrice) {
        this.takeawayPrice = takeawayPrice;
    }

    public double getTakeawayVat() {
        return takeawayVat;
    }

    public void setTakeawayVat(double takeawayVat) {
        this.takeawayVat = takeawayVat;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getServing() {
        return serving;
    }

    public void setServing(String serving) {
        this.serving = serving;
    }

    public double getListPrice() {
        return listPrice;
    }

    public void setListPrice(double listPrice) {
        this.listPrice = listPrice;
    }

    public double getVendorPrice() {
        return vendorPrice;
    }

    public void setVendorPrice(double vendorPrice) {
        this.vendorPrice = vendorPrice;
    }

    public double getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(double retailPrice) {
        this.retailPrice = retailPrice;
    }

    public double getInPrice() {
        return inPrice;
    }

    public void setInPrice(double inPrice) {
        this.inPrice = inPrice;
    }

    public double getVat() {
        return vat;
    }

    public void setVat(double vat) {
        this.vat = vat;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public int getCookingTime() {
        return cookingTime;
    }

    public void setCookingTime(int cookingTime) {
        this.cookingTime = cookingTime;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public boolean isMiscellaneous() {
        return miscellaneous;
    }

    public void setMiscellaneous(boolean miscellaneous) {
        this.miscellaneous = miscellaneous;
    }
}
