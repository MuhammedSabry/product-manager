package com.susoft.productmanager.domain.repository;

import com.susoft.productmanager.domain.model.Product;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

/**
 * @author Muhammed Sabry
 */
public interface ProductRepository {

    Completable deleteAllProducts();

    Completable insert(Product product);

    Flowable<List<Product>> getProductsByNameAndCategory();

    Flowable<List<Product>> getProductsById(String id);

    Flowable<List<Product>> getProductsByBarcode(String barcode);

}
