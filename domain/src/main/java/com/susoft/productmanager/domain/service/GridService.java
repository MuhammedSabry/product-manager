package com.susoft.productmanager.domain.service;

import com.susoft.productmanager.domain.model.Grid;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * @author Muhammed Sabry
 */
public interface GridService {

    Completable createGrid(Grid grid);

    Completable updateGrid(Grid grid);

    Completable deleteGrid(long gridId);

    Single<Grid> getGrid(long gridId);

}
