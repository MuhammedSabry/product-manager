package com.susoft.productmanager.preferences.base;

import android.content.SharedPreferences;

/**
 * @author Muhammed Sabry
 */
public class StringPreference extends BasePreference {
    private final String defaultValue;

    public StringPreference(SharedPreferences preferences, String key) {
        this(preferences, key, null);
    }

    private StringPreference(SharedPreferences preferences, String key, String defaultValue) {
        super(preferences, key);
        this.defaultValue = defaultValue;
    }

    public String get() {
        return preferences.getString(key, defaultValue);
    }

    public void set(String value) {
        preferences.edit().putString(key, value).apply();
    }
}