package com.susoft.productmanager.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.susoft.productmanager.domain.AuthTokenStorage;
import com.susoft.productmanager.domain.model.Shop;
import com.susoft.productmanager.model.LoginForm;
import com.susoft.productmanager.preferences.base.ObjectPreference;
import com.susoft.productmanager.preferences.base.StringPreference;

import javax.inject.Inject;

import androidx.annotation.Nullable;

/**
 * @author Muhammed Sabry
 */
public class UserAccountStorage implements AuthTokenStorage, SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TOKEN_PREF_KEY = "pref_authentication_token";
    private final StringPreference tokenPreference;
    private final ObjectPreference<LoginForm> loginFormPreference;
    private final ObjectPreference<Shop> shopPreference;
    private OnTokenChangeListener onTokenChangeListener;

    @Inject
    public UserAccountStorage(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        tokenPreference = new StringPreference(preferences, TOKEN_PREF_KEY);
        loginFormPreference = new ObjectPreference<>(preferences, "pref_login_data", new LoginForm(), LoginForm.class);
        shopPreference = new ObjectPreference<>(preferences, "pref_shop", Shop.class);
        preferences.registerOnSharedPreferenceChangeListener(this);
    }

    public String getToken() {
        return tokenPreference.get();
    }

    public void setToken(String version) {
        tokenPreference.set(version);
    }

    @Override
    public void onTokenChanged(OnTokenChangeListener onTokenChangeListener) {
        this.onTokenChangeListener = onTokenChangeListener;
    }

    public void setLoginFormPreference(LoginForm loginForm) {
        loginFormPreference.set(loginForm);
    }

    public LoginForm getLoginForm() {
        return loginFormPreference.get();
    }

    public void clearLoginData() {
        loginFormPreference.set(new LoginForm());
    }

    public void deleteToken() {
        tokenPreference.delete();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(TOKEN_PREF_KEY) && onTokenChangeListener != null)
            onTokenChangeListener.onTokenChanged(getToken());
    }

    @Nullable
    public Shop getShop() {
        return shopPreference.get();
    }

    public void setShop(Shop shop) {
        shopPreference.set(shop);
    }

    public void deleteShop() {
        shopPreference.delete();
    }
}
