package com.susoft.productmanager.util;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

/**
 * @author Muhammed Sbary
 */
public class ActivityStateTracker {
    private static final String TAG = ActivityStateTracker.class.getSimpleName();

    private static final long BACKGROUND_EVENT_DELAY_NORMAL = 1000;
    private static final Handler MAIN_THREAD_HANDLER = new Handler(Looper.getMainLooper());
    private static long BACKGROUND_EVENT_DELAY;
    private final ConnectivityTracker connectivityTracker;

    private int activityCounter = 0;

    private boolean awaitingBackgroundSwitch = false;

    private final Runnable appInBackground = this::onSwitchedToBackground;

    public ActivityStateTracker(ConnectivityTracker connectivityTracker) {
        this.connectivityTracker = connectivityTracker;
        BACKGROUND_EVENT_DELAY = BACKGROUND_EVENT_DELAY_NORMAL;
    }

    public void onActivityResumed() {
        activityCounter++;

        if (activityCounter == 1 && !awaitingBackgroundSwitch) {
            onSwitchedToForeground();
        }

        awaitingBackgroundSwitch = false;
        MAIN_THREAD_HANDLER.removeCallbacks(appInBackground);
        connectivityTracker.notifyChanges();
    }

    public void onActivityPaused() {
        activityCounter--;

        if (isAppInBackground()) {
            awaitingBackgroundSwitch = true;
            MAIN_THREAD_HANDLER.postDelayed(appInBackground, BACKGROUND_EVENT_DELAY);
        }
    }

    private boolean isAppInBackground() {
        return activityCounter == 0;
    }

    private void onSwitchedToForeground() {
        Log.d(TAG, "Application is in FOREGROUND");
        connectivityTracker.trackConnectivityChanges();
    }

    private void onSwitchedToBackground() {
        Log.d(TAG, "Application is in BACKGROUND");
        awaitingBackgroundSwitch = false;
        connectivityTracker.stopConnectivityChangesTracking();
    }
}
