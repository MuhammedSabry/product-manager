package com.susoft.productmanager.util;

import android.text.Editable;

/**
 * @author Muhammed Sbary
 */
public class TextUtils {

    public static String getText(Editable editable) {
        return editable == null ? "" : editable.toString().trim();
    }

    public static int integerOf(String str) {
        return Integer.valueOf(str);
    }

    public static double doubleOf(String str) {
        return Double.valueOf(str);
    }
}
