package com.susoft.productmanager.util;

import com.susoft.productmanager.domain.interactor.GetProductCategories;
import com.susoft.productmanager.domain.interactor.GetProductTypesInteractor;
import com.susoft.productmanager.domain.interactor.GetProductsOnline;
import com.susoft.productmanager.domain.model.Category;
import com.susoft.productmanager.domain.model.Product;
import com.susoft.productmanager.domain.model.ProductType;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * @author Muhammed Sbary
 */
public class PollingHelper {

    private final GetProductsOnline getProductsOnline;
    private final GetProductCategories pollCategories;
    private final GetProductTypesInteractor getProductTypesInteractor;

    @Inject
    PollingHelper(GetProductsOnline getProductsOnline, GetProductCategories pollCategories, GetProductTypesInteractor getProductTypesInteractor) {
        this.getProductsOnline = getProductsOnline;
        this.pollCategories = pollCategories;
        this.getProductTypesInteractor = getProductTypesInteractor;
    }

    public Flowable<Category> fetchCategories() {
        return pollCategories.execute()
                .toFlowable();
    }

    public Single<List<ProductType>> getProductTypes() {
        return getProductTypesInteractor.execute();
    }

    public Flowable<List<Product>> fetchProducts(int page, int pageSize, String query, Category category) {
        return getProductsOnline.execute(page, pageSize, query, category)
                .toFlowable();
    }

    public Flowable<List<Product>> fetchProducts(int page, int pageSize, String query) {
        return fetchProducts(page, pageSize, query, null);
    }
}
