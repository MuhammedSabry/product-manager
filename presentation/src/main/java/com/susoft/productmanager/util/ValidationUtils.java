package com.susoft.productmanager.util;

import android.text.TextUtils;

public class ValidationUtils {

    public static boolean isValidText(String input) {
        return !TextUtils.isEmpty(input);
    }

    public static boolean isValidNumber(String input, long min, long max) {
        try {
            long number = Long.valueOf(input);
            return number >= min && number <= max;
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
    }

    public static boolean isValidDouble(String input, double min, double max) {
        try {
            double number = Double.valueOf(input);
            return number >= min && number <= max;
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
    }

}
