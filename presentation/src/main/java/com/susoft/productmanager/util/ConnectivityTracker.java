package com.susoft.productmanager.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.susoft.productmanager.domain.eventbus.ConnectivityEvent;
import com.susoft.productmanager.domain.eventbus.EventBus;

/**
 * @author Muhammed Sbary
 */
public class ConnectivityTracker extends BroadcastReceiver {

    private final Context context;
    private final EventBus<ConnectivityEvent> eventBus;

    private boolean isConnected;
    private boolean isRegistered = false;

    public ConnectivityTracker(Context context, EventBus<ConnectivityEvent> eventBus) {
        this.context = context;
        this.eventBus = eventBus;

        isConnected = hasInternetConnection(context);
    }

    public static boolean hasInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public void trackConnectivityChanges() {
        if (!isRegistered) {
            IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
            context.registerReceiver(this, filter);
            isRegistered = true;
        }
    }

    public void stopConnectivityChangesTracking() {
        if (isRegistered) {
            context.unregisterReceiver(this);
            isRegistered = false;
        }
    }

    public void notifyChanges() {
        eventBus.post(new ConnectivityEvent(isConnected));
    }

    @Override
    public void onReceive(Context c, Intent intent) {

        boolean connected = hasInternetConnection(context);

        if (connected != isConnected) {

            isConnected = connected;

            notifyChanges();
        }
    }

}
