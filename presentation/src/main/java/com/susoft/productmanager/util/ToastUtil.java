package com.susoft.productmanager.util;

import com.susoft.productmanager.ProductApp;

import es.dmoral.toasty.Toasty;

public class ToastUtil {

    private ToastUtil() {
    }

    public static void shortSuccess(String message) {
        Toasty.success(ProductApp.getContext(), message, Toasty.LENGTH_SHORT).show();
    }

    public static void longSuccess(String message) {
        Toasty.success(ProductApp.getContext(), message, Toasty.LENGTH_LONG).show();
    }

    public static void shortError(String message) {
        Toasty.error(ProductApp.getContext(), message, Toasty.LENGTH_SHORT).show();
    }

    public static void longError(String message) {
        Toasty.error(ProductApp.getContext(), message, Toasty.LENGTH_LONG).show();
    }
}
