package com.susoft.productmanager.model;

/**
 * @author Muhammed Sabry
 */
public interface BaseFeedbackListener {

    void onSuccess(String message);

    void onFail(String message);

}
