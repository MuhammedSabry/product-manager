package com.susoft.productmanager.model;

/**
 * @author Muhammed Sabry
 */
public interface BaseFormListener extends BaseFeedbackListener {

    void onFallBack();

}
