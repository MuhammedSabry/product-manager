package com.susoft.productmanager.model;

import android.text.Editable;

/**
 * @author Muhammed Sabry
 */
public class CreateMenuForm {

    private Editable title;
    private Editable columns;
    private Editable rows;

    public CreateMenuForm(Editable title, Editable columns, Editable rows) {
        this.title = title;
        this.columns = columns;
        this.rows = rows;
    }

    public Editable getColumns() {
        return columns;
    }

    public Editable getRows() {
        return rows;
    }

    public Editable getTitle() {
        return title;
    }

    public interface Listener extends BaseFormListener {
        void onTitleError(String message);

        void onColumnsError(String message);

        void onRowsError(String message);
    }

}

