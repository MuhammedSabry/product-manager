package com.susoft.productmanager.model;

import android.text.Editable;

/**
 * @author Muhammed Sabry
 */
public class CreateCategoryForm {

    private Editable categoryId;
    private Editable category;
    private Editable level;

    public CreateCategoryForm(Editable categoryId, Editable category, Editable level) {
        this.categoryId = categoryId;
        this.category = category;
        this.level = level;
    }

    public Editable getCategory() {
        return category;
    }

    public Editable getCategoryId() {
        return categoryId;
    }

    public Editable getLevel() {
        return level;
    }

    public interface Listener extends BaseFormListener {
        void onIdError(String message);

        void onCategoryError(String message);

        void onLevelError(String message);
    }

}
