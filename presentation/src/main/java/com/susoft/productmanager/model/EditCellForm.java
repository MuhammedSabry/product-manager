package com.susoft.productmanager.model;

import android.graphics.Bitmap;
import android.text.Editable;

/**
 * @author Muhammed Sabry
 */
public class EditCellForm {
    private Bitmap image;
    private Editable title;

    public EditCellForm(Bitmap image, Editable title) {
        this.image = image;
        this.title = title;
    }

    public Editable getTitle() {
        return title;
    }

    public Bitmap getImage() {
        return image;
    }

    public interface Listener extends BaseFormListener {
        void onTitleError(String message);
    }
}
