package com.susoft.productmanager.model;

/**
 * @author Muhammed Sabry
 */
public interface CreateProductListener extends BaseFormListener {

    void onNameError(String message);

    void onBarcodeError(String message);

    void onTakeawayPriceError(String message);

    void onTakeawayVatError(String message);

    void onVatError(String message);

    void onRetailPriceError(String message);

    void onAbcCodeError(String message);

    void onProcessLocationError(String message);
}
