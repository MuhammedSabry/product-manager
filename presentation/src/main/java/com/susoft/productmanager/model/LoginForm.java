package com.susoft.productmanager.model;

/**
 * @author Muhammed Sabry
 */
public class LoginForm {

    private String loginData;
    private String chain;
    private String password;
    private boolean shouldSaveInfo = false;

    public LoginForm() {
    }

    public void setShouldSaveInfo(boolean shouldSaveInfo) {
        this.shouldSaveInfo = shouldSaveInfo;
    }

    public boolean shouldSaveInfo() {
        return shouldSaveInfo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getChain() {
        return chain;
    }

    public void setChain(String chain) {
        this.chain = chain;
    }

    public String getLoginData() {
        return loginData;
    }

    public void setLoginData(String loginData) {
        this.loginData = loginData;
    }

    public void clear() {
        this.loginData = null;
        this.chain = null;
        this.password = null;
    }

    public interface Listener extends BaseFormListener {
        void onLoginDataError(String message);

        void onChainError(String message);

        void onPasswordError(String message);
    }
}
