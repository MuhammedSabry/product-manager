package com.susoft.productmanager.viewmodel;

import android.text.Editable;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.susoft.productmanager.domain.eventbus.CellChangedEvent;
import com.susoft.productmanager.domain.eventbus.ConnectivityEvent;
import com.susoft.productmanager.domain.eventbus.EventBus;
import com.susoft.productmanager.domain.interactor.CreateCellOnline;
import com.susoft.productmanager.domain.interactor.CreateGridOnline;
import com.susoft.productmanager.domain.interactor.CreateMenuOnline;
import com.susoft.productmanager.domain.interactor.DeleteCellOnline;
import com.susoft.productmanager.domain.interactor.GetCellGridOnline;
import com.susoft.productmanager.domain.interactor.GetGridCellsOnline;
import com.susoft.productmanager.domain.interactor.GetGridOnline;
import com.susoft.productmanager.domain.interactor.GetMenusOnline;
import com.susoft.productmanager.domain.interactor.UpdateCellOnline;
import com.susoft.productmanager.domain.model.Category;
import com.susoft.productmanager.domain.model.Cell;
import com.susoft.productmanager.domain.model.Grid;
import com.susoft.productmanager.domain.model.Menu;
import com.susoft.productmanager.domain.model.Product;
import com.susoft.productmanager.model.BaseFormListener;
import com.susoft.productmanager.model.CreateMenuForm;
import com.susoft.productmanager.model.EditCellForm;
import com.susoft.productmanager.util.ImageHelper;
import com.susoft.productmanager.util.PollingHelper;
import com.susoft.productmanager.viewmodel.base.BaseViewModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;

import static com.susoft.productmanager.util.TextUtils.getText;
import static com.susoft.productmanager.util.TextUtils.integerOf;
import static com.susoft.productmanager.util.ValidationUtils.isValidNumber;
import static com.susoft.productmanager.util.ValidationUtils.isValidText;

/**
 * @author Muhammed Sbary
 */
public class QuickMenuViewModel extends BaseViewModel {

    private final PollingHelper pollingHelper;

    //InterActors
    private final GetGridCellsOnline getGridCellsInteractor;
    private final GetCellGridOnline getSubGridInteractor;
    private final CreateMenuOnline createMenuInteractor;
    private final CreateCellOnline createCellInteractor;
    private final UpdateCellOnline updateCellInteractor;
    private final DeleteCellOnline deleteCellInteractor;
    private final CreateGridOnline createGridInteractor;
    private final GetMenusOnline getMenusInteractor;
    private final GetGridOnline getGridInteractor;

    //Disposables
    private Disposable menuCellsSubscription;

    //LiveData for callbacks
    private final MutableLiveData<List<Cell>> cellsLiveData;
    private final MutableLiveData<List<Product>> productSearchLiveData;
    private final MutableLiveData<List<Cell>> cellStackLiveData;
    private final MutableLiveData<List<Menu>> menusLiveData;
    private final MutableLiveData<List<Category>> categoriesLiveData;

    //State variables
    private ObservableList<Cell> cellStack;
    private String productSearchString;
    private Category currentCategory;
    private Cell cellToEdit;
    private Grid currentGrid;
    private Menu currentMenu;

    private boolean isFetchingData = false;
    private int currentPage = 0;
    private boolean isEndOfList = false;

    @Inject
    QuickMenuViewModel(EventBus<ConnectivityEvent> connectivityEventBus,
                       EventBus<CellChangedEvent> cellEventBus,
                       PollingHelper pollingHelper,
                       CreateMenuOnline createMenuInteractor,
                       CreateCellOnline createCellInteractor,
                       UpdateCellOnline updateCellInteractor,
                       DeleteCellOnline deleteCellInteractor,
                       CreateGridOnline createGridInteractor,
                       GetMenusOnline getMenusInteractor,
                       GetGridCellsOnline getGridCellsInteractor,
                       GetGridOnline getGridInteractor,
                       GetCellGridOnline getSubGridInteractor) {
        super(connectivityEventBus);
        this.createMenuInteractor = createMenuInteractor;
        this.createCellInteractor = createCellInteractor;
        this.updateCellInteractor = updateCellInteractor;
        this.deleteCellInteractor = deleteCellInteractor;
        this.createGridInteractor = createGridInteractor;
        this.getMenusInteractor = getMenusInteractor;
        this.getGridCellsInteractor = getGridCellsInteractor;
        this.getGridInteractor = getGridInteractor;
        this.getSubGridInteractor = getSubGridInteractor;
        this.pollingHelper = pollingHelper;
        this.cellsLiveData = new MutableLiveData<>();
        this.productSearchLiveData = new MutableLiveData<>();
        this.cellStackLiveData = new MutableLiveData<>();
        this.menusLiveData = new MutableLiveData<>();
        this.categoriesLiveData = new MutableLiveData<>();
        this.cellStack = new ObservableArrayList<>();
        updateProductsData(true);
        initMenuStack();
        fetchMenus();
        initCellBus(cellEventBus);
    }

    private void initCellBus(EventBus<CellChangedEvent> cellEventBus) {
        addDisposable(cellEventBus.register()
                .subscribe(cellChangedEvent -> refreshCellData()));
    }

    public void refreshData() {
        this.refreshMenuData();
        this.refreshCellData();
    }

    private void refreshCellData() {
        if (cellStack.isEmpty())
            updateGridCells(false, currentMenu.getGridId());
        else
            updateGridCells(true, cellStack.get(cellStack.size() - 1).getId());
    }

    private void initMenuStack() {
        this.cellStack.addOnListChangedCallback(new ObservableList.OnListChangedCallback<ObservableList<Cell>>() {
            @Override
            public void onChanged(ObservableList<Cell> sender) {
                updateGridData();
            }

            @Override
            public void onItemRangeChanged(ObservableList<Cell> sender, int positionStart, int itemCount) {
                onChanged(sender);
            }

            @Override
            public void onItemRangeInserted(ObservableList<Cell> sender, int positionStart, int itemCount) {
                onChanged(sender);
            }

            @Override
            public void onItemRangeMoved(ObservableList<Cell> sender, int fromPosition, int toPosition, int itemCount) {
                onChanged(sender);
            }

            @Override
            public void onItemRangeRemoved(ObservableList<Cell> sender, int positionStart, int itemCount) {
                onChanged(sender);
            }
        });
    }

    private void updateGridData() {
        if (cellStack.isEmpty())
            updateGridCells(false, currentMenu.getGridId());
        else
            updateGridCells(true, cellStack.get(cellStack.size() - 1).getId());
    }

    public LiveData<List<Category>> getCategories() {
        return categoriesLiveData;
    }

    private void fetchMenus() {
        addDisposable(getMenusInteractor.execute()
                .subscribe(menus -> {
                    logSuccess("Loaded menus successfully " + menus.size());
                    menusLiveData.postValue(menus);
                }, throwable -> logError(throwable, "Problem loading menus ")));
    }

    public LiveData<List<Menu>> getMenus() {
        return menusLiveData;
    }

    public LiveData<List<Cell>> getCells() {
        return cellsLiveData;
    }

    private void updateGridCells(boolean isSubGrid, long gridOrCellId) {
        removeDisposable(menuCellsSubscription);

        Single<Grid> gridSingle;

        if (isSubGrid)
            gridSingle = getSubGridInteractor.execute(gridOrCellId);
        else
            gridSingle = getGridInteractor.execute(gridOrCellId);

        menuCellsSubscription = gridSingle.toFlowable()
                .doOnNext(grid -> currentGrid = grid)
                .flatMap(grid -> getGridCellsInteractor.execute(grid.getId()))
                .doOnNext(cells -> cellStackLiveData.setValue(cellStack))
                .subscribe(cellsLiveData::postValue,
                        throwable -> logError(throwable, "Couldn't update grid cells"));

        addDisposable(menuCellsSubscription);
    }

    private Completable createMenu(String name, int columns, int rows) {
        return createMenuInteractor.execute(name, columns, rows);
    }

    private Completable createSubMenu(Grid grid) {
        return createGridInteractor.execute(grid);
    }

    public LiveData<List<Product>> getSearchProducts() {
        return productSearchLiveData;
    }

    public void onQueryChanged(Editable s) {
        if (s == null)
            productSearchString = null;
        else
            productSearchString = s.toString();
        updateProductsData(true);
    }

    private void updateProductsData(boolean resetData) {

        if (resetData) {
            isEndOfList = false;
            currentPage = 0;
        }

        if (isEndOfList)
            return;

        isFetchingData = true;

        int pageSize = 20;

        addDisposable(pollingHelper.fetchProducts(currentPage, pageSize, productSearchString, currentCategory)
                .subscribe(products -> {

                    isFetchingData = false;

                    currentPage++;

                    List<Product> oldValue = productSearchLiveData.getValue();
                    if (oldValue == null || oldValue.isEmpty() || resetData)
                        oldValue = products;
                    else
                        oldValue.addAll(products);

                    productSearchLiveData.postValue(oldValue);
                }, throwable -> {
                    isFetchingData = false;
                    if (throwable instanceof HttpException && throwable.getMessage().contains("no products"))
                        this.isEndOfList = true;
                    logError(throwable, "Problem loading products data because ");
                }));

        addDisposable(pollingHelper.fetchCategories().map(Category::getChildren)
                .subscribe(categoriesLiveData::postValue,
                        throwable -> logError(throwable, "Problem loading categories")));
    }

    public void onCategorySelected(Category category) {
        if (category != Category.allCategories() && category != Category.noCategory())
            currentCategory = category;
        else
            currentCategory = null;
        updateProductsData(true);
    }

    public boolean onMenuSelected(Menu menu) {
        if (!menu.equals(currentMenu)) {
            currentMenu = menu;
            cellStack.clear();
            refreshCellData();
            return true;
        }
        return false;
    }

    public Category getCurrentCategory() {
        return currentCategory;
    }

    public String getSearchString() {
        return productSearchString;
    }

    public void onCellClicked(Cell cell) {
        if (cell != null && !isValidText(cell.getProductId()))
            cellStack.add(cell);
    }

    public void onCreateMenuClicked(int position, CreateMenuForm createMenuForm, CreateMenuForm.Listener listener) {

        boolean isSubMenu = position > -1;

        String title = getText(createMenuForm.getTitle());

        String columns = getText(createMenuForm.getColumns());

        String rows = getText(createMenuForm.getRows());

        boolean isValidTitle = isValidText(title);
        if (!isValidTitle)
            listener.onTitleError("Invalid currentMainMenu title");

        boolean isValidColumns = isValidNumber(columns, 3, 10);
        if (!isValidColumns)
            listener.onColumnsError("Columns must be a number between 3 and 10");

        boolean isValidRows = isValidNumber(rows, 3, 100);
        if (!isValidRows)
            listener.onRowsError("Rows must be a number between 3 and 100");

        boolean isValidMenu = isValidColumns && isValidRows && (isValidTitle || isSubMenu);

        if (isValidMenu) {
            if (isSubMenu) {

                addDisposable(createCellWithGrid(position, integerOf(columns), integerOf(rows))
                        .subscribe(() -> {
                                    listener.onSuccess("Sub menu created successfully");
                                    logSuccess("Sub menu created successfully ");
                                    refreshCellData();
                                },
                                throwable -> {
                                    logError(throwable, "Adding SubMenu error ");
                                    listener.onFail(throwable.getMessage());
                                }));

            } else {
                addDisposable(createMenu(title, integerOf(columns), integerOf(rows))
                        .subscribe(() -> {
                                    listener.onSuccess("Menu added successfully");
                                    logSuccess("Menu created successfully ");
                                    refreshMenuData();
                                },
                                throwable -> {
                                    logError(throwable, "Menu insertion error ");
                                    listener.onFail(throwable.getMessage());
                                }));
            }
        } else
            listener.onFallBack();

    }

    private void refreshMenuData() {
        fetchMenus();
    }

    public LiveData<List<Cell>> getCellStackLiveData() {
        return cellStackLiveData;
    }

    public Menu getCurrentMenu() {
        return currentMenu;
    }

    public boolean onStackClicked(Cell cell) {
        if (!isCellOnTopOfStack(cell)) {
            while (!cellStack.isEmpty()) {
                if (isCellOnTopOfStack(cell))
                    break;
                cellStack.remove(cellStack.size() - 1);
            }
            return true;
        }
        return false;
    }

    private boolean isCellOnTopOfStack(Cell cell) {
        return cellStack.get(cellStack.size() - 1) == cell;
    }

    public boolean onHomeMenuClicked() {
        if (cellStack.isEmpty()) {
            return false;
        } else {
            cellStack.clear();
            return true;
        }
    }

    public void onProductDraggedOverCell(int position, Product product) {
        Cell cell = new Cell(product.getName(), position, currentGrid.getId());
        cell.setProductId(product.getId());
        createCellWithProduct(cell);
    }


    private Single<Cell> createCell(Cell cell) {
        return createCellInteractor.execute(cell)
                .doOnSuccess((createdCell) -> logSuccess("Cell created successfully"))
                .doOnError(throwable -> logError(throwable, "Error creating cell "));
    }

    private void createCellWithProduct(Cell cell) {
        addDisposable(createCell(cell)
                .ignoreElement()
                .subscribe(() -> {
                            logSuccess("Cell created successfully");
                            refreshCellData();
                        },
                        throwable -> logError(throwable, "Error creating cell ")));
    }

    private Completable createCellWithGrid(int position, int cols, int rows) {
        Cell newCell = new Cell("", position, currentGrid.getId());
        return createCell(newCell)
                .flatMapCompletable(createdCell -> createSubMenu(new Grid(0, cols, rows, createdCell.getId())));
    }

    public Cell getCellToEdit() {
        return cellToEdit;
    }

    public void setCellToEdit(Cell cell) {
        this.cellToEdit = cell;
    }

    public void onDeleteClicked(@NonNull Cell currentCell, BaseFormListener listener) {
        addDisposable(deleteCellInteractor.execute(currentCell.getId())
                .subscribe(() -> {
                            logSuccess("Delete cell succeeded");
                            listener.onSuccess("Cell deleted successfully");
                            refreshCellData();
                        },
                        throwable -> {
                            listener.onFail(throwable.getMessage());
                            logError(throwable, "Deleting cell failed due to ");
                        }));
    }

    public void onEditDialogCreateClicked(EditCellForm editCellForm, EditCellForm.Listener listener) {
        Cell cell = cellToEdit;

        String mimeType = cell.getMimeType();
        byte[] image = null;

        if (editCellForm.getImage() != null) {
            image = ImageHelper.getBytesFromBitmap(editCellForm.getImage());
            mimeType = ImageHelper.getMimeType();
        }

        String title = getText(editCellForm.getTitle());

        if (!isValidText(title)) {
            listener.onTitleError("Title cannot be empty");
            listener.onFallBack();
        } else {
            cell.setImage(image);
            cell.setText(title);
            cell.setMimeType(mimeType);

            addDisposable(updateCellInteractor.execute(cell)
                    .subscribe(() -> {
                                setCellToEdit(null);
                                listener.onSuccess("Cell editing successfully");
                                refreshCellData();
                            },
                            throwable -> {
                                logError(throwable, "Cell editing error ");
                                listener.onFail(throwable.getMessage());
                            }));
        }
    }

    public boolean shouldOpenEditCellDialog() {
        return cellToEdit != null;
    }

    public boolean isLoading() {
        return isFetchingData;
    }

    public void onEndOfListReached() {
        if (!isFetchingData)
            updateProductsData(false);
    }

    public Grid getCurrentGrid() {
        return currentGrid;
    }

}
