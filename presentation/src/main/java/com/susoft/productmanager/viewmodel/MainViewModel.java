package com.susoft.productmanager.viewmodel;

import com.susoft.productmanager.domain.eventbus.ConnectivityEvent;
import com.susoft.productmanager.domain.eventbus.DeviceOfflineEvent;
import com.susoft.productmanager.domain.eventbus.EventBus;
import com.susoft.productmanager.domain.interactor.CheckConnectionInteractor;
import com.susoft.productmanager.model.BaseFeedbackListener;
import com.susoft.productmanager.viewmodel.base.BaseViewModel;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class MainViewModel extends BaseViewModel {

    private final CheckConnectionInteractor checkConnectionInteractor;
    private BaseFeedbackListener listener;
    private Disposable connectionRetrySubscription;

    @Inject
    MainViewModel(EventBus<ConnectivityEvent> eventBus,
                  EventBus<DeviceOfflineEvent> deviceOfflineEventEventBus,
                  CheckConnectionInteractor checkConnectionInteractor) {
        super(eventBus);
        this.checkConnectionInteractor = checkConnectionInteractor;
        addDisposable(deviceOfflineEventEventBus.register()
                .subscribe(deviceOfflineEvent -> onConnectivityChanged(deviceOfflineEvent.isOffline())));
    }

    public void setListener(BaseFeedbackListener listener) {
        this.listener = listener;
    }

    private void onConnectivityChanged(boolean isOffline) {
        if (listener != null) {
            if (isOffline)
                listener.onFail("");
            else
                listener.onSuccess("");
        }
    }

    @Override
    public void accept(ConnectivityEvent connectivityEvent) {
        super.accept(connectivityEvent);
        onConnectivityChanged(!connectivityEvent.hasInternetConnection());
    }

    public void retryToConnect(BaseFeedbackListener listener) {
        removeDisposable(connectionRetrySubscription);
        connectionRetrySubscription = checkConnectionInteractor.execute()
                .subscribe((onNext) -> {
                            listener.onSuccess("Connection established successfully");
                            logSuccess("Connection retry succeeded");
                        },
                        error -> {
                            listener.onFail("Couldn't connect to server");
                            logError(error, "Connection retry failed");
                        });
        addDisposable(connectionRetrySubscription);
    }
}
