package com.susoft.productmanager.viewmodel;

import android.text.TextUtils;

import com.susoft.productmanager.domain.eventbus.ConnectivityEvent;
import com.susoft.productmanager.domain.eventbus.EventBus;
import com.susoft.productmanager.domain.interactor.GetShopInteractor;
import com.susoft.productmanager.domain.interactor.LoginInteractor;
import com.susoft.productmanager.domain.model.Shop;
import com.susoft.productmanager.model.LoginForm;
import com.susoft.productmanager.preferences.UserAccountStorage;
import com.susoft.productmanager.util.ValidationUtils;
import com.susoft.productmanager.viewmodel.base.BaseViewModel;

import java.util.regex.Pattern;

import javax.inject.Inject;

import retrofit2.HttpException;

import static com.susoft.productmanager.util.ValidationUtils.isValidNumber;
import static com.susoft.productmanager.util.ValidationUtils.isValidText;

/**
 * @author Muhammed Sbary
 */
public class LoginViewModel extends BaseViewModel {
    private final LoginInteractor loginInteractor;
    private final UserAccountStorage userAccountStorage;
    private final GetShopInteractor getShopInteractor;

    private final LoginForm loginForm;

    @Inject
    LoginViewModel(EventBus<ConnectivityEvent> eventBus,
                   LoginInteractor loginInteractor,
                   UserAccountStorage userAccountStorage, GetShopInteractor getShopInteractor) {
        super(eventBus);
        this.loginInteractor = loginInteractor;
        this.userAccountStorage = userAccountStorage;
        this.loginForm = userAccountStorage.getLoginForm();
        this.getShopInteractor = getShopInteractor;
        this.userAccountStorage.deleteToken();
        this.userAccountStorage.deleteShop();
    }

    public void onLoginDataChanged(String loginData) {
        loginForm.setLoginData(loginData);
    }

    public void onChainChanged(String chain) {
        loginForm.setChain(chain);
    }

    public void onPasswordChanged(String password) {
        loginForm.setPassword(password);
    }

    public void onSaveInfoCheckChanged(boolean isChecked) {
        loginForm.setShouldSaveInfo(isChecked);
    }

    public void onLoginClicked(LoginForm.Listener listener) {
        String loginData = loginForm.getLoginData();

        String userId = "";
        String chain = loginForm.getChain();
        String password = loginForm.getPassword();
        String shopId = "";

        boolean isLoginDataValid;
        if (isValidText(loginData) && loginData.contains(".")) {

            String[] idAndShop = TextUtils.split(loginData, Pattern.quote("."));
            userId = idAndShop[0];
            shopId = idAndShop[1];

            isLoginDataValid = isValidNumber(userId, 1, 100000) && isValidNumber(shopId, 1, 100000);
        } else
            isLoginDataValid = false;

        if (!isLoginDataValid)
            listener.onLoginDataError("Invalid login data");

        boolean isValidChain = ValidationUtils.isValidText(chain);
        if (!isValidChain)
            listener.onChainError("Chain cannot be empty");

        boolean isValidPassword = ValidationUtils.isValidText(password);
        if (!isValidPassword)
            listener.onPasswordError("Password cannot be empty");

        boolean isValidLogin = isValidChain && isValidPassword && isLoginDataValid;

        if (isValidLogin && isOnline())
            addDisposable(loginInteractor.execute(userId, shopId, chain, password)
                    .doOnSuccess(userAccountStorage::setToken)
                    .flatMap(token -> getShopInteractor.execute())
                    .subscribe(shop -> onLoginSuccess(listener, shop),
                            error -> {
                                if (isUnauthorized(error))
                                    listener.onFail("Incorrect login information");
                                else
                                    listener.onFail(error.getMessage());
                            }));

        else if (!isOnline())
            listener.onFail("No internet connection");

        else
            listener.onFallBack();
    }

    private boolean isUnauthorized(Throwable throwable) {
        return throwable instanceof HttpException && ((HttpException) throwable).code() == 401;
    }

    private void onLoginSuccess(LoginForm.Listener listener, Shop shop) {
        userAccountStorage.setShop(shop);
        listener.onSuccess("Welcome back " + shop.getName());
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        saveLoginData();
    }

    private void saveLoginData() {
        if (loginForm.shouldSaveInfo())
            userAccountStorage.setLoginFormPreference(loginForm);
        else
            userAccountStorage.clearLoginData();
    }

    public LoginForm getLoginForm() {
        return loginForm;
    }

}
