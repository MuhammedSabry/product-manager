package com.susoft.productmanager.viewmodel.base;

import android.util.Log;

import com.susoft.productmanager.domain.eventbus.ConnectivityEvent;
import com.susoft.productmanager.domain.eventbus.EventBus;

import javax.annotation.Nullable;
import javax.inject.Inject;

import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * @author Muhammed Sabry
 */
public class BaseViewModel extends ViewModel implements Consumer<ConnectivityEvent> {

    private final String TAG = getClass().getSimpleName();
    private final EventBus<ConnectivityEvent> eventBus;
    private final CompositeDisposable disposables;
    private boolean isOnline = true;

    @Inject
    protected BaseViewModel(EventBus<ConnectivityEvent> eventBus) {
        super();
        this.eventBus = eventBus;
        this.disposables = new CompositeDisposable();
        initEventBus();
    }

    private void initEventBus() {
        disposables.add(eventBus.register().subscribe(this));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposables != null)
            disposables.dispose();
    }

    protected void addDisposable(Disposable disposable) {
        disposables.add(disposable);
    }

    protected void removeDisposable(@Nullable Disposable disposable) {
        if (disposable != null)
            disposables.remove(disposable);
    }

    protected boolean isOnline() {
        return isOnline;
    }

    @Override
    public void accept(ConnectivityEvent connectivityEvent) {
        isOnline = connectivityEvent.hasInternetConnection();
    }

    protected void logSuccess(String message) {
        Log.d(TAG, message);
    }

    protected void logError(Throwable throwable, String message) {
        Log.e(TAG, message + " error: " + throwable.getMessage());
    }
}
