package com.susoft.productmanager.viewmodel;

import android.graphics.Bitmap;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MutableLiveData;

import com.susoft.productmanager.domain.eventbus.ConnectivityEvent;
import com.susoft.productmanager.domain.eventbus.EventBus;
import com.susoft.productmanager.domain.interactor.AddImageToProduct;
import com.susoft.productmanager.domain.interactor.CreateCategoryOnline;
import com.susoft.productmanager.domain.interactor.DeleteCategoryOnline;
import com.susoft.productmanager.domain.interactor.GetProductByBarcodeInteractor;
import com.susoft.productmanager.domain.interactor.GetProductImages;
import com.susoft.productmanager.domain.interactor.InsertProductOnline;
import com.susoft.productmanager.domain.interactor.IsAvailableBarcode;
import com.susoft.productmanager.domain.interactor.UpdateCategoryOnline;
import com.susoft.productmanager.domain.interactor.UpdateProductOnline;
import com.susoft.productmanager.domain.model.Category;
import com.susoft.productmanager.domain.model.Product;
import com.susoft.productmanager.domain.model.ProductImage;
import com.susoft.productmanager.domain.model.ProductType;
import com.susoft.productmanager.model.BaseFeedbackListener;
import com.susoft.productmanager.model.CreateCategoryForm;
import com.susoft.productmanager.model.CreateProductListener;
import com.susoft.productmanager.preferences.UserAccountStorage;
import com.susoft.productmanager.util.ImageHelper;
import com.susoft.productmanager.util.PollingHelper;
import com.susoft.productmanager.util.ValidationUtils;
import com.susoft.productmanager.viewmodel.base.BaseViewModel;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;

import static com.susoft.productmanager.util.TextUtils.doubleOf;
import static com.susoft.productmanager.util.TextUtils.getText;
import static com.susoft.productmanager.util.TextUtils.integerOf;

/**
 * @author Muhammed Sbary
 */
public class CreateProductViewModel extends BaseViewModel {

    /**
     * Delay till the server is called with the given barcode to check if a product is available or not
     * Note: Unit is millisecond
     */
    private static final int PRODUCT_AVAILABILITY_CHECK_DELAY = 500;
    private static final int CATEGORY_ID_LENGTH = 5;
    private static final int CATEGORY_NAME_LIMIT = 30;
    //Interactors
    private final CreateCategoryOnline createCategoryInteractor;
    private final UpdateCategoryOnline updateCategoryInteractor;
    private final DeleteCategoryOnline deleteCategoryInteractor;
    private final AddImageToProduct addImageToProductInteractor;
    private final InsertProductOnline insertProductInteractor;
    private final UpdateProductOnline updateProductInteractor;
    private final GetProductImages getProductImagesInteractor;
    private final IsAvailableBarcode isAvailableBarcodeInteractor;
    private final GetProductByBarcodeInteractor getProductByBarcodeInteractor;

    private final PollingHelper pollingHelper;
    private final UserAccountStorage userAccountStorage;

    private final MutableLiveData<List<Product>> productNameSearch;
    private final MutableLiveData<List<Category>> categoriesLiveData;
    private final MutableLiveData<List<ProductType>> productTypesLiveData;

    private Disposable searchSubscription;
    private boolean isInEditMode = false;

    private Product product;
    private Category category1;
    private Category category2;
    private String nameField;
    private String barcodeField;
    private String retailPriceField;
    private String vatField;
    private String takeawayVatField;
    private String takeawayPriceField;
    private boolean isActive;
    private byte[] image;
    private String description;
    private String searchQuery;
    private String abcCode;
    private String processLocation;
    private Disposable typesAndCategoriesDisposable;
    private Disposable isBarcodeAvailableDisposable;
    private Disposable productByBarcodeDisposable;
    private CreateProductListener productFieldListener;
    private int productType;
    private boolean isMiscellaneous;

    @Inject
    CreateProductViewModel(InsertProductOnline insertProductInteractor,
                           PollingHelper pollingHelper,
                           EventBus<ConnectivityEvent> eventBus,
                           UpdateProductOnline updateProductInteractor,
                           CreateCategoryOnline createCategoryInteractor,
                           UpdateCategoryOnline updateCategoryInteractor,
                           DeleteCategoryOnline deleteCategoryInteractor,
                           AddImageToProduct addImageToProductInteractor,
                           GetProductImages getProductImagesInteractor,
                           IsAvailableBarcode isAvailableBarcodeInteractor,
                           GetProductByBarcodeInteractor getProductByBarcodeInteractor,
                           UserAccountStorage userAccountStorage) {
        super(eventBus);
        this.insertProductInteractor = insertProductInteractor;
        this.updateProductInteractor = updateProductInteractor;
        this.createCategoryInteractor = createCategoryInteractor;
        this.updateCategoryInteractor = updateCategoryInteractor;
        this.deleteCategoryInteractor = deleteCategoryInteractor;
        this.addImageToProductInteractor = addImageToProductInteractor;
        this.getProductImagesInteractor = getProductImagesInteractor;
        this.isAvailableBarcodeInteractor = isAvailableBarcodeInteractor;
        this.getProductByBarcodeInteractor = getProductByBarcodeInteractor;
        this.userAccountStorage = userAccountStorage;
        this.productNameSearch = new MutableLiveData<>();
        this.categoriesLiveData = new MutableLiveData<>();
        this.productTypesLiveData = new MutableLiveData<>();
        this.pollingHelper = pollingHelper;
        resetDefaultValues();
        refreshData();
    }

    public void acceptListener(CreateProductListener listener) {
        this.productFieldListener = listener;
    }


    public void onSearchQueryChanged(String query) {
        this.searchQuery = query;
        updateSearchProducts();
    }

    public void updateSearchProducts() {
        removeDisposable(searchSubscription);

        searchSubscription = pollingHelper.fetchProducts(0, 30, searchQuery)
                .subscribe(productNameSearch::postValue,
                        throwable -> logError(throwable, "Search result error "));

        addDisposable(searchSubscription);
    }

    private Completable insertProduct(Product product) {
        return insertProductInteractor.execute(product);
    }

    private Completable updateProduct(Product product) {
        return updateProductInteractor.execute(product);
    }

    private Completable createCategory(Category category) {
        if (category.getLevel() == 2)
            return createCategoryInteractor.execute(category1.getServerId(), category.getServerId(), category.getValue());
        else
            return createCategoryInteractor.execute(category.getServerId(), null, category.getValue());
    }

    private Completable updateCategory(Category category) {
        if (category.getLevel() == 2)
            return updateCategoryInteractor.execute(category1.getServerId(), category.getServerId(), category.getValue());
        else
            return updateCategoryInteractor.execute(category.getServerId(), null, category.getValue());
    }

    private Completable deleteCategory(Category category) {
        if (category.getLevel() == 2)
            return deleteCategoryInteractor.execute(category1.getServerId(), category.getServerId());
        else
            return deleteCategoryInteractor.execute(category.getServerId(), null);
    }

    public void createCategory(CreateCategoryForm categoryForm, boolean isNewCategory, CreateCategoryForm.Listener listener) {

        String categoryId = getText(categoryForm.getCategoryId());

        String categoryName = getText(categoryForm.getCategory());

        String level = getText(categoryForm.getLevel());

        boolean isValidId = ValidationUtils.isValidText(categoryId) && categoryId.length() <= CATEGORY_ID_LENGTH;
        if (!isValidId) {
            if (!ValidationUtils.isValidText(categoryId))
                listener.onIdError("Invalid category id");
            else
                listener.onIdError("Id can't exceed 5 letters");
        }

        boolean isValidLevel = ValidationUtils.isValidNumber(level, 1, 5);
        if (!isValidLevel)
            listener.onLevelError("Invalid level number. Must be between 1 and 5");

        boolean isValidCategoryName = ValidationUtils.isValidText(categoryName) && categoryName.length() <= CATEGORY_NAME_LIMIT;
        if (!isValidCategoryName) {
            if (!ValidationUtils.isValidText(categoryName))
                listener.onCategoryError("Invalid category name");
            else
                listener.onCategoryError("Name can't exceed 30 letters");
        }

        boolean isValidCategory = isValidId && isValidLevel && isValidCategoryName;

        if (isValidCategory) {
            Category category = new Category();

            category.setServerId(categoryId);
            category.setLevel(integerOf(level));
            category.setValue(categoryName);

            if (isNewCategory)
                addDisposable(createCategory(category)
                        .subscribe(() -> {
                                    listener.onSuccess("Category added successfully");
                                    logSuccess("Category created successfully");
                                },
                                throwable -> {
                                    logError(throwable, "Couldn't create category");
                                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 400)
                                        listener.onFail("Invalid category id");
                                    else
                                        listener.onFail(throwable.getMessage());
                                }));
            else
                addDisposable(updateCategory(category)
                        .subscribe(() -> {
                                    listener.onSuccess("Category updated successfully");
                                    logSuccess("Category updated successfully");
                                },
                                throwable -> {
                                    logError(throwable, "Couldn't update category");
                                    listener.onFail(throwable.getMessage());
                                }));

        } else
            listener.onFallBack();
    }

    public void onCategoryDeleteClicked(int level, BaseFeedbackListener listener) {
        Category categoryToDelete;

        if (level == 1)
            categoryToDelete = category1;
        else
            categoryToDelete = category2;

        addDisposable(deleteCategory(categoryToDelete)
                .subscribe(() -> {
                            resetDeletedCategory(level);
                            listener.onSuccess("Category deleted successfully");
                            logSuccess("Category deleted successfully");
                        },
                        throwable -> {
                            logError(throwable, "Category couldn't be deleted");
                            listener.onFail(throwable.getMessage());
                        }));
    }

    private void resetDeletedCategory(int level) {
        if (level == 1)
            category1 = Category.noCategory();
        else
            category2 = Category.noCategory();
    }

    public boolean isInEditMode() {
        return isInEditMode;
    }

    public Product getProduct() {
        return product;
    }

    public void setName(String name) {
        this.nameField = name;
        validateNameField();
    }

    public void setBarcode(String barcode) {
        this.barcodeField = barcode;
        validateBarcodeField();
    }

    public void isBarcodeAvailable(String barcode, BaseFeedbackListener listener) {
        removeDisposable(isBarcodeAvailableDisposable);
        if (isValidBarcode() && !isInEditMode()) {
            isBarcodeAvailableDisposable =
                    Completable.timer(PRODUCT_AVAILABILITY_CHECK_DELAY,
                            TimeUnit.MICROSECONDS)
                            .concatWith(isAvailableBarcodeInteractor.execute(barcode))
                            .subscribe(() -> listener.onSuccess(""),
                                    throwable -> listener.onFail("Barcode is used by another product"));
            addDisposable(isBarcodeAvailableDisposable);
        } else
            listener.onSuccess("");
    }

    public void setRetailPrice(String retailPrice) {
        this.retailPriceField = retailPrice;
        validateRetailPriceField();
    }

    public void setVat(String vat) {
        this.vatField = vat;
        validateVatField();
    }

    public void setTakeawayVat(String takeawayVat) {
        this.takeawayVatField = takeawayVat;
        validateTakeawayVatField();
    }

    public void setTakeawayPrice(String takeawayPrice) {
        this.takeawayPriceField = takeawayPrice;
        validateTakeawayPriceField();
    }

    public void setAbcCode(String abcCode) {
        this.abcCode = abcCode;
        this.validateAbcField();
    }

    public void setProcessLocation(String processLocation) {
        this.processLocation = processLocation;
        this.validateProcessLocationField();
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public void setCategory1(Category category1) {
        this.category1 = category1;
    }

    public void setCategory2(Category category2) {
        this.category2 = category2;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void createProduct() {

        boolean isValidProduct = validateProductFields();

        if (isValidProduct) {
            if (product == null || !isInEditMode)
                product = new Product();
            product.setName(nameField);
            product.setBarcode(barcodeField);

            product.setRetailPrice(doubleOf(retailPriceField));
            product.setVat(doubleOf(vatField));

            product.setTakeawayVat(doubleOf(takeawayVatField));
            product.setTakeawayPrice(doubleOf(takeawayPriceField));

            product.setActive(isActive);
            product.setImage(image);
            product.setDescription(description);
            product.setAbcCode(abcCode != null ? abcCode.toUpperCase() : null);
            product.setProcessLocation(processLocation);
            product.setType(productType);
            product.setMiscellaneous(isMiscellaneous);

            setProductCategories();

            ProductImage productImage = new ProductImage(true, image, ImageHelper.getMimeType(), barcodeField);

            if (!isInEditMode) {

                addDisposable(insertProduct(product)
                        .doOnComplete(() -> {
                            if (image != null)
                                addDisposable(addImageToProductInteractor.execute(productImage)
                                        .subscribe(() -> logSuccess("Product image added"),
                                                throwable -> logError(throwable, "Product Image couldn't be added")));
                        })
                        .subscribe(() -> {
                                    productFieldListener.onSuccess("Created product successfully");
                                    logSuccess("Created product successfully " + product);
                                    updateSearchProducts();
                                },
                                throwable -> {
                                    logError(throwable, "Product couldn't be created");
                                    productFieldListener.onFail(throwable.getMessage());
                                }));
            } else
                addDisposable(updateProduct(product)
                        .doOnComplete(() -> {
                            if (image != null)
                                addDisposable(addImageToProductInteractor.execute(productImage)
                                        .subscribe(() -> logSuccess("Product image added"),
                                                throwable -> logError(throwable, "Product Image couldn't be added")));
                        })
                        .subscribe(() -> {
                                    productFieldListener.onSuccess("Product updated successfully");
                                    logSuccess("Product updated " + product);
                                    updateSearchProducts();
                                },
                                throwable -> {
                                    logError(throwable, "Product couldn't be updated");
                                    productFieldListener.onFail(throwable.getMessage());
                                }));
        } else
            productFieldListener.onFallBack();
    }

    private boolean validateProductFields() {
        if (this.productFieldListener == null)
            return false;
        boolean isValidName = validateNameField();

        boolean isValidBarcode = validateBarcodeField();

        boolean isValidRetailPrice = validateRetailPriceField();

        boolean isValidVat = validateVatField();

        boolean isValidTakeawayVat = validateTakeawayVatField();

        boolean isValidTakeawayPrice = validateTakeawayPriceField();

        boolean isValidAbc = validateAbcField();

        boolean isValidProcessLocation = validateProcessLocationField();

        return isValidName &&
                isValidBarcode &&
                isValidRetailPrice &&
                isValidVat &&
                isValidTakeawayVat &&
                isValidTakeawayPrice &&
                isValidAbc &&
                isValidProcessLocation;
    }

    private boolean validateProcessLocationField() {
        if (productFieldListener == null)
            return false;
        boolean isValidProcessLocation = !ValidationUtils.isValidText(processLocation) || processLocation.length() < 6;
        if (!isValidProcessLocation)
            productFieldListener.onProcessLocationError("Must not exceed 6 letters");
        return isValidProcessLocation;
    }

    private boolean validateAbcField() {
        if (productFieldListener == null)
            return false;
        boolean isValidAbc = !ValidationUtils.isValidText(abcCode) || (abcCode.length() == 1
                && Pattern.matches("[A-Z]", abcCode));
        if (!isValidAbc)
            productFieldListener.onAbcCodeError("Must be 1 letter from A-Z");
        return isValidAbc;
    }

    private boolean validateTakeawayPriceField() {
        if (productFieldListener == null)
            return false;
        boolean isValidTakeawayPrice = isValidTakeawayPrice();
        if (!isValidTakeawayPrice)
            productFieldListener.onTakeawayPriceError("Invalid takeaway price");
        return isValidTakeawayPrice;
    }

    private boolean validateTakeawayVatField() {
        if (productFieldListener == null)
            return false;
        boolean isValidTakeawayVat = isValidTakeawayVat();
        if (!isValidTakeawayVat)
            productFieldListener.onTakeawayVatError("Invalid takeaway VAT");
        return isValidTakeawayVat;
    }

    private boolean validateVatField() {
        if (productFieldListener == null)
            return false;
        boolean isValidVat = isValidVat();
        if (!isValidVat)
            productFieldListener.onVatError("VAT Must be between 0 and 100");
        return isValidVat;
    }

    private boolean validateRetailPriceField() {
        if (productFieldListener == null)
            return false;
        boolean isValidRetailPrice = isValidRetailPrice();
        if (!isValidRetailPrice)
            productFieldListener.onRetailPriceError("Invalid retail price");
        return isValidRetailPrice;
    }

    private boolean validateBarcodeField() {
        if (productFieldListener == null)
            return false;
        boolean isValidBarcode = isValidBarcode();
        if (!isValidBarcode) {
            if (ValidationUtils.isValidText(barcodeField))
                productFieldListener.onBarcodeError("Only numbers 0-9 & capital letters A-Z allowed.");
            else
                productFieldListener.onBarcodeError("Invalid barcode");
        }
        return isValidBarcode;
    }

    private boolean validateNameField() {
        if (productFieldListener == null)
            return false;
        boolean isValidName = ValidationUtils.isValidText(nameField);
        if (!isValidName)
            productFieldListener.onNameError("Invalid product name");
        return isValidName;
    }

    public boolean isValidBarcode() {
        return ValidationUtils.isValidText(barcodeField) && Pattern.matches("[0-9A-Z]+", barcodeField);
    }

    private boolean isValidTakeawayPrice() {
        return ValidationUtils.isValidDouble(takeawayPriceField, 0, Double.MAX_VALUE);
    }

    private boolean isValidTakeawayVat() {
        return ValidationUtils.isValidDouble(takeawayVatField, 0, 100);
    }

    private boolean isValidVat() {
        return ValidationUtils.isValidDouble(vatField, 0, 100);
    }

    private boolean isValidRetailPrice() {
        return ValidationUtils.isValidDouble(retailPriceField, 0, Double.MAX_VALUE);
    }

    private void setProductCategories() {
        if (category1 != Category.noCategory())
            product.setCategory1(category1);
        else
            product.setCategory1(null);

        if (category2 != Category.noCategory() && category1 != Category.noCategory())
            product.setCategory2(category2);
        else
            product.setCategory2(null);
    }

    public LiveData<Product> onProductSelected(Product selectedProduct) {
        this.product = new Product(selectedProduct);
        isInEditMode = true;
        return LiveDataReactiveStreams.fromPublisher(Flowable.just(this.product)
                .map(this::productCategoryMapper)
                .onErrorReturnItem(this.product)
                .first(this.product)
                .flatMap(this::defaultProductImageMapper)
                .doOnSuccess(this::setupStateWithProduct)
                .toFlowable());
    }

    private void setupStateWithProduct(Product product1) {
        this.category1 = product1.getCategory1();
        this.category2 = product1.getCategory2();
        this.productType = product1.getType();
    }

    private Single<Product> defaultProductImageMapper(Product product) {
        return getProductImagesInteractor.execute(product.getBarcode())
                .flatMapIterable(productImages -> productImages)
                .filter(ProductImage::isDefault)
                .map(productImage -> {
                    product.setImage(productImage.getImage());
                    return product;
                })
                .firstOrError()
                .onErrorReturnItem(product);
    }

    private Product productCategoryMapper(Product productToMap) {
        List<Category> firstCategories = categoriesLiveData.getValue();
        if (firstCategories != null && !firstCategories.isEmpty()) {
            Category secondCategory = Flowable.fromIterable(firstCategories)
                    .filter(category -> category.getServerId().equals(productToMap.getCatServerId1()))
                    .firstOrError()
                    .map(category -> {
                        productToMap.setCategory1(category);
                        return category.getChildren();
                    })
                    .toFlowable()
                    .flatMapIterable(categories -> categories)
                    .filter(category -> category.getServerId().equals(productToMap.getCatServerId2()))
                    .onErrorReturnItem(Category.noCategory())
                    .first(Category.noCategory())
                    .blockingGet();
            if (secondCategory != Category.noCategory())
                productToMap.setCategory2(secondCategory);
        }
        return productToMap;
    }

    public void onClearClicked() {
        disableEditMode();
        resetDefaultValues();
    }

    private void resetDefaultValues() {
        retailPriceField = getDefaultRetailPrice();
        vatField = getDefaultVat();
        takeawayVatField = getDefaultTakeawayVat();
        takeawayPriceField = getDefaultTakeawayPrice();
        category1 = Category.noCategory();
        category2 = Category.noCategory();
        productType = 0;
        isMiscellaneous = false;
        isActive = true;
    }

    private void disableEditMode() {
        product = null;
        isInEditMode = false;
        this.image = null;
    }

    public LiveData<List<Product>> getNameSearchProducts() {
        return this.productNameSearch;
    }

    public void onProductImageSelected(Bitmap bitmapImage) {
        this.image = ImageHelper.getBytesFromBitmap(bitmapImage);
    }

    public LiveData<List<Category>> getFirstLevelCategories() {
        return categoriesLiveData;
    }

    private Category getCategory1() {
        return category1;
    }

    private Category getCategory2() {
        return category2;
    }

    public Category getCategory(int level) {
        if (level == 1)
            return getCategory1();
        else
            return getCategory2();
    }

    public Product getSavedState() {
        Product savedProduct = new Product();

        savedProduct.setName(nameField);
        savedProduct.setBarcode(barcodeField);
        savedProduct.setCategory1(getCategory1());
        savedProduct.setCategory2(getCategory2());
        savedProduct.setActive(isActive);
        savedProduct.setImage(image);
        savedProduct.setDescription(description);
        savedProduct.setAbcCode(abcCode);
        savedProduct.setProcessLocation(processLocation);
        savedProduct.setType(this.productType);
        savedProduct.setMiscellaneous(isMiscellaneous);

        if (isValidTakeawayPrice())
            savedProduct.setTakeawayPrice(doubleOf(takeawayPriceField));
        else
            savedProduct.setTakeawayPrice(doubleOf(getDefaultTakeawayPrice()));

        if (isValidTakeawayVat())
            savedProduct.setTakeawayVat(doubleOf(takeawayVatField));

        if (isValidRetailPrice())
            savedProduct.setRetailPrice(doubleOf(retailPriceField));

        if (isValidVat())
            savedProduct.setVat(doubleOf(vatField));

        return savedProduct;
    }

    private String getDefaultRetailPrice() {
        return String.valueOf(0.0);
    }

    private String getDefaultTakeawayPrice() {
        return String.valueOf(0.0);
    }

    private String getDefaultVat() {
        return String.valueOf(25.0);
    }

    private String getDefaultTakeawayVat() {
        return String.valueOf(15.0);
    }

    public void getProductByBarcode(BaseFeedbackListener listener) {
        removeDisposable(productByBarcodeDisposable);
        if (isValidBarcode()) {
            productByBarcodeDisposable = getProductByBarcodeInteractor.execute(barcodeField)
                    .map(this::productCategoryMapper)
                    .flatMap(this::defaultProductImageMapper)
                    .subscribe(product1 -> {
                                this.product = product1;
                                this.isInEditMode = true;
                                listener.onSuccess("");
                            },
                            error -> {
                                logError(error, "Couldn't load by barcode");
                                listener.onFail("Product doesn't exist");
                            });
            addDisposable(productByBarcodeDisposable);
        }
    }

    private void refreshData() {
        updateData(null);
    }

    public void updateData(BaseFeedbackListener listener) {

        removeDisposable(typesAndCategoriesDisposable);

        typesAndCategoriesDisposable = Flowable.zip(pollingHelper.getProductTypes().toFlowable(),
                pollingHelper.fetchCategories(),
                (productTypes, category) -> {
                    productTypesLiveData.postValue(productTypes);
                    categoriesLiveData.postValue(category.getChildren());
                    return true;
                })
                .subscribe(b -> {
                            if (listener != null)
                                listener.onSuccess("");
                        },
                        error -> {
                            if (listener != null) {
                                if (productTypesLiveData.getValue() != null
                                        && !productTypesLiveData.getValue().isEmpty()
                                        && categoriesLiveData.getValue() != null
                                        && !categoriesLiveData.getValue().isEmpty())
                                    listener.onSuccess("");
                                else
                                    listener.onFail("Couldn't fetch data from server");
                            }
                            logError(error, "Error fetching data from server");
                        });
        addDisposable(typesAndCategoriesDisposable);
    }

    public LiveData<List<ProductType>> getProductTypes() {
        return productTypesLiveData;
    }

    public void onProductTypeSelected(int position) {
        List<ProductType> types = productTypesLiveData.getValue();
        if (types != null && types.size() > position)
            this.productType = types.get(position).getId();
    }

    public void onMiscellaneousChanged(boolean isMiscellaneous) {
        this.isMiscellaneous = isMiscellaneous;
    }

    public int getProductTypeIndex(Integer typeID) {
        List<ProductType> types = productTypesLiveData.getValue();
        if (types != null && typeID != null)
            for (int i = 0; i < types.size(); i++)
                if (types.get(i).getId() == typeID)
                    return i;
        return 0;
    }

    public boolean isRestaurant() {
        if (userAccountStorage.getShop() != null)
            return userAccountStorage.getShop().isRestaurant();
        return true;
    }

    public int getCategoryPosition(int level) {
        List<Category> categories = categoriesLiveData.getValue();
        if (categories != null) {

            for (int i = 0; i < categories.size(); i++) {

                Category category1 = getCategory1();

                if (category1 == null || category1.equals(Category.noCategory()))
                    return 0;
                if (categories.get(i).getServerId().equals(category1.getServerId())) {
                    if (level == 1)
                        return i + 1;
                    else
                        return getPositionForCategory2(category1);
                }
            }

        }
        return 0;
    }

    private int getPositionForCategory2(Category parentCategory) {
        List<Category> children = parentCategory.getChildren();
        for (int j = 0; j < children.size(); j++) {
            Category category2 = getCategory2();
            if (category2 == null || category2.equals(Category.noCategory()))
                return 0;
            if (children.get(j).getServerId().equals(category2.getServerId())) {
                return j + 1;
            }
        }
        return 0;
    }
}
