package com.susoft.productmanager;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import com.susoft.productmanager.dagger.ApplicationComponent;
import com.susoft.productmanager.dagger.ApplicationModule;
import com.susoft.productmanager.dagger.DaggerApplicationComponent;
import com.susoft.productmanager.viewmodel.factory.ViewModelFactory;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.lifecycle.ViewModelProvider;
import es.dmoral.toasty.Toasty;

/**
 * @author Muhammed Sabry
 */
@SuppressLint("StaticFieldLeak")
public class ProductApp extends Application {

    private static ViewModelFactory viewModelFactory;
    private static ApplicationComponent applicationComponent;
    private static Context context;

    public static ViewModelProvider.Factory getViewModelFactory() {
        return viewModelFactory;
    }

    public static Context getContext() {
        return context;
    }

    @Inject
    void setViewModelFactory(ViewModelFactory viewModelFactory) {
        ProductApp.viewModelFactory = viewModelFactory;
    }

    public static ApplicationComponent getDaggerComponent() {
        return applicationComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);

        context = this;

        Toasty.Config.getInstance()
                .allowQueue(false)
                .apply();

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
}
