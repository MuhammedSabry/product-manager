package com.susoft.productmanager.dagger;

import android.app.Application;
import android.content.Context;

import com.susoft.productmanager.ProductApp;
import com.susoft.productmanager.data.DatabaseRepository;
import com.susoft.productmanager.data.SusoftDatabase;
import com.susoft.productmanager.data.network.NetworkRepository;
import com.susoft.productmanager.data.network.retrofit.RetrofitNetworkRepository;
import com.susoft.productmanager.domain.AuthTokenStorage;
import com.susoft.productmanager.domain.eventbus.CellChangedEvent;
import com.susoft.productmanager.domain.eventbus.ConnectivityEvent;
import com.susoft.productmanager.domain.eventbus.DeviceOfflineEvent;
import com.susoft.productmanager.domain.eventbus.EventBus;
import com.susoft.productmanager.domain.interactor.usecase.BackgroundExecutionThread;
import com.susoft.productmanager.domain.interactor.usecase.PostExecutionThread;
import com.susoft.productmanager.domain.repository.CategoryRepository;
import com.susoft.productmanager.domain.repository.MenuCellRepository;
import com.susoft.productmanager.domain.repository.MenuRepository;
import com.susoft.productmanager.domain.repository.ProductRepository;
import com.susoft.productmanager.domain.service.AuthenticationService;
import com.susoft.productmanager.domain.service.CategoryService;
import com.susoft.productmanager.domain.service.CellService;
import com.susoft.productmanager.domain.service.GridService;
import com.susoft.productmanager.domain.service.MenuService;
import com.susoft.productmanager.domain.service.ProductService;
import com.susoft.productmanager.domain.service.UserService;
import com.susoft.productmanager.executor.BackgroundThreadExecutor;
import com.susoft.productmanager.executor.MainThreadExecutor;
import com.susoft.productmanager.preferences.UserAccountStorage;
import com.susoft.productmanager.util.ActivityStateTracker;
import com.susoft.productmanager.util.ConnectivityTracker;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Muhammed Sabry
 */
@Module
public class ApplicationModule {

    private final ProductApp application;

    public ApplicationModule(ProductApp application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return this.application;
    }

    @Provides
    @Singleton
    Application provideApplicationContext() {
        return this.application;
    }

    @Provides
    @Singleton
    SusoftDatabase getDatabase(Context context) {
        return SusoftDatabase.getInstance(context);
    }

    @Provides
    @Singleton
    EventBus<ConnectivityEvent> provideConnectivityBus() {
        return new EventBus<>();
    }

    @Provides
    @Singleton
    EventBus<CellChangedEvent> provideCellBus() {
        return new EventBus<>();
    }

    @Provides
    @Singleton
    EventBus<DeviceOfflineEvent> provideOfflineEvent() {
        return new EventBus<>();
    }

    @Provides
    @Singleton
    ConnectivityTracker provideConnectivityTracker(Context context, EventBus<ConnectivityEvent> eventBus) {
        return new ConnectivityTracker(context, eventBus);
    }

    @Provides
    @Singleton
    ActivityStateTracker provideActivityStateTracker(ConnectivityTracker connectivityTracker) {
        return new ActivityStateTracker(connectivityTracker);
    }

    @Provides
    @Singleton
    AuthTokenStorage provideAuthenticationTokenStorage(Context context) {
        return new UserAccountStorage(context);
    }

    @Provides
    @Singleton
    DatabaseRepository getDatabaseRepository(SusoftDatabase susoftDatabase) {
        return new DatabaseRepository(susoftDatabase);
    }

    @Provides
    @Singleton
    BackgroundExecutionThread getBackgroundExecutionThread() {
        return new BackgroundThreadExecutor();
    }

    @Provides
    @Singleton
    PostExecutionThread getPostExecutionThread() {
        return new MainThreadExecutor();
    }

    @Provides
    @Singleton
    ProductRepository provideProductRepository(DatabaseRepository databaseRepository) {
        return databaseRepository;
    }

    @Provides
    @Singleton
    CategoryRepository provideCategoryRepository(DatabaseRepository databaseRepository) {
        return databaseRepository;
    }

    @Provides
    @Singleton
    MenuRepository provideMenuRepository(DatabaseRepository databaseRepository) {
        return databaseRepository;
    }

    @Provides
    @Singleton
    MenuCellRepository provideMenuCellRepository(DatabaseRepository databaseRepository) {
        return databaseRepository;
    }

    @Provides
    @Singleton
    NetworkRepository provideNetworkRepository(RetrofitNetworkRepository retrofitNetworkRepository) {
        return retrofitNetworkRepository;
    }

    @Provides
    @Singleton
    AuthenticationService provideAuthenticationService(NetworkRepository networkRepository) {
        return networkRepository;
    }

    @Provides
    @Singleton
    ProductService provideProductService(NetworkRepository networkRepository) {
        return networkRepository;
    }

    @Provides
    @Singleton
    CategoryService provideCategoryService(NetworkRepository networkRepository) {
        return networkRepository;
    }

    @Provides
    @Singleton
    CellService provideCellService(NetworkRepository networkRepository) {
        return networkRepository;
    }

    @Provides
    @Singleton
    GridService provideGridService(NetworkRepository networkRepository) {
        return networkRepository;
    }

    @Provides
    @Singleton
    MenuService provideMenuService(NetworkRepository networkRepository) {
        return networkRepository;
    }

    @Provides
    @Singleton
    UserService provideUserService(NetworkRepository networkRepository) {
        return networkRepository;
    }
}
