package com.susoft.productmanager.dagger;

import com.susoft.productmanager.viewmodel.CreateProductViewModel;
import com.susoft.productmanager.viewmodel.LoginViewModel;
import com.susoft.productmanager.viewmodel.MainViewModel;
import com.susoft.productmanager.viewmodel.QuickMenuViewModel;
import com.susoft.productmanager.viewmodel.factory.ViewModelKey;

import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * @author Muhammed Sabry
 */
@Module
interface ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CreateProductViewModel.class)
    ViewModel provideCreateCategoryViewModel(CreateProductViewModel createProductViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(QuickMenuViewModel.class)
    ViewModel provideQuickMenuViewModel(QuickMenuViewModel quickMenuViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    ViewModel provideLoginViewModel(LoginViewModel loginViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    ViewModel provideMainViewModel(MainViewModel mainViewModel);
}
