package com.susoft.productmanager.dagger;

import com.susoft.productmanager.ProductApp;
import com.susoft.productmanager.ui.activity.BaseTrackedActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Muhammed Sabry
 */
@Singleton
@Component(modules = {ApplicationModule.class, ViewModelModule.class})
public interface ApplicationComponent {

    void inject(ProductApp productApp);

    void inject(BaseTrackedActivity baseTrackedActivity);
}
