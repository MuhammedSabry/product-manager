package com.susoft.productmanager.ui.custom;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.os.Build;
import android.view.View;

import com.google.android.material.shape.MaterialShapeDrawable;
import com.google.android.material.shape.ShapeAppearanceModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @author Muhammed Sabry
 */
class CutoutDrawable extends MaterialShapeDrawable {
    private final Paint cutoutPaint;
    private final RectF cutoutBounds;
    private int savedLayer;

    CutoutDrawable(@Nullable ShapeAppearanceModel shapeAppearanceModel) {
        super(shapeAppearanceModel != null ? shapeAppearanceModel : new ShapeAppearanceModel());
        cutoutPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        setPaintStyles();
        cutoutBounds = new RectF();
    }

    private void setPaintStyles() {
        cutoutPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        cutoutPaint.setColor(Color.WHITE);
        cutoutPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
    }

    private void setCutout(float left, float top, float right, float bottom) {
        // Avoid expensive redraws by only calling invalidateSelf if one of the cutout's dimensions has
        // changed.
        if (left != cutoutBounds.left
                || top != cutoutBounds.top
                || right != cutoutBounds.right
                || bottom != cutoutBounds.bottom) {
            cutoutBounds.set(left, top, right, bottom);
            invalidateSelf();
        }
    }

    void setCutout(RectF bounds) {
        setCutout(bounds.left, bounds.top, bounds.right, bounds.bottom);
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        preDraw(canvas);
        super.draw(canvas);

        // Draw mask for the cutout.
        canvas.drawRect(cutoutBounds, cutoutPaint);

        postDraw(canvas);
    }

    private void preDraw(@NonNull Canvas canvas) {
        Callback callback = getCallback();

        if (useHardwareLayer(callback)) {
            View viewCallback = (View) callback;
            viewCallback.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // If we're not using a hardware layer, save the canvas layer.
            saveCanvasLayer(canvas);
        }
    }

    private void saveCanvasLayer(@NonNull Canvas canvas) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            savedLayer = canvas.saveLayer(0, 0, canvas.getWidth(), canvas.getHeight(), null);
        } else {
            savedLayer =
                    canvas.saveLayer(0, 0, canvas.getWidth(), canvas.getHeight(), null, Canvas.ALL_SAVE_FLAG);
        }
    }

    private void postDraw(@NonNull Canvas canvas) {
        if (!useHardwareLayer(getCallback())) {
            canvas.restoreToCount(savedLayer);
        }
    }

    private boolean useHardwareLayer(Callback callback) {
        return callback instanceof View;
    }
}
