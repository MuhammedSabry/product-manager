package com.susoft.productmanager.ui.adapter.dropdown;

import android.content.Context;
import android.database.AbstractCursor;
import android.database.Cursor;

import com.susoft.productmanager.R;
import com.susoft.productmanager.domain.model.Product;

import java.util.List;

import androidx.cursoradapter.widget.SimpleCursorAdapter;

/**
 * @author Muhammed Sabry
 */
public class ProductCursorAdapter extends SimpleCursorAdapter {

    private static final String[] mFields = {"_id", "name", "barcode"};
    private static final int[] mViewIds = {R.id.product_id, R.id.product_name, R.id.product_barcode};
    private List<Product> products;

    public ProductCursorAdapter(Context context, List<Product> products) {
        super(context, R.layout.itemview_search_dropdown, null, mFields, mViewIds, 0);
        this.products = products;
    }

    @Override
    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        return new SuggestionsCursor();
    }

    public void setProducts(List<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    public Product getProduct(int position) {
        return products.get(position);
    }

    private class SuggestionsCursor extends AbstractCursor {

        @Override
        public int getCount() {
            return products.size();
        }

        @Override
        public int getType(int column) {
            return FIELD_TYPE_STRING;
        }

        @Override
        public String[] getColumnNames() {
            return mFields;
        }

        @Override
        public long getLong(int column) {
            if (column == 0)
                return getPosition();
            throw new UnsupportedOperationException("unimplemented");
        }

        @Override
        public String getString(int column) {
            if (column == 0)
                return "ID: " + String.valueOf(products.get(getPosition()).getId());
            else if (column == 1)
                return products.get(getPosition()).getName();
            else if (column == 2)
                return "Barcode: " + products.get(getPosition()).getBarcode();
            throw new UnsupportedOperationException("unimplemented");
        }

        @Override
        public short getShort(int column) {
            throw new UnsupportedOperationException("unimplemented");
        }

        @Override
        public int getInt(int column) {
            throw new UnsupportedOperationException("unimplemented");
        }

        @Override
        public float getFloat(int column) {
            throw new UnsupportedOperationException("unimplemented");
        }

        @Override
        public double getDouble(int column) {
            throw new UnsupportedOperationException("unimplemented");
        }

        @Override
        public boolean isNull(int column) {
            return false;
        }
    }
}