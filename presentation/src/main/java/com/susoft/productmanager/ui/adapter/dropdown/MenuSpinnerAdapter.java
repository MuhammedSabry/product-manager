package com.susoft.productmanager.ui.adapter.dropdown;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.susoft.productmanager.R;
import com.susoft.productmanager.domain.model.Menu;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @author Muhammed Sabry
 */
public class MenuSpinnerAdapter extends ArrayAdapter<Menu> {

    private List<Menu> menus;

    public MenuSpinnerAdapter(@NonNull Context context, @NonNull List<Menu> menus) {
        super(context, R.layout.itemview_spinner_normal, menus);
        this.menus = menus;
        setDropDownViewResource(R.layout.itemview_spinner_dropdown);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        if (position >= 0 && position < menus.size())
            view.setText(menus.get(position).getName());
        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        if (position >= 0 && position < menus.size())
            view.setText(menus.get(position).getName());
        return view;
    }

    public void setMenus(List<Menu> menus) {
        this.menus.clear();
        this.menus.addAll(menus);
        notifyDataSetChanged();
    }
}
