package com.susoft.productmanager.ui.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;

import com.susoft.productmanager.ProductApp;
import com.susoft.productmanager.util.ActivityStateTracker;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * @author Muhammed Sabry
 */
@SuppressLint("Registered")
public class BaseTrackedActivity extends AppCompatActivity {

    @Inject
    ActivityStateTracker activityStateTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ProductApp.getDaggerComponent().inject(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityStateTracker.onActivityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        activityStateTracker.onActivityResumed();
    }
}
