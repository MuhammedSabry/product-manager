package com.susoft.productmanager.ui.custom;

import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.susoft.productmanager.R;

/**
 * @author Muhammed Sbary
 */
public class ProductDragShadow extends View.DragShadowBuilder {

    // The drag shadow image, defined as a drawable thing
    private Drawable shadow;

    public ProductDragShadow(View view) {
        super(view);
        shadow = new ColorDrawable(view.getResources().getColor(R.color.primary_50));
    }

    // Defines a callback that sends the drag shadow dimensions and touch point back to the
    // system.
    @Override
    public void onProvideShadowMetrics(Point size, Point touch) {
        // Defines local variables
        int width, height;

        //Sets the width of the shadow to half the width of the original View
        width = getView().getWidth() / 2;

        // Sets the height of the shadow to half the height of the original View
        height = getView().getHeight();

        // The drag shadow is a ColorDrawable. This sets its dimensions to be the same as the
        // Canvas that the system will provide. As a result, the drag shadow will fill the
        // Canvas.
        shadow.setBounds(0, 0, width, height);

        // Sets the size parameter's width and height values. These get back to the system
        // through the size parameter.
        size.set(width, height);

        // Sets the touch point's position to be in the middle of the drag shadow
        touch.set(width / 2, height / 2);
    }
}
