package com.susoft.productmanager.ui.custom;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatSpinner;

/**
 * @author Muhammed Sabry
 */
public class CustomSpinner extends AppCompatSpinner {

    private boolean mOpenInitiated = false;
    private OnSelectedListener onSelectedListener;

    public CustomSpinner(Context context) {
        super(context, null);
    }

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setSelection(int position) {
        setSelection(position, false);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public boolean performClick() {
        // register that the Spinner was opened so we have a status
        // indicator for when the container holding this Spinner may lose focus
        mOpenInitiated = true;
        setActivated(true);
        requestFocus();
        return super.performClick();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasBeenOpened() && hasFocus) {
            performClosedEvent();
        }
    }

    @Override
    public void setSelection(int position, boolean animate) {
        super.setSelection(position, animate);
        if (onSelectedListener != null)
            onSelectedListener.onSelected(position);
    }

    public void setOnSelectedListener(OnSelectedListener listener) {
        this.onSelectedListener = listener;
    }

    /**
     * Propagate the closed Spinner event to the onCategorySelectedListener from outside if needed.
     */
    public void performClosedEvent() {
        mOpenInitiated = false;
        setActivated(false);
        clearFocus();
    }

    public interface OnSelectedListener {
        void onSelected(int position);
    }

    /**
     * A boolean flag indicating that the Spinner triggered an open event.
     *
     * @return true for opened Spinner
     */
    public boolean hasBeenOpened() {
        return mOpenInitiated;
    }

}
