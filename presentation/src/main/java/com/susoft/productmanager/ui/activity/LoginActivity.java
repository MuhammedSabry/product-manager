package com.susoft.productmanager.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;

import com.susoft.productmanager.ProductApp;
import com.susoft.productmanager.R;
import com.susoft.productmanager.databinding.ActivityLoginBinding;
import com.susoft.productmanager.model.LoginForm;
import com.susoft.productmanager.util.InputUtils;
import com.susoft.productmanager.util.TextUtils;
import com.susoft.productmanager.viewmodel.LoginViewModel;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import static com.susoft.productmanager.util.InputUtils.setErrorCanceller;
import static com.susoft.productmanager.util.ToastUtil.shortError;
import static com.susoft.productmanager.util.ToastUtil.shortSuccess;

/**
 * @author Muhammed Sabry
 */
public class LoginActivity extends BaseTrackedActivity implements LoginForm.Listener {

    private ActivityLoginBinding binding;
    private LoginViewModel loginViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActivity();
    }

    private void initActivity() {
        initDataBinding();
        initViewModel();
        initViews();
    }

    private void initViews() {
        setErrorCanceller(binding.loginData);
        setErrorCanceller(binding.chain);
        setErrorCanceller(binding.password);

        LoginForm loginForm = loginViewModel.getLoginForm();

        InputUtils.setText(binding.loginData, loginForm.getLoginData());
        InputUtils.setText(binding.chain, loginForm.getChain());
        InputUtils.setText(binding.password, loginForm.getPassword());
        binding.saveLoginInfo.setChecked(loginForm.shouldSaveInfo());
    }

    private void initViewModel() {
        loginViewModel = ViewModelProviders.of(this, ProductApp.getViewModelFactory()).get(LoginViewModel.class);
    }

    private void initDataBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setHandler(this);
    }

    public void onLoginDataChanged(Editable loginData) {
        loginViewModel.onLoginDataChanged(TextUtils.getText(loginData));
    }

    public void onChainChanged(Editable chain) {
        loginViewModel.onChainChanged(TextUtils.getText(chain));
    }

    public void onPasswordChanged(Editable password) {
        loginViewModel.onPasswordChanged(TextUtils.getText(password));
    }

    public void onSaveInfoCheckChanged(boolean isChecked) {
        loginViewModel.onSaveInfoCheckChanged(isChecked);
    }

    public void onLoginClicked() {
        disableLogin();

        loginViewModel.onLoginClicked(this);
    }

    private void disableLogin() {
        binding.loadingScreen.setVisibility(View.VISIBLE);
        binding.loginButton.setEnabled(false);
        binding.loginButton.setAlpha(0.75f);
        InputUtils.hideSoftKeyboard(this);
    }

    private void enableLogin() {
        binding.loadingScreen.setVisibility(View.GONE);
        binding.loginButton.setEnabled(true);
        binding.loginButton.setAlpha(1f);
    }

    @Override
    public void onLoginDataError(String message) {
        binding.loginData.setError(message);
    }

    @Override
    public void onChainError(String message) {
        binding.chain.setError(message);
    }

    @Override
    public void onPasswordError(String message) {
        binding.password.setError(message);
    }

    @Override
    public void onSuccess(String message) {
        startActivity(new Intent(this, MainActivity.class));
        finish();
        shortSuccess(message);
    }

    @Override
    public void onFallBack() {
        enableLogin();
    }

    @Override
    public void onFail(String message) {
        shortError(message);
        enableLogin();
    }
}

