package com.susoft.productmanager.ui.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;

import com.susoft.productmanager.R;
import com.susoft.productmanager.databinding.DialogEditCellBinding;
import com.susoft.productmanager.domain.model.Cell;
import com.susoft.productmanager.model.EditCellForm;
import com.susoft.productmanager.util.ImageHelper;
import com.susoft.productmanager.util.InputUtils;
import com.susoft.productmanager.viewmodel.QuickMenuViewModel;

import androidx.databinding.DataBindingUtil;

import static com.susoft.productmanager.util.ToastUtil.shortError;
import static com.susoft.productmanager.util.ToastUtil.shortSuccess;

/**
 * @author Muhammed Sbary
 */
public class EditCellDialog extends AlertDialog implements EditCellForm.Listener {

    private DialogEditCellBinding binding;
    private QuickMenuViewModel viewModel;
    private OnSelectImageRequestedListener onSelectImageRequestedListener;

    public EditCellDialog(Context context, QuickMenuViewModel viewModel, OnSelectImageRequestedListener onSelectImageRequestedListener) {
        super(context);
        this.viewModel = viewModel;
        this.onSelectImageRequestedListener = onSelectImageRequestedListener;

        initDataBinding(context);
        initDialog();
        initViews();
    }

    private void initDataBinding(Context context) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_edit_cell,
                null,
                false);
        binding.setHandler(this);
    }

    private void initDialog() {
        setView(binding.getRoot());

        setOnDismissListener(dialog1 -> viewModel.setCellToEdit(null));

        InputUtils.setErrorCanceller(binding.menuTitle);
    }

    private void initViews() {
        InputUtils.clearError(binding.menuTitle);
        Cell cell = viewModel.getCellToEdit();

        InputUtils.setText(binding.menuTitle, cell.getText());

        byte[] image = cell.getImage();

        if (image != null)
            binding.image.setImageBitmap(ImageHelper.getImageFromBytes(image));
        else
            binding.image.setImageDrawable(null);
    }

    private void startLoading() {
        setCancelable(false);
        binding.loadingScreen.setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        setCancelable(true);
        binding.loadingScreen.setVisibility(View.GONE);
    }

    public void onSelectImageClicked() {
        onSelectImageRequestedListener.requestImage();
    }

    public void onImageSelected(Uri uri) {
        binding.image.setImageURI(uri);
    }

    public void onSaveClicked() {

        startLoading();

        BitmapDrawable drawable = (BitmapDrawable) binding.image.getDrawable();

        Bitmap bitmap = null;
        if (drawable != null)
            bitmap = drawable.getBitmap();

        EditCellForm editCellForm = new EditCellForm(bitmap, InputUtils.getTextEditable(binding.menuTitle));

        viewModel.onEditDialogCreateClicked(editCellForm, this);
    }

    @Override
    public void onTitleError(String message) {
        binding.menuTitle.setError(message);
    }

    @Override
    public void onSuccess(String message) {
        stopLoading();
        shortSuccess(message);
        dismiss();
    }

    @Override
    public void onFallBack() {
        stopLoading();
    }

    @Override
    public void onFail(String message) {
        stopLoading();
        shortError(message);
    }

    @Override
    public void show() {
        super.show();
        initViews();
    }

    public interface OnSelectImageRequestedListener {
        void requestImage();
    }
}
