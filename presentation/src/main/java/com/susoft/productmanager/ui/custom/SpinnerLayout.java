package com.susoft.productmanager.ui.custom;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.AbsSavedState;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStructure;
import android.view.accessibility.AccessibilityEvent;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.R;
import com.google.android.material.animation.AnimationUtils;
import com.google.android.material.internal.CollapsingTextHelper;
import com.google.android.material.internal.DescendantOffsetUtils;
import com.google.android.material.internal.ThemeEnforcement;
import com.google.android.material.resources.MaterialResources;
import com.google.android.material.shape.MaterialShapeDrawable;
import com.google.android.material.shape.ShapeAppearanceModel;
import com.google.android.material.textfield.TextInputEditText;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.ColorInt;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.annotation.VisibleForTesting;
import androidx.appcompat.widget.AppCompatDrawableManager;
import androidx.appcompat.widget.DrawableUtils;
import androidx.appcompat.widget.TintTypedArray;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.GravityCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import androidx.core.widget.TextViewCompat;

import static com.google.android.material.internal.ThemeEnforcement.createThemedContext;

/**
 * @author Muhammed Sbary
 */
@SuppressLint({"RestrictedApi", "PrivateResource"})
public class SpinnerLayout extends LinearLayout {

    public static final int BOX_BACKGROUND_OUTLINE = 2;
    private static final int DEF_STYLE_RES = R.style.Widget_Design_TextInputLayout;
    /**
     * Duration for the label's scale up and down animations.
     */
    private static final int LABEL_SCALE_ANIMATION_DURATION = 167;
    final CollapsingTextHelper collapsingTextHelper = new CollapsingTextHelper(this);
    private final FrameLayout inputFrame;
    private final IndicatorViewController indicatorViewController = new IndicatorViewController(this);
    private final ShapeAppearanceModel shapeAppearanceModel;
    private final ShapeAppearanceModel cornerAdjustedShapeAppearanceModel;
    private final int boxLabelCutoutPaddingPx;
    private final int boxStrokeWidthDefaultPx;
    private final int boxStrokeWidthFocusedPx;
    private final Rect tmpRect = new Rect();
    private final Rect tmpBoundsRect = new Rect();
    private final RectF tmpRectF = new RectF();
    @ColorInt
    private final int defaultStrokeColor;
    @ColorInt
    private final int hoveredStrokeColor;
    @ColorInt
    private final int disabledColor;
    Spinner spinner;
    private boolean hintEnabled;
    private CharSequence hint;
    /**
     * instance of {@link TextInputEditText}, this value defines the behavior of its {@link
     * TextInputEditText#getHint()} method.
     */
    private boolean isProvidingHint;
    private MaterialShapeDrawable boxBackground;
    @BoxBackgroundMode
    private int boxBackgroundMode;
    private int boxStrokeWidthPx;
    @ColorInt
    private int boxStrokeColor;
    @ColorInt
    private int boxBackgroundColor;
    private float textSize;
    private ColorStateList defaultHintTextColor;
    private ColorStateList focusedTextColor;
    @ColorInt
    private int focusedStrokeColor;
    // Only used for testing
    private boolean hintExpanded;
    private boolean hintAnimationEnabled;
    private ValueAnimator animator;
    private boolean inDrawableStateChanged;

    public SpinnerLayout(Context context) {
        this(context, null);
    }

    public SpinnerLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, R.attr.textInputStyle);
    }


    @SuppressLint("RestrictedApi")
    public SpinnerLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(createThemedContext(context, attrs, defStyleAttr, DEF_STYLE_RES), attrs, defStyleAttr);
        // Ensure we are using the correctly themed context rather than the context that was passed in.
        context = getContext();

        setOrientation(VERTICAL);
        setWillNotDraw(false);
        setAddStatesFromChildren(true);

        inputFrame = new FrameLayout(context);
        inputFrame.setAddStatesFromChildren(true);
        addView(inputFrame);

        collapsingTextHelper.setTextSizeInterpolator(AnimationUtils.LINEAR_INTERPOLATOR);
        collapsingTextHelper.setPositionInterpolator(AnimationUtils.LINEAR_INTERPOLATOR);
        collapsingTextHelper.setCollapsedTextGravity(Gravity.TOP | GravityCompat.START);

        final TintTypedArray a =
                ThemeEnforcement.obtainTintedStyledAttributes(
                        context,
                        attrs,
                        R.styleable.TextInputLayout,
                        defStyleAttr,
                        DEF_STYLE_RES,
                        R.styleable.TextInputLayout_counterTextAppearance,
                        R.styleable.TextInputLayout_counterOverflowTextAppearance,
                        R.styleable.TextInputLayout_errorTextAppearance,
                        R.styleable.TextInputLayout_helperTextTextAppearance,
                        R.styleable.TextInputLayout_hintTextAppearance);

        hintEnabled = a.getBoolean(R.styleable.TextInputLayout_hintEnabled, true);
        setHint(a.getText(R.styleable.TextInputLayout_android_hint));
        hintAnimationEnabled = a.getBoolean(R.styleable.TextInputLayout_hintAnimationEnabled, true);

        shapeAppearanceModel = new ShapeAppearanceModel(context, attrs, defStyleAttr, DEF_STYLE_RES);
        cornerAdjustedShapeAppearanceModel = new ShapeAppearanceModel(shapeAppearanceModel);
        setBoxBackgroundMode(
                a.getInt(R.styleable.TextInputLayout_boxBackgroundMode, BOX_BACKGROUND_OUTLINE));

        boxLabelCutoutPaddingPx =
                context
                        .getResources()
                        .getDimensionPixelOffset(R.dimen.mtrl_textinput_box_label_cutout_padding);
        boxStrokeWidthDefaultPx =
                context
                        .getResources()
                        .getDimensionPixelSize(R.dimen.mtrl_textinput_box_stroke_width_default);
        boxStrokeWidthFocusedPx =
                context
                        .getResources()
                        .getDimensionPixelSize(R.dimen.mtrl_textinput_box_stroke_width_focused);
        boxStrokeWidthPx = boxStrokeWidthDefaultPx;

        float boxCornerRadiusTopStart =
                a.getDimension(R.styleable.TextInputLayout_boxCornerRadiusTopStart, -1f);
        float boxCornerRadiusTopEnd =
                a.getDimension(R.styleable.TextInputLayout_boxCornerRadiusTopEnd, -1f);
        float boxCornerRadiusBottomEnd =
                a.getDimension(R.styleable.TextInputLayout_boxCornerRadiusBottomEnd, -1f);
        float boxCornerRadiusBottomStart =
                a.getDimension(R.styleable.TextInputLayout_boxCornerRadiusBottomStart, -1f);
        if (boxCornerRadiusTopStart >= 0) {
            shapeAppearanceModel.getTopLeftCorner().setCornerSize(boxCornerRadiusTopStart);
        }
        if (boxCornerRadiusTopEnd >= 0) {
            shapeAppearanceModel.getTopRightCorner().setCornerSize(boxCornerRadiusTopEnd);
        }
        if (boxCornerRadiusBottomEnd >= 0) {
            shapeAppearanceModel.getBottomRightCorner().setCornerSize(boxCornerRadiusBottomEnd);
        }
        if (boxCornerRadiusBottomStart >= 0) {
            shapeAppearanceModel.getBottomLeftCorner().setCornerSize(boxCornerRadiusBottomStart);
        }
        adjustCornerSizeForStrokeWidth();

        boxBackgroundColor = Color.TRANSPARENT;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, com.susoft.productmanager.R.styleable.SpinnerLayout, 0, 0);
        try {
            textSize = typedArray.getDimension(com.susoft.productmanager.R.styleable.SpinnerLayout_labelSize, 16);
        } finally {
            typedArray.recycle();
        }
        collapsingTextHelper.setCollapsedTextSize(textSize);
        collapsingTextHelper.setExpandedTextSize(textSize);

        if (a.hasValue(R.styleable.TextInputLayout_android_textColorHint)) {
            defaultHintTextColor =
                    focusedTextColor = a.getColorStateList(R.styleable.TextInputLayout_android_textColorHint);
        }

        ColorStateList boxStrokeColorStateList =
                MaterialResources.getColorStateList(context, a, R.styleable.TextInputLayout_boxStrokeColor);
        if (boxStrokeColorStateList != null && boxStrokeColorStateList.isStateful()) {
            defaultStrokeColor = boxStrokeColorStateList.getDefaultColor();
            disabledColor =
                    boxStrokeColorStateList.getColorForState(new int[]{-android.R.attr.state_enabled}, -1);
            hoveredStrokeColor =
                    boxStrokeColorStateList.getColorForState(new int[]{android.R.attr.state_hovered}, -1);
            focusedStrokeColor =
                    boxStrokeColorStateList.getColorForState(
                            new int[]{android.R.attr.state_focused}, Color.TRANSPARENT);
        } else {
            // If attribute boxStrokeColor is not a color state list but only a single value, its value
            // will be applied to the outlined color in the focused state
            focusedStrokeColor =
                    a.getColor(R.styleable.TextInputLayout_boxStrokeColor, Color.TRANSPARENT);
            defaultStrokeColor =
                    ContextCompat.getColor(context, R.color.mtrl_textinput_default_box_stroke_color);
            disabledColor = ContextCompat.getColor(context, R.color.mtrl_textinput_disabled_color);
            hoveredStrokeColor =
                    ContextCompat.getColor(context, R.color.mtrl_textinput_hovered_box_stroke_color);
        }

        final int hintAppearance = a.getResourceId(R.styleable.TextInputLayout_hintTextAppearance, -1);
        if (hintAppearance != -1) {
            setHintTextAppearance(a.getResourceId(R.styleable.TextInputLayout_hintTextAppearance, 0));
        }

        final int errorTextAppearance =
                a.getResourceId(R.styleable.TextInputLayout_errorTextAppearance, 0);
        final boolean errorEnabled = a.getBoolean(R.styleable.TextInputLayout_errorEnabled, false);

        setErrorEnabled(errorEnabled);
        setErrorTextAppearance(errorTextAppearance);

        if (a.hasValue(R.styleable.TextInputLayout_errorTextColor)) {
            setErrorTextColor(a.getColorStateList(R.styleable.TextInputLayout_errorTextColor));
        }
        if (a.hasValue(R.styleable.TextInputLayout_hintTextColor)) {
            setHintTextColor(a.getColorStateList(R.styleable.TextInputLayout_hintTextColor));
        }
        a.recycle();

        // For accessibility, consider TextInputLayout itself to be a simple container for an EditText,
        // and do not expose it to accessibility services.
        ViewCompat.setImportantForAccessibility(this, ViewCompat.IMPORTANT_FOR_ACCESSIBILITY_NO);
    }

    private static void recursiveSetEnabled(final ViewGroup vg, final boolean enabled) {
        for (int i = 0, count = vg.getChildCount(); i < count; i++) {
            final View child = vg.getChildAt(i);
            child.setEnabled(enabled);
            if (child instanceof ViewGroup) {
                recursiveSetEnabled((ViewGroup) child, enabled);
            }
        }
    }

    @Override
    public void addView(View child, int index, final ViewGroup.LayoutParams params) {
        if (child instanceof Spinner) {
            // Make sure that the EditText is vertically at the bottom, so that it sits on the
            // EditText's underline
            FrameLayout.LayoutParams flp = new FrameLayout.LayoutParams(params);
            flp.gravity = Gravity.CENTER_VERTICAL | (flp.gravity & ~Gravity.VERTICAL_GRAVITY_MASK);
            inputFrame.addView(child, flp);

            // Now use the EditText's LayoutParams as our own and updateCell them to make enough space
            // for the label
            inputFrame.setLayoutParams(params);
            updateInputLayoutMargins();

            setSpinner((Spinner) child);
        } else {
            // Carry on adding the View...
            super.addView(child, index, params);
        }
    }

    @NonNull
    private Drawable getBoxBackground() {
        if (boxBackgroundMode == BOX_BACKGROUND_OUTLINE) {
            return boxBackground;
        }
        throw new IllegalStateException();
    }

    /**
     * Set the mode for the box's background (filled, outline, or none).
     *
     * @param boxBackgroundMode the box's background mode.
     */
    public void setBoxBackgroundMode(@BoxBackgroundMode int boxBackgroundMode) {
        if (boxBackgroundMode == this.boxBackgroundMode) {
            return;
        }
        this.boxBackgroundMode = boxBackgroundMode;
        onApplyBoxBackgroundMode();
    }

    private void onApplyBoxBackgroundMode() {
        assignBoxBackgroundByMode();
        updateInputLayoutMargins();
        updateTextInputBoxBounds();
    }

    private void assignBoxBackgroundByMode() {
        // Make boxBackground a CutoutDrawable if in outline mode, there is a hint, and
        // boxBackground isn't already a CutoutDrawable.
        boxBackground = new CutoutDrawable(shapeAppearanceModel);
    }

    /**
     * Adjust the corner size based on the stroke width to maintain GradientDrawable's behavior.
     * MaterialShapeDrawable internally adjusts the corner size so that the corner size does not
     * depend on the stroke width. GradientDrawable does not account for stroke width, so this causes
     * a visual diff when migrating from GradientDrawable to MaterialShapeDrawable. This method
     * reverts the corner size adjustment in MaterialShapeDrawable to maintain the visual behavior
     * from GradientDrawable for now.
     */
    private void adjustCornerSizeForStrokeWidth() {
        float strokeInset = boxBackgroundMode == BOX_BACKGROUND_OUTLINE ? boxStrokeWidthPx / 2f : 0;
        if (strokeInset <= 0f) {
            return; // Only adjust the corner size if there's a stroke inset.
        }

        float cornerRadiusTopLeft = shapeAppearanceModel.getTopLeftCorner().getCornerSize();
        cornerAdjustedShapeAppearanceModel
                .getTopLeftCorner()
                .setCornerSize(cornerRadiusTopLeft + strokeInset);

        float cornerRadiusTopRight = shapeAppearanceModel.getTopRightCorner().getCornerSize();
        cornerAdjustedShapeAppearanceModel
                .getTopRightCorner()
                .setCornerSize(cornerRadiusTopRight + strokeInset);

        float cornerRadiusBottomRight = shapeAppearanceModel.getBottomRightCorner().getCornerSize();
        cornerAdjustedShapeAppearanceModel
                .getBottomRightCorner()
                .setCornerSize(cornerRadiusBottomRight + strokeInset);

        float cornerRadiusBottomLeft = shapeAppearanceModel.getBottomLeftCorner().getCornerSize();
        cornerAdjustedShapeAppearanceModel
                .getBottomLeftCorner()
                .setCornerSize(cornerRadiusBottomLeft + strokeInset);

        ensureCornerAdjustedShapeAppearanceModel();
    }

    private void ensureCornerAdjustedShapeAppearanceModel() {
        if (getBoxBackground() instanceof MaterialShapeDrawable) {
            ((MaterialShapeDrawable) getBoxBackground())
                    .setShapeAppearanceModel(cornerAdjustedShapeAppearanceModel);
        }
    }

    @Override
    public void dispatchProvideAutofillStructure(ViewStructure structure, int flags) {
        if (spinner == null) {
            super.dispatchProvideAutofillStructure(structure, flags);
            return;
        }

        // Temporarily sets child's hint to its original value so it is properly set in the
        // child's ViewStructure.
        boolean wasProvidingHint = isProvidingHint;
        // Ensures a child TextInputEditText does not retrieve its hint from this TextInputLayout.
        isProvidingHint = false;
        try {
            super.dispatchProvideAutofillStructure(structure, flags);
        } finally {
            isProvidingHint = wasProvidingHint;
        }
    }

    private void updateInputLayoutMargins() {
        // Create/updateCell the LayoutParams so that we can add enough top margin
        // to the EditText to make room for the label.
        final LayoutParams lp = (LayoutParams) inputFrame.getLayoutParams();
        final int newTopMargin = calculateLabelMarginTop();

        if (newTopMargin != lp.topMargin) {
            lp.topMargin = newTopMargin;
            inputFrame.requestLayout();
        }
    }

    @Override
    public int getBaseline() {
        if (spinner != null) {
            return spinner.getBaseline() + getPaddingTop() + calculateLabelMarginTop();
        } else {
            return super.getBaseline();
        }
    }

    void updateLabelState(boolean animate) {
        final boolean isEnabled = isEnabled();
        final boolean hasFocus = spinner != null && spinner.hasFocus();
        final boolean errorShouldBeShown = indicatorViewController.errorShouldBeShown();

        // Set the expanded and collapsed labels to the default text color.
        if (defaultHintTextColor != null) {
            collapsingTextHelper.setCollapsedTextColor(defaultHintTextColor);
            collapsingTextHelper.setExpandedTextColor(defaultHintTextColor);
        }

        // Set the collapsed and expanded label text colors based on the current state.
        if (!isEnabled) {
            collapsingTextHelper.setCollapsedTextColor(ColorStateList.valueOf(disabledColor));
            collapsingTextHelper.setExpandedTextColor(ColorStateList.valueOf(disabledColor));
        } else if (errorShouldBeShown) {
            collapsingTextHelper.setCollapsedTextColor(indicatorViewController.getErrorViewTextColors());
        } else if ((hasFocus) && focusedTextColor != null) {
            collapsingTextHelper.setCollapsedTextColor(focusedTextColor);
        } // If none of these states apply, leave the expanded and collapsed colors as they are.

        collapseHint(animate);

    }

    public Spinner getSpinner() {
        return spinner;
    }

    private void setSpinner(Spinner spinner) {
        // If we already have an EditText, throw an exception
        if (this.spinner != null) {
            throw new IllegalArgumentException("We already have an EditText, can only have one");
        }
        this.spinner = spinner;
        onApplyBoxBackgroundMode();
        setTextInputAccessibilityDelegate(new AccessibilityDelegate(this));

        collapsingTextHelper.setExpandedTextSize(textSize);

        final int editTextGravity = this.spinner.getGravity();
        collapsingTextHelper.setCollapsedTextGravity(
                Gravity.TOP | (editTextGravity & ~Gravity.VERTICAL_GRAVITY_MASK));
        collapsingTextHelper.setExpandedTextGravity(editTextGravity);


        // If we do not have a valid hint, try and retrieve it from the EditText, if enabled
        if (hintEnabled) {
            if (TextUtils.isEmpty(hint)) {
                setHint(null);
            }
            this.isProvidingHint = true;
        }

        indicatorViewController.adjustIndicatorPadding();

        // Update the label visibility with no animation, but force a state change
        updateLabelState(false);
    }

    private void setHintInternal(CharSequence hint) {
        if (!TextUtils.equals(hint, this.hint)) {
            this.hint = hint;
            collapsingTextHelper.setText(hint);
            // Reset the cutout to make room for a larger hint.
            if (!hintExpanded) {
                openCutout();
            }
        }
    }

    /**
     * Returns the hint which is displayed in the floating label, if enabled.
     *
     * @return the hint, or null if there isn't one set, or the hint is not enabled.
     */
    @Nullable
    public CharSequence getHint() {
        return hintEnabled ? hint : null;
    }

    /**
     * Set the hint to be displayed in the floating label, if enabled.
     */
    public void setHint(@Nullable CharSequence hint) {
        if (hintEnabled) {
            setHintInternal(hint);
            sendAccessibilityEvent(AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED);
        }
    }

    /**
     * Sets the collapsed hint text color, size, style from the specified TextAppearance resource.
     */
    public void setHintTextAppearance(@StyleRes int resId) {
        collapsingTextHelper.setCollapsedTextAppearance(resId);
        focusedTextColor = collapsingTextHelper.getCollapsedTextColor();

        if (spinner != null) {
            updateLabelState(false);
            // Text size might have changed so updateCell the top margin
            updateInputLayoutMargins();
        }
    }

    /**
     * Sets the collapsed hint text color from the specified ColorStateList resource.
     */
    public void setHintTextColor(@Nullable ColorStateList hintTextColor) {
        if (collapsingTextHelper.getCollapsedTextColor() != hintTextColor) {
            collapsingTextHelper.setCollapsedTextColor(hintTextColor);
            focusedTextColor = hintTextColor;

            if (spinner != null) {
                updateLabelState(false);
            }
        }
    }

    /**
     * Whether the error functionality is enabled or not in this layout. Enabling this functionality
     * before setting an error message via {@link #setError(CharSequence)}, will mean that this layout
     * will not change size when an error is displayed.
     */
    public void setErrorEnabled(boolean enabled) {
        indicatorViewController.setErrorEnabled(enabled);
    }

    /**
     * Sets the text color and size for the error message from the specified TextAppearance resource.
     */
    public void setErrorTextAppearance(@StyleRes int errorTextAppearance) {
        indicatorViewController.setErrorTextAppearance(errorTextAppearance);
    }

    /**
     * Sets the text color used by the error message in all states.
     */
    public void setErrorTextColor(@Nullable ColorStateList errorTextColor) {
        indicatorViewController.setErrorViewTextColor(errorTextColor);
    }

    @Override
    public void setEnabled(boolean enabled) {
        // Since we're set to addStatesFromChildren, we need to make sure that we set all
        // children to enabled/disabled otherwise any enabled children will wipe out our disabled
        // drawable state
        recursiveSetEnabled(this, enabled);
        super.setEnabled(enabled);
    }

    /**
     * Returns the {@code contentDescription} for accessibility purposes of the counter view, or
     * {@code null} if the counter is not enabled, not overflowed, or has no description.
     */
    @Nullable
    CharSequence getCounterOverflowDescription() {
        return null;
    }

    void setTextAppearanceCompatWithErrorFallback(TextView textView, @StyleRes int textAppearance) {
        boolean useDefaultColor = false;
        try {
            TextViewCompat.setTextAppearance(textView, textAppearance);

            if (VERSION.SDK_INT >= VERSION_CODES.M
                    && textView.getTextColors().getDefaultColor() == Color.MAGENTA) {
                // Caused by our theme not extending from Theme.Design*. On API 23 and
                // above, unresolved theme attrs result in MAGENTA rather than an exception.
                // Flag so that we use a decent default
                useDefaultColor = true;
            }
        } catch (Exception e) {
            // Caused by our theme not extending from Theme.Design*. Flag so that we use
            // a decent default
            useDefaultColor = true;
        }
        if (useDefaultColor) {
            // Probably caused by our theme not extending from Theme.Design*. Instead
            // we manually set something appropriate
            TextViewCompat.setTextAppearance(textView, R.style.TextAppearance_AppCompat_Caption);
            textView.setTextColor(ContextCompat.getColor(getContext(), R.color.design_error));
        }
    }

    private void updateTextInputBoxBounds() {
        if (spinner == null)
            return;
        int left = spinner.getLeft();
        int top = calculateBoxBackgroundTop();
        int right = spinner.getRight();
        int bottom = spinner.getBottom();

        // Create space for the wider stroke width to ensure that the outline box's stroke is not cut
        // off.
        if (boxBackgroundMode == BOX_BACKGROUND_OUTLINE) {
            left += boxStrokeWidthFocusedPx / 2;
            top -= boxStrokeWidthFocusedPx / 2;
            right -= boxStrokeWidthFocusedPx / 2;
            bottom += boxStrokeWidthFocusedPx / 2;
        }

        boxBackground.setBounds(left, top, right, bottom);
        applyBoxAttributes();
        updateEditTextBackgroundBounds();
    }

    private int calculateBoxBackgroundTop() {
        if (spinner == null) {
            return 0;
        }

        switch (boxBackgroundMode) {
            case BOX_BACKGROUND_OUTLINE:
                return spinner.getTop() + calculateLabelMarginTop();
            default:
                return 0;
        }
    }

    private int calculateLabelMarginTop() {
        if (!hintEnabled) {
            return 0;
        }

        switch (boxBackgroundMode) {
            case BOX_BACKGROUND_OUTLINE:
                return (int) (collapsingTextHelper.getCollapsedTextHeight() / 2);
            default:
                return 0;
        }
    }

    private Rect calculateCollapsedTextBounds(Rect rect) {
        if (spinner == null) {
            throw new IllegalStateException();
        }
        Rect bounds = tmpBoundsRect;

        bounds.bottom = rect.bottom;
        switch (boxBackgroundMode) {
            case BOX_BACKGROUND_OUTLINE:
                bounds.left = rect.left + spinner.getPaddingLeft();
                bounds.top = getBoxBackground().getBounds().top - calculateLabelMarginTop();
                bounds.right = rect.right - spinner.getPaddingRight();
                return bounds;
        }
        return bounds;
    }

    private Rect calculateExpandedTextBounds() {
        if (spinner == null) {
            throw new IllegalStateException();
        }

        return tmpBoundsRect;
    }

    private void updateEditTextBackgroundBounds() {
        if (spinner == null) {
            return;
        }
        Drawable editTextBackground = spinner.getBackground();
        if (editTextBackground == null) {
            return;
        }

        if (DrawableUtils.canSafelyMutateDrawable(editTextBackground)) {
            editTextBackground = editTextBackground.mutate();
        }

        final Rect editTextBounds = new Rect();
        DescendantOffsetUtils.getDescendantRect(this, spinner, editTextBounds);

        Rect editTextBackgroundBounds = editTextBackground.getBounds();
        if (editTextBackgroundBounds.left != editTextBackgroundBounds.right) {

            Rect editTextBackgroundPadding = new Rect();
            editTextBackground.getPadding(editTextBackgroundPadding);

            final int left = editTextBackgroundBounds.left - editTextBackgroundPadding.left;
            final int right = editTextBackgroundBounds.right + editTextBackgroundPadding.right * 2;
            editTextBackground.setBounds(left, editTextBackgroundBounds.top, right, spinner.getBottom());
        }
    }

    private void setBoxAttributes() {
        if (focusedStrokeColor == Color.TRANSPARENT) {
            focusedStrokeColor =
                    focusedTextColor.getColorForState(
                            getDrawableState(), focusedTextColor.getDefaultColor());
        }
    }

    /*
     * Calculates the box background color that should be set.
     *
     * The filled text field has a surface layer with value {@code ?attr/colorSurface} underneath its
     * background that is taken into account when calculating the background color.
     */
    private int calculateBoxBackgroundColor() {
        return boxBackgroundColor;
    }

    private void applyBoxAttributes() {
        if (boxBackground == null) {
            return;
        }

        setBoxAttributes();

        if (spinner != null) {
            ViewCompat.setBackground(spinner, null);
        }

        if (boxStrokeWidthPx > -1 && boxStrokeColor != Color.TRANSPARENT) {
            boxBackground.setStroke(boxStrokeWidthPx, boxStrokeColor);
        }
        boxBackground.setFillColor(ColorStateList.valueOf(calculateBoxBackgroundColor()));
        invalidate();
    }

    void updateEditTextBackground() {
        if (spinner == null) {
            return;
        }

        Drawable editTextBackground = spinner.getBackground();
        if (editTextBackground == null) {
            return;
        }

        if (DrawableUtils.canSafelyMutateDrawable(editTextBackground)) {
            editTextBackground = editTextBackground.mutate();
        }

        if (indicatorViewController.errorShouldBeShown()) {
            // Set a color filter for the error color
            editTextBackground.setColorFilter(
                    AppCompatDrawableManager.getPorterDuffColorFilter(
                            indicatorViewController.getErrorViewCurrentTextColor(), PorterDuff.Mode.SRC_IN));
        } else {
            // Else reset the color filter and refresh the drawable state so that the
            // normal tint is used
            DrawableCompat.clearColorFilter(editTextBackground);
            spinner.refreshDrawableState();
        }
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        return new SavedState(superState);
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        setError(ss.error);
        requestLayout();
    }

    /**
     * is {@code null}, the error message will be cleared.
     *
     * <p>If the error functionality has not been enabled via {@link #setErrorEnabled(boolean)}, then
     * it will be automatically enabled if {@code error} is not empty.
     *
     * @param errorText Error message to display, or null to clear
     */
    public void setError(@Nullable final CharSequence errorText) {
        if (!indicatorViewController.isErrorEnabled()) {
            if (TextUtils.isEmpty(errorText)) {
                // If error isn't enabled, and the error is empty, just return
                return;
            }
            // Else, we'll assume that they want to enable the error functionality
            setErrorEnabled(true);
        }

        if (!TextUtils.isEmpty(errorText)) {
            indicatorViewController.showError(errorText);
        } else {
            indicatorViewController.hideError();
        }
    }

    @Override
    public void draw(Canvas canvas) {
        if (boxBackground != null) {
            boxBackground.draw(canvas);
        }
        super.draw(canvas);
        if (hintEnabled) {
            collapsingTextHelper.draw(canvas);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setTextInputAccessibilityDelegate(AccessibilityDelegate delegate) {
        if (spinner != null) {
            ViewCompat.setAccessibilityDelegate(spinner, delegate);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (boxBackground != null) {
            updateTextInputBoxBounds();
        }

        if (hintEnabled && spinner != null) {
            Rect rect = tmpRect;
            DescendantOffsetUtils.getDescendantRect(this, spinner, rect);

            collapsingTextHelper.setExpandedBounds(calculateExpandedTextBounds());
            collapsingTextHelper.setCollapsedBounds(calculateCollapsedTextBounds(rect));
            collapsingTextHelper.recalculate();

            // If the label should be collapsed, set the cutout bounds on the CutoutDrawable to make sure
            // it draws with a cutout in draw().
            if (cutoutEnabled() && !hintExpanded) {
                openCutout();
            }
        }
    }

    private void collapseHint(boolean animate) {
        if (animator != null && animator.isRunning()) {
            animator.cancel();
        }
        if (animate && hintAnimationEnabled) {
            animateToExpansionFraction();
        } else {
            collapsingTextHelper.setExpansionFraction(1f);
        }
        hintExpanded = false;
        if (cutoutEnabled()) {
            openCutout();
        }
    }

    private boolean cutoutEnabled() {
        return hintEnabled && !TextUtils.isEmpty(hint) && boxBackground instanceof CutoutDrawable;
    }

    private void openCutout() {
        if (!cutoutEnabled()) {
            return;
        }
        final RectF cutoutBounds = tmpRectF;
        collapsingTextHelper.getCollapsedTextActualBounds(cutoutBounds);
        applyCutoutPadding(cutoutBounds);
        ((CutoutDrawable) boxBackground).setCutout(cutoutBounds);
    }

    private void applyCutoutPadding(RectF cutoutBounds) {
        cutoutBounds.left -= boxLabelCutoutPaddingPx;
        cutoutBounds.top -= boxLabelCutoutPaddingPx;
        cutoutBounds.right += boxLabelCutoutPaddingPx;
        cutoutBounds.bottom += boxLabelCutoutPaddingPx;
    }

    @Override
    protected void drawableStateChanged() {
        if (inDrawableStateChanged) {
            // Some of the calls below will updateCell the drawable state of child views. Since we're
            // using addStatesFromChildren we can get into infinite recursion, hence we'll just
            // exit in this instance
            return;
        }

        inDrawableStateChanged = true;

        super.drawableStateChanged();

        final int[] state = getDrawableState();

        // Drawable state has changed so see if we need to updateCell the label
        updateLabelState(ViewCompat.isLaidOut(this) && isEnabled());

        updateEditTextBackground();
        updateTextInputBoxBounds();
        updateTextInputBoxState();

        if (collapsingTextHelper.setState(state))
            invalidate();

        inDrawableStateChanged = false;
    }

    void updateTextInputBoxState() {
        if (boxBackground == null) {
            return;
        }
        final boolean isActivated = spinner != null && spinner.isActivated();
        final boolean isPressed = spinner != null && spinner.isPressed();
        final boolean hasFocus = spinner != null && spinner.hasFocus();
        final boolean isHovered = spinner != null && spinner.isHovered();

        // Update the text box's stroke based on the current state.
        if (boxBackgroundMode == BOX_BACKGROUND_OUTLINE) {
            if (!isEnabled()) {
                boxStrokeColor = disabledColor;
            } else if (indicatorViewController.errorShouldBeShown()) {
                boxStrokeColor = indicatorViewController.getErrorViewCurrentTextColor();
            } else if (hasFocus || isPressed || isActivated) {
                boxStrokeColor = focusedStrokeColor;
            } else if (isHovered) {
                boxStrokeColor = hoveredStrokeColor;
            } else {
                boxStrokeColor = defaultStrokeColor;
            }

            if ((isHovered || hasFocus || isActivated || isPressed) && isEnabled()) {
                boxStrokeWidthPx = boxStrokeWidthFocusedPx;
                adjustCornerSizeForStrokeWidth();
            } else {
                boxStrokeWidthPx = boxStrokeWidthDefaultPx;
                adjustCornerSizeForStrokeWidth();
            }
        }
        applyBoxAttributes();
    }

    @VisibleForTesting
    void animateToExpansionFraction() {
        if (collapsingTextHelper.getExpansionFraction() == 1f) {
            return;
        }
        if (this.animator == null) {
            this.animator = new ValueAnimator();
            this.animator.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
            this.animator.setDuration(LABEL_SCALE_ANIMATION_DURATION);
            this.animator.addUpdateListener(
                    animator -> collapsingTextHelper.setExpansionFraction((float) animator.getAnimatedValue()));
        }
        this.animator.setFloatValues(collapsingTextHelper.getExpansionFraction(), 1f);
        this.animator.start();
    }

    /**
     * Values for box background mode. There is either a filled background, an outline background, or
     * no background.
     */
    @IntDef({BOX_BACKGROUND_OUTLINE})
    @Retention(RetentionPolicy.SOURCE)
    @interface BoxBackgroundMode {
    }

    static class SavedState extends AbsSavedState {
        public static final Creator<SavedState> CREATOR =
                new ClassLoaderCreator<SavedState>() {
                    @Override
                    public SavedState createFromParcel(Parcel in, ClassLoader loader) {
                        return new SavedState(in, loader);
                    }

                    @Override
                    public SavedState createFromParcel(Parcel in) {
                        return new SavedState(in, null);
                    }

                    @Override
                    public SavedState[] newArray(int size) {
                        return new SavedState[size];
                    }
                };
        CharSequence error;
        boolean isPasswordToggledVisible;

        SavedState(Parcelable superState) {
            super(superState);
        }

        @SuppressLint("NewApi")
        SavedState(Parcel source, ClassLoader loader) {
            super(source, loader);
            error = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
            isPasswordToggledVisible = (source.readInt() == 1);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            TextUtils.writeToParcel(error, dest, flags);
            dest.writeInt(isPasswordToggledVisible ? 1 : 0);
        }
    }

    public static class AccessibilityDelegate extends AccessibilityDelegateCompat {
        private final SpinnerLayout layout;

        AccessibilityDelegate(SpinnerLayout layout) {
            this.layout = layout;
        }

        @Override
        public void onInitializeAccessibilityNodeInfo(View host, AccessibilityNodeInfoCompat info) {
            super.onInitializeAccessibilityNodeInfo(host, info);
            CharSequence hintText = layout.getHint();
            CharSequence counterDesc = layout.getCounterOverflowDescription();
            boolean showingText = !TextUtils.isEmpty(null);
            boolean hasHint = !TextUtils.isEmpty(hintText);
            boolean contentInvalid = !TextUtils.isEmpty(counterDesc);

            if (showingText)
                info.setText(null);
            else if (hasHint)
                info.setText(hintText);

            if (hasHint) {
                info.setHintText(hintText);
                info.setShowingHintText(!showingText);
            }

            if (contentInvalid) {
                info.setContentInvalid(true);
            }
        }

        @Override
        public void onPopulateAccessibilityEvent(View host, AccessibilityEvent event) {
            super.onPopulateAccessibilityEvent(host, event);
            CharSequence eventText = layout.getHint();
            if (!TextUtils.isEmpty(eventText)) {
                event.getText().add(eventText);
            }
        }
    }
}

