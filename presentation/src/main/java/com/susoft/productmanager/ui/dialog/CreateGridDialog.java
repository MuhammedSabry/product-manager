package com.susoft.productmanager.ui.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.susoft.productmanager.R;
import com.susoft.productmanager.databinding.DialogCreateMenuBinding;
import com.susoft.productmanager.model.CreateMenuForm;
import com.susoft.productmanager.util.InputUtils;
import com.susoft.productmanager.viewmodel.QuickMenuViewModel;

import androidx.databinding.DataBindingUtil;

import static com.susoft.productmanager.util.InputUtils.getTextEditable;
import static com.susoft.productmanager.util.InputUtils.setErrorCanceller;
import static com.susoft.productmanager.util.ToastUtil.shortError;
import static com.susoft.productmanager.util.ToastUtil.shortSuccess;

/**
 * @author Muhammed Sbary
 */
public class CreateGridDialog extends AlertDialog implements CreateMenuForm.Listener {

    private Context context;
    private DialogCreateMenuBinding binding;
    private QuickMenuViewModel viewModel;
    private int position;

    public CreateGridDialog(Context context, QuickMenuViewModel quickMenuViewModel) {
        this(context, -1, quickMenuViewModel);
    }

    public CreateGridDialog(Context context, int position, QuickMenuViewModel quickMenuViewModel) {
        super(context);

        this.context = context;
        this.position = position;
        this.viewModel = quickMenuViewModel;

        initDataBinding();
        setupDialog();
        initViews();
    }

    private void initDataBinding() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.dialog_create_menu,
                null,
                false);
        binding.setHandler(this);
    }

    private void setupDialog() {
        setView(binding.getRoot());
    }

    private void initViews() {
        setErrorCanceller(binding.menuTitle);
        setErrorCanceller(binding.columns);
        setErrorCanceller(binding.rows);
        if (position > -1)
            binding.menuTitle.setVisibility(View.GONE);
        else
            binding.menuTitle.setVisibility(View.VISIBLE);
    }

    public void onCreateClicked() {

        startLoading();

        CreateMenuForm createMenuForm = new CreateMenuForm(getTextEditable(binding.menuTitle),
                getTextEditable(binding.columns),
                getTextEditable(binding.rows));

        viewModel.onCreateMenuClicked(position, createMenuForm, this);
    }

    private void startLoading() {
        setCancelable(false);
        binding.loadingScreen.setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        setCancelable(true);
        binding.loadingScreen.setVisibility(View.GONE);
    }

    @Override
    public void onTitleError(String message) {
        binding.menuTitle.setError(message);
    }

    @Override
    public void onColumnsError(String message) {
        binding.columns.setError(message);
    }

    @Override
    public void onRowsError(String message) {
        binding.rows.setError(message);
    }

    @Override
    public void onSuccess(String message) {
        stopLoading();
        InputUtils.clearInputLayoutsErrorAndText(binding.menuTitle,
                binding.columns,
                binding.rows);
        shortSuccess(message);
        dismiss();
    }

    @Override
    public void onFallBack() {
        stopLoading();
    }

    @Override
    public void onFail(String message) {
        stopLoading();
        shortError(message);
    }
}
