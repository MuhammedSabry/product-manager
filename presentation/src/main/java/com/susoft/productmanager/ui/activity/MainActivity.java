package com.susoft.productmanager.ui.activity;

import android.os.Bundle;
import android.view.View;

import com.susoft.productmanager.ProductApp;
import com.susoft.productmanager.R;
import com.susoft.productmanager.databinding.ActivityMainBinding;
import com.susoft.productmanager.model.BaseFeedbackListener;
import com.susoft.productmanager.ui.adapter.MainPagerAdapter;
import com.susoft.productmanager.util.ToastUtil;
import com.susoft.productmanager.viewmodel.MainViewModel;

import androidx.appcompat.app.ActionBar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

/**
 * @author Muhammed Sabry
 */
public class MainActivity extends BaseTrackedActivity implements BaseFeedbackListener {

    private ActivityMainBinding mainBinding;
    private MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity();
    }

    private void initializeActivity() {
        initDataBinding();
        initViewModel();
        setupToolbar();
        initViewPager();
        mainBinding.offlineOverlay.retryButton.setOnClickListener(v -> this.onRetryClicked());
        mainBinding.offlineOverlay.exitButton.setOnClickListener(v -> this.onExitClicked());
    }

    private void onExitClicked() {
        finish();
    }

    private void onRetryClicked() {
        mainBinding.offlineOverlay.retryButton.setEnabled(false);
        viewModel.retryToConnect(new BaseFeedbackListener() {
            @Override
            public void onSuccess(String message) {
                ToastUtil.shortSuccess(message);
            }

            @Override
            public void onFail(String message) {
                ToastUtil.shortError(message);
                mainBinding.offlineOverlay.retryButton.setEnabled(true);
            }
        });
    }

    private void initViewModel() {
        this.viewModel = ViewModelProviders.of(this, ProductApp.getViewModelFactory()).get(MainViewModel.class);
        viewModel.setListener(this);
    }

    private void initDataBinding() {
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
    }

    private void setupToolbar() {
        setSupportActionBar(mainBinding.toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null)
            supportActionBar.setTitle(null);
    }

    private void initViewPager() {
        MainPagerAdapter pagerAdapter = new MainPagerAdapter(getSupportFragmentManager());

        mainBinding.viewPager.setOffscreenPageLimit(2);
        mainBinding.viewPager.setAdapter(pagerAdapter);

        mainBinding.tabLayout.setupWithViewPager(mainBinding.viewPager);
    }

    @Override
    public void onSuccess(String message) {
        mainBinding.offlineOverlay.getRoot().setVisibility(View.GONE);
    }

    @Override
    public void onFail(String message) {
        mainBinding.offlineOverlay.getRoot().setVisibility(View.VISIBLE);
    }
}
