package com.susoft.productmanager.ui.adapter.dropdown;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.susoft.productmanager.R;
import com.susoft.productmanager.domain.model.ProductType;

import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ProductTypeAdapter extends ArrayAdapter<ProductType> {
    private List<ProductType> types;

    public ProductTypeAdapter(@NonNull Context context, @NonNull List<ProductType> types) {
        super(context, R.layout.itemview_spinner_normal, types);
        this.types = types;
        setItems(types);
        setDropDownViewResource(R.layout.itemview_spinner_dropdown);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        if (position >= 0 && position < types.size())
            view.setText(types.get(position).getName(Locale.getDefault().getLanguage()));
        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);

        String languageCode = Locale.getDefault().getLanguage();
        if (position >= 0 && position < types.size())
            view.setText(types.get(position).getName(languageCode));
        return view;
    }

    @Override
    public int getCount() {
        return types.size();
    }

    public void setItems(List<ProductType> types) {
        this.types.clear();
        this.types.addAll(types);
        notifyDataSetChanged();
    }
}
