package com.susoft.productmanager.ui.dialog;

import android.content.Context;
import android.view.View;

import com.susoft.productmanager.R;
import com.susoft.productmanager.databinding.DialogCreateCategoryBinding;
import com.susoft.productmanager.domain.model.Category;
import com.susoft.productmanager.model.CreateCategoryForm;
import com.susoft.productmanager.viewmodel.CreateProductViewModel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import static com.susoft.productmanager.util.InputUtils.clearInputLayoutsErrorAndText;
import static com.susoft.productmanager.util.InputUtils.getTextEditable;
import static com.susoft.productmanager.util.InputUtils.setErrorCanceller;
import static com.susoft.productmanager.util.InputUtils.setText;
import static com.susoft.productmanager.util.ToastUtil.shortError;
import static com.susoft.productmanager.util.ToastUtil.shortSuccess;

/**
 * @author Muhammed Sbary
 */
public class CreateCategoryDialog extends AlertDialog implements CreateCategoryForm.Listener {

    private DialogCreateCategoryBinding binding;
    private OnCategoryDataChangedListener listener;
    private CreateProductViewModel viewModel;

    public CreateCategoryDialog(@NonNull Context context, CreateProductViewModel viewModel, OnCategoryDataChangedListener listener) {
        super(context);
        this.listener = listener;
        this.viewModel = viewModel;

        initDataBinding();
        setupDialog();
        initViews();
    }

    private void onCategoryInsertionSuccess(String message) {
        stopLoading();

        shortSuccess(message);
        if (listener != null)
            listener.onCategoryDataChanged();

        dismiss();
    }

    private void setupDialog() {
        setView(binding.getRoot());
    }

    private void initDataBinding() {
        binding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.dialog_create_category,
                null,
                false);
        binding.setHandler(this);
    }

    private void initViews() {
        setErrorCanceller(binding.category);
        setErrorCanceller(binding.categoryId);
        setErrorCanceller(binding.categoryLevel);
        clearInputLayoutsErrorAndText(binding.category, binding.categoryId, binding.categoryLevel);
    }

    public void onCreateCategoryClicked() {

        startLoading();
        CreateCategoryForm categoryForm = new CreateCategoryForm(getTextEditable(binding.categoryId),
                getTextEditable(binding.category),
                getTextEditable(binding.categoryLevel));

        viewModel.createCategory(categoryForm, binding.categoryId.isEnabled(), this);
    }

    @Override
    public void onIdError(String message) {
        binding.categoryId.setError(message);
    }

    @Override
    public void onCategoryError(String message) {
        binding.category.setError(message);
    }

    @Override
    public void onLevelError(String message) {
        binding.categoryLevel.setError(message);
    }

    @Override
    public void onSuccess(String message) {
        onCategoryInsertionSuccess(message);
    }

    @Override
    public void onFallBack() {
        stopLoading();
    }

    private void startLoading() {
        setCancelable(false);
        binding.loadingScreen.setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        setCancelable(true);
        binding.loadingScreen.setVisibility(View.GONE);
    }

    @Override
    public void onFail(String message) {
        onCategoryInsertionFail(message);
    }

    public interface OnCategoryDataChangedListener {
        void onCategoryDataChanged();
    }

    private void onCategoryInsertionFail(String message) {
        stopLoading();
        shortError(message);
    }

    public void show() {
    }

    public void show(Category category, int level) {
        super.show();
        initViews();
        if (category == null) {
            binding.categoryId.setEnabled(true);
            binding.createCategory.setText(R.string.create_button_label);
        } else {
            binding.categoryId.setEnabled(false);
            setText(binding.categoryId, category.getServerId());
            setText(binding.category, category.getValue());
            binding.createCategory.setText(R.string.save_button_label);
        }
        setText(binding.categoryLevel, level);
    }
}
