package com.susoft.productmanager.ui.dialog;

import android.content.Context;

import com.susoft.productmanager.R;
import com.susoft.productmanager.databinding.DialogCellManagerBinding;
import com.susoft.productmanager.domain.model.Cell;
import com.susoft.productmanager.util.ValidationUtils;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

public class CellManagerDialog extends AlertDialog {

    private final ActionsListener listener;
    private DialogCellManagerBinding binding;
    private Cell cell;
    private int position;

    public CellManagerDialog(@NonNull Context context, ActionsListener listener) {
        super(context);
        this.listener = listener;
        initDataBinding();
        setupDialog();
        initViews();
    }

    private void setupDialog() {
        setView(binding.getRoot());
        setCancelable(true);
    }

    private void initDataBinding() {
        binding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.dialog_cell_manager,
                null,
                false);
        binding.setHandler(this);
    }

    private void initViews() {
        binding.addSubMenu.setEnabled(cell == null);
        binding.editCell.setEnabled(cell != null && !ValidationUtils.isValidText(cell.getProductId()));
        binding.deleteCell.setEnabled(cell != null);
    }

    @Override
    public void show() {
    }

    public void show(int position, Cell cell) {
        super.show();
        this.cell = cell;
        this.position = position;
        initViews();
    }

    public void onAddClicked() {
        listener.onAddClicked(position);
        dismiss();
    }

    public void onEditClicked() {
        listener.onEditClicked(cell);
        dismiss();
    }

    public void onDeleteClicked() {
        listener.onDeleteClicked(cell);
        dismiss();
    }

    public interface ActionsListener {
        void onAddClicked(int position);

        void onEditClicked(Cell cell);

        void onDeleteClicked(Cell cell);
    }
}
