package com.susoft.productmanager.ui.custom;

/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.R;
import com.google.android.material.animation.AnimationUtils;
import com.google.android.material.animation.AnimatorSetCompat;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.ColorInt;
import androidx.annotation.IntDef;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.view.ViewCompat;
import androidx.legacy.widget.Space;

import static android.view.View.TRANSLATION_Y;
import static android.view.View.VISIBLE;

/**
 * Controller for indicator views underneath the text input line in {@link
 * com.google.android.material.textfield.TextInputLayout}. This class controls helper and error views.
 */
final class IndicatorViewController {

    private static final int ERROR_INDEX = 0;
    /**
     * Duration for the caption's vertical translation animation.
     */
    private static final int CAPTION_TRANSLATE_Y_ANIMATION_DURATION = 217;
    /**
     * Duration for the caption's opacity fade animation.
     */
    private static final int CAPTION_OPACITY_FADE_ANIMATION_DURATION = 167;
    private static final int CAPTION_STATE_NONE = 0;
    private static final int CAPTION_STATE_ERROR = 1;
    private static final int CAPTION_STATE_HELPER_TEXT = 2;
    private final Context context;
    private final SpinnerLayout textInputView;
    private final float captionTranslationYPx;
    private LinearLayout indicatorArea;
    private int indicatorsAdded;
    private FrameLayout captionArea;
    private int captionViewsAdded;
    @Nullable
    private Animator captionAnimator;
    private int captionDisplayed;
    private int captionToShow;
    private CharSequence errorText;
    private boolean errorEnabled;
    private TextView errorView;
    private int errorTextAppearance;
    @Nullable
    private ColorStateList errorViewTextColor;

    IndicatorViewController(SpinnerLayout textInputView) {
        this.context = textInputView.getContext();
        this.textInputView = textInputView;
        this.captionTranslationYPx =
                context.getResources().getDimensionPixelSize(R.dimen.design_textinput_caption_translate_y);
    }

    void showError(final CharSequence errorText) {
        cancelCaptionAnimator();
        this.errorText = errorText;
        errorView.setText(errorText);

        // If error is not already shown, show error.
        if (captionDisplayed != CAPTION_STATE_ERROR) {
            captionToShow = CAPTION_STATE_ERROR;
        }
        updateCaptionViewsVisibility(
                captionDisplayed, captionToShow, shouldAnimateCaptionView(errorView, errorText));
    }

    void hideError() {
        errorText = null;
        cancelCaptionAnimator();
        // Hide  error if it's shown.
        if (captionDisplayed == CAPTION_STATE_ERROR) {
            // Otherwise, just hide the error.
            captionToShow = CAPTION_STATE_NONE;
        }
        updateCaptionViewsVisibility(
                captionDisplayed, captionToShow, shouldAnimateCaptionView(errorView, null));
    }

    /**
     * Check if the caption view should animate. Only animate the caption view if we're enabled, laid
     * out, and have a different caption message.
     *
     * @param captionView The view that contains text for the caption underneath the text input area
     * @param captionText The text for the caption view
     * @return Whether the view should animate when setting the caption
     */
    private boolean shouldAnimateCaptionView(
            TextView captionView, @Nullable final CharSequence captionText) {
        return ViewCompat.isLaidOut(textInputView)
                && textInputView.isEnabled()
                && (captionToShow != captionDisplayed
                || captionView == null
                || !TextUtils.equals(captionView.getText(), captionText));
    }

    @SuppressLint("RestrictedApi")
    private void updateCaptionViewsVisibility(
            final @CaptionDisplayState int captionToHide,
            final @CaptionDisplayState int captionToShow,
            boolean animate) {

        if (animate) {
            final AnimatorSet captionAnimator = new AnimatorSet();
            this.captionAnimator = captionAnimator;
            List<Animator> captionAnimatorList = new ArrayList<>();

            createCaptionAnimators(
                    captionAnimatorList,
                    errorEnabled,
                    errorView,
                    captionToHide,
                    captionToShow);

            AnimatorSetCompat.playTogether(captionAnimator, captionAnimatorList);
            final TextView captionViewToHide = getCaptionViewFromDisplayState(captionToHide);
            final TextView captionViewToShow = getCaptionViewFromDisplayState(captionToShow);

            captionAnimator.addListener(
                    new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animator) {
                            captionDisplayed = captionToShow;
                            IndicatorViewController.this.captionAnimator = null;
                            if (captionViewToHide != null) {
                                captionViewToHide.setVisibility(View.INVISIBLE);
                                if (captionToHide == CAPTION_STATE_ERROR && errorView != null) {
                                    errorView.setText(null);
                                }
                            }
                        }

                        @Override
                        public void onAnimationStart(Animator animator) {
                            if (captionViewToShow != null) {
                                captionViewToShow.setVisibility(VISIBLE);
                            }
                        }
                    });
            captionAnimator.start();
        } else {
            setCaptionViewVisibilities(captionToHide, captionToShow);
        }
        textInputView.updateEditTextBackground();
        textInputView.updateLabelState(animate);
        textInputView.updateTextInputBoxState();
    }

    private void setCaptionViewVisibilities(
            @CaptionDisplayState int captionToHide, @CaptionDisplayState int captionToShow) {
        if (captionToHide == captionToShow) {
            return;
        }

        if (captionToShow != CAPTION_STATE_NONE) {
            TextView captionViewToShow = getCaptionViewFromDisplayState(captionToShow);
            if (captionViewToShow != null) {
                captionViewToShow.setVisibility(VISIBLE);
                captionViewToShow.setAlpha(1f);
            }
        }

        if (captionToHide != CAPTION_STATE_NONE) {
            TextView captionViewDisplayed = getCaptionViewFromDisplayState(captionToHide);
            if (captionViewDisplayed != null) {
                captionViewDisplayed.setVisibility(View.INVISIBLE);
                // Only set the caption text to null if it's the error.
                if (captionToHide == CAPTION_STATE_ERROR) {
                    captionViewDisplayed.setText(null);
                }
            }
        }
        captionDisplayed = captionToShow;
    }

    private void createCaptionAnimators(
            List<Animator> captionAnimatorList,
            boolean captionEnabled,
            TextView captionView,
            @CaptionDisplayState int captionToHide,
            @CaptionDisplayState int captionToShow) {
        // If caption view is null or not enabled, do nothing.
        if (captionView == null || !captionEnabled) {
            return;
        }
        // If the caption view should be shown, set alpha to 1f.
        if ((IndicatorViewController.CAPTION_STATE_ERROR == captionToShow) || (IndicatorViewController.CAPTION_STATE_ERROR == captionToHide)) {
            captionAnimatorList.add(
                    createCaptionOpacityAnimator(captionView, captionToShow == IndicatorViewController.CAPTION_STATE_ERROR));
            if (captionToShow == IndicatorViewController.CAPTION_STATE_ERROR) {
                captionAnimatorList.add(createCaptionTranslationYAnimator(captionView));
            }
        }
    }

    private ObjectAnimator createCaptionOpacityAnimator(TextView captionView, boolean display) {
        float endValue = display ? 1f : 0f;
        ObjectAnimator opacityAnimator = ObjectAnimator.ofFloat(captionView, View.ALPHA, endValue);
        opacityAnimator.setDuration(CAPTION_OPACITY_FADE_ANIMATION_DURATION);
        opacityAnimator.setInterpolator(AnimationUtils.LINEAR_INTERPOLATOR);
        return opacityAnimator;
    }

    private ObjectAnimator createCaptionTranslationYAnimator(TextView captionView) {
        ObjectAnimator translationYAnimator =
                ObjectAnimator.ofFloat(captionView, TRANSLATION_Y, -captionTranslationYPx, 0f);
        translationYAnimator.setDuration(CAPTION_TRANSLATE_Y_ANIMATION_DURATION);
        translationYAnimator.setInterpolator(AnimationUtils.LINEAR_OUT_SLOW_IN_INTERPOLATOR);
        return translationYAnimator;
    }

    private void cancelCaptionAnimator() {
        if (captionAnimator != null) {
            captionAnimator.cancel();
        }
    }

    private boolean isCaptionView() {
        return true;
    }

    @SuppressLint("SwitchIntDef")
    @Nullable
    private TextView getCaptionViewFromDisplayState(@CaptionDisplayState int captionDisplayState) {
        switch (captionDisplayState) {
            case CAPTION_STATE_ERROR:
                return errorView;
            default: // No caption displayed, fall out and return null.
        }
        return null;
    }

    void adjustIndicatorPadding() {
        if (canAdjustIndicatorPadding()) {
            // Add padding to the indicators so that they match the EditText
            ViewCompat.setPaddingRelative(
                    indicatorArea,
                    ViewCompat.getPaddingStart(textInputView.getSpinner()),
                    0,
                    ViewCompat.getPaddingEnd(textInputView.getSpinner()),
                    0);
        }
    }

    private boolean canAdjustIndicatorPadding() {
        return indicatorArea != null && textInputView.getSpinner() != null;
    }

    private void addIndicator(TextView indicator) {
        if (indicatorArea == null && captionArea == null) {
            indicatorArea = new LinearLayout(context);
            indicatorArea.setOrientation(LinearLayout.HORIZONTAL);
            textInputView.addView(indicatorArea, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

            captionArea = new FrameLayout(context);
            indicatorArea.addView(
                    captionArea,
                    -1,
                    new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

            final Space spacer = new Space(context);
            final LayoutParams spacerLp = new LinearLayout.LayoutParams(0, 0, 1f);
            indicatorArea.addView(spacer, spacerLp);

            if (textInputView.getSpinner() != null) {
                adjustIndicatorPadding();
            }
        }

        if (isCaptionView()) {
            captionArea.setVisibility(VISIBLE);
            captionArea.addView(indicator);
            captionViewsAdded++;
        } else {
            if (indicatorArea != null)
                indicatorArea.addView(indicator, IndicatorViewController.ERROR_INDEX);
        }
        if (indicatorArea != null)
            indicatorArea.setVisibility(VISIBLE);
        indicatorsAdded++;
    }

    private void removeIndicator(TextView indicator) {
        if (indicatorArea == null) {
            return;
        }

        if (isCaptionView() && captionArea != null) {
            captionViewsAdded--;
            setViewGroupGoneIfEmpty(captionArea, captionViewsAdded);
            captionArea.removeView(indicator);
        } else {
            indicatorArea.removeView(indicator);
        }
        indicatorsAdded--;
        setViewGroupGoneIfEmpty(indicatorArea, indicatorsAdded);
    }

    private void setViewGroupGoneIfEmpty(ViewGroup viewGroup, int indicatorsAdded) {
        if (indicatorsAdded == 0) {
            viewGroup.setVisibility(View.GONE);
        }
    }

    boolean isErrorEnabled() {
        return errorEnabled;
    }

    void setErrorEnabled(boolean enabled) {
        // If the enabled state is the same as before, do nothing.
        if (errorEnabled == enabled) {
            return;
        }

        // Otherwise, adjust enabled state.
        cancelCaptionAnimator();

        if (enabled) {
            errorView = new AppCompatTextView(context);
            errorView.setId(R.id.textinput_error);
            setErrorTextAppearance(errorTextAppearance);
            setErrorViewTextColor(errorViewTextColor);
            errorView.setVisibility(View.INVISIBLE);
            ViewCompat.setAccessibilityLiveRegion(errorView, ViewCompat.ACCESSIBILITY_LIVE_REGION_POLITE);
            addIndicator(errorView);
        } else {
            hideError();
            removeIndicator(errorView);
            errorView = null;
            textInputView.updateEditTextBackground();
            textInputView.updateTextInputBoxState();
        }
        errorEnabled = enabled;
    }

    boolean errorShouldBeShown() {
        return isCaptionStateError(captionToShow);
    }

    private boolean isCaptionStateError(@CaptionDisplayState int captionState) {
        return captionState == CAPTION_STATE_ERROR
                && errorView != null
                && !TextUtils.isEmpty(errorText);
    }

    @ColorInt
    int getErrorViewCurrentTextColor() {
        return errorView != null ? errorView.getCurrentTextColor() : -1;
    }

    @Nullable
    ColorStateList getErrorViewTextColors() {
        return errorView != null ? errorView.getTextColors() : null;
    }

    void setErrorViewTextColor(ColorStateList errorViewTextColor) {
        this.errorViewTextColor = errorViewTextColor;
        if (errorView != null && errorViewTextColor != null) {
            errorView.setTextColor(errorViewTextColor);
        }
    }

    void setErrorTextAppearance(@StyleRes int resId) {
        this.errorTextAppearance = resId;
        if (errorView != null) {
            textInputView.setTextAppearanceCompatWithErrorFallback(errorView, resId);
        }
    }

    /**
     * Values for caption display state constants. There is either an error displayed, helper text
     * displayed, or no caption.
     */
    @IntDef({CAPTION_STATE_NONE, CAPTION_STATE_ERROR, CAPTION_STATE_HELPER_TEXT})
    @Retention(RetentionPolicy.SOURCE)
    private @interface CaptionDisplayState {
    }
}