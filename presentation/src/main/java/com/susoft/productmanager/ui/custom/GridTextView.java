package com.susoft.productmanager.ui.custom;

import android.content.Context;
import android.util.AttributeSet;

import com.susoft.productmanager.domain.model.Cell;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

/**
 * @author Muhammed Sbary
 */
public class GridTextView extends AppCompatTextView {

    private Cell cell;

    public GridTextView(Context context, Cell cell) {
        super(context);
        this.cell = cell;
    }

    public GridTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Cell getCell() {
        return cell;
    }
}
