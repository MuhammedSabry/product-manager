package com.susoft.productmanager.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.susoft.productmanager.ProductApp;
import com.susoft.productmanager.R;
import com.susoft.productmanager.databinding.FragmentQuickMenuBinding;
import com.susoft.productmanager.domain.model.Category;
import com.susoft.productmanager.domain.model.Cell;
import com.susoft.productmanager.domain.model.Grid;
import com.susoft.productmanager.domain.model.Menu;
import com.susoft.productmanager.domain.model.Product;
import com.susoft.productmanager.model.BaseFormListener;
import com.susoft.productmanager.ui.adapter.dropdown.CategoriesSpinnerAdapter;
import com.susoft.productmanager.ui.adapter.dropdown.MenuSpinnerAdapter;
import com.susoft.productmanager.ui.adapter.recyclerview.CellListAdapter;
import com.susoft.productmanager.ui.adapter.recyclerview.ProductsListAdapter;
import com.susoft.productmanager.ui.custom.GridTextView;
import com.susoft.productmanager.ui.dialog.CellManagerDialog;
import com.susoft.productmanager.ui.dialog.ConfirmationDialog;
import com.susoft.productmanager.ui.dialog.CreateGridDialog;
import com.susoft.productmanager.ui.dialog.EditCellDialog;
import com.susoft.productmanager.util.ValidationUtils;
import com.susoft.productmanager.viewmodel.QuickMenuViewModel;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.susoft.productmanager.data.network.NetworkConstants.REQUEST_TIMEOUT_IN_MILLISECONDS;
import static com.susoft.productmanager.util.ToastUtil.longError;
import static com.susoft.productmanager.util.ToastUtil.shortError;
import static com.susoft.productmanager.util.ToastUtil.shortSuccess;

/**
 * @author Muhammed Sbary
 */
public class QuickMenuFragment extends Fragment implements CellListAdapter.CellListener,
        EditCellDialog.OnSelectImageRequestedListener, CellManagerDialog.ActionsListener {

    private Context context;

    private FragmentQuickMenuBinding binding;

    private QuickMenuViewModel viewModel;

    private ProductsListAdapter productsAdapter;

    private CellListAdapter cellListAdapter;

    private MenuSpinnerAdapter menuSpinnerAdapter;

    private CategoriesSpinnerAdapter categoriesSpinnerAdapter;

    private CreateGridDialog menuDialog;

    private GridLayoutManager layoutManager;

    private EditCellDialog editCellDialog;

    private Handler loadingHandler = new Handler();
    private Runnable loadingRunnable = () -> longError("Request timeout!, press refresh to retry");

    private CellManagerDialog confirmationDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initViewModel();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initDataBinding(inflater, container);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        subscribeToProductsData();
        subscribeToMenus();
        subscribeToCategories();
        subscribeToMenuCells();
        subscribeToMenuStack();
        listenForClearSearchPress();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void listenForClearSearchPress() {
        binding.productSearch.setOnTouchListener((view, event) -> {
            final int DRAWABLE_RIGHT = 2;

            if (event.getAction() == MotionEvent.ACTION_UP
                    && binding.productSearch.getCompoundDrawables()[DRAWABLE_RIGHT] != null) {
                if (event.getRawX() >= (binding.productSearch.getRight() -
                        binding.productSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()
                        - binding.productSearch.getPaddingRight())) {
                    clearSearchText();
                    return true;
                }
            }
            return false;
        });
    }

    private void clearSearchText() {
        binding.productSearch.setText("");
    }

    @Override
    public void onCreateOptionsMenu(@NonNull android.view.Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.product_list_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh_action:
                onRefreshClicked();
                return true;
            case R.id.add_menu_action:
                onAddMenuClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onRefreshClicked() {
        startLoading();
        viewModel.refreshData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeTimeoutRunnable();
    }

    private void initViewModel() {
        viewModel = ViewModelProviders.of(this, ProductApp.getViewModelFactory()).get(QuickMenuViewModel.class);
    }

    private void initDataBinding(@NonNull LayoutInflater inflater, ViewGroup container) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_quick_menu, container, false);
        binding.setHandler(this);
    }

    private void initViews() {
        binding.productSearch.setText(viewModel.getSearchString());
        initMenuCells();
        initProductsList();
        initMenuSpinner();
        initCategorySpinner();
        if (viewModel.shouldOpenEditCellDialog())
            openEditCellDialog();
    }

    private void initMenuCells() {
        cellListAdapter = new CellListAdapter(null, Collections.emptyList(), context, this);
        binding.cellList.setAdapter(cellListAdapter);
        binding.cellList.setEmptyView(binding.emptyMenus);
    }

    private void initProductsList() {
        productsAdapter = new ProductsListAdapter(context, Collections.emptyList());
        binding.productsList.setAdapter(productsAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        binding.productsList.setLayoutManager(layoutManager);
        binding.productsList.setDividerDrawable(R.drawable.vertical_recycler_divider, DividerItemDecoration.VERTICAL);
        binding.productsList.setEmptyView(binding.emptyProducts);
        binding.productsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (viewModel.isLoading())
                    return;
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                    viewModel.onEndOfListReached();
                }
            }
        });

    }

    private void initMenuSpinner() {
        menuSpinnerAdapter = new MenuSpinnerAdapter(context, new ArrayList<>());
        binding.menuSpinner.setAdapter(menuSpinnerAdapter);
    }

    private void initCategorySpinner() {
        categoriesSpinnerAdapter = new CategoriesSpinnerAdapter(context, new ArrayList<>(), Category.allCategories());
        binding.categorySpinner.setAdapter(categoriesSpinnerAdapter);
    }

    private void subscribeToProductsData() {
        viewModel.getSearchProducts().observe(this, productsAdapter::setProducts);
    }

    private void subscribeToMenus() {
        viewModel.getMenus().observe(this, this::setMenus);
    }

    private void subscribeToCategories() {
        viewModel.getCategories().observe(this, this::setCategories);
    }

    private void subscribeToMenuCells() {
        viewModel.getCells().observe(this, this::setCells);
    }

    private void subscribeToMenuStack() {
        viewModel.getCellStackLiveData().observe(this, this::initStack);
    }

    private void initStack(List<Cell> cellStack) {

        //Remove all the views from the stack's linear layout
        //except for the home image button
        for (int i = binding.menuStack.getChildCount() - 1; i > 0; i--)
            binding.menuStack.removeViewAt(i);

        for (int i = 0; i < cellStack.size(); i++) {
            GridTextView gridTextView = new GridTextView(context, cellStack.get(i));

            styleMenuTextView(cellStack.get(i), i < cellStack.size() - 1, gridTextView);

            gridTextView.setOnClickListener(this::onStackItemClicked);

            binding.menuStack.addView(gridTextView);
        }

        if (!cellStack.isEmpty())
            DrawableCompat.setTint(binding.menuHome.getDrawable(),
                    ContextCompat.getColor(context, R.color.colorAccent));
        else
            DrawableCompat.setTint(binding.menuHome.getDrawable(),
                    ContextCompat.getColor(context, R.color.primaryLight));
    }

    private void onStackItemClicked(View v) {
        boolean isConsumed = viewModel.onStackClicked(((GridTextView) v).getCell());
        if (isConsumed)
            startLoading();
    }

    private void styleMenuTextView(Cell cell, boolean isLastView, GridTextView gridTextView) {
        if (TextUtils.isEmpty(cell.getText()))
            gridTextView.setText(getString(R.string.directory_text,
                    getString(R.string.cell) + " " + cell.getPosition()));
        else
            gridTextView.setText(getString(R.string.directory_text,
                    cell.getText()));
        gridTextView.setTextSize(20);

        if (isLastView)
            gridTextView.setTextColor(getResources().getColor(R.color.colorAccent));
        else
            gridTextView.setTextColor(getResources().getColor(R.color.primaryLight));
    }

    private void setMenus(List<Menu> menus) {
        menuSpinnerAdapter.setMenus(menus);
        setMenuIfExists();
    }

    private void setMenuIfExists() {
        Menu currentMenu = viewModel.getCurrentMenu();
        if (currentMenu != null)
            binding.menuSpinner.setSelection(menuSpinnerAdapter.getPosition(currentMenu));
    }

    private void setCategories(List<Category> categories) {
        categoriesSpinnerAdapter.setItems(categories);
        setCategoryIfExists();
    }

    private void setCategoryIfExists() {
        if (viewModel.getCurrentCategory() != null)
            binding.categorySpinner.setSelection(categoriesSpinnerAdapter.getPosition(viewModel.getCurrentCategory()));
    }

    private void setCells(List<Cell> cells) {
        Grid grid = viewModel.getCurrentGrid();

        if (layoutManager == null)
            layoutManager = new GridLayoutManager(context, grid.getColumns());

        else if (layoutManager.getSpanCount() != grid.getColumns())
            layoutManager.setSpanCount(grid.getColumns());

        binding.cellList.setLayoutManager(layoutManager);

        cellListAdapter.setCells(grid, cells);
        stopLoading();
    }

    private void stopLoading() {
        binding.loadingScreen.setVisibility(View.GONE);
        removeTimeoutRunnable();
    }

    private void removeTimeoutRunnable() {
        loadingHandler.removeCallbacks(loadingRunnable);
    }

    private void startLoading() {
        binding.loadingScreen.setVisibility(View.VISIBLE);

        loadingHandler.postDelayed(loadingRunnable, REQUEST_TIMEOUT_IN_MILLISECONDS);
    }

    private void onAddMenuClicked() {
        if (menuDialog == null)
            createAddMenuDialog();

        if (!menuDialog.isShowing())
            menuDialog.show();
    }

    private void createAddMenuDialog() {
        menuDialog = new CreateGridDialog(context, viewModel);
    }

    public void onMenuSelected(int position) {
        boolean isConsumed = viewModel.onMenuSelected(menuSpinnerAdapter.getItem(position));
        if (isConsumed)
            startLoading();
    }

    public void onCategorySelected(int position) {
        viewModel.onCategorySelected(categoriesSpinnerAdapter.getItem(position));
    }

    public void onSearchTextChanged(Editable editable) {
        viewModel.onQueryChanged(editable);
        if (ValidationUtils.isValidText(editable.toString()))
            binding.productSearch.setCompoundDrawablesWithIntrinsicBounds(null,
                    null,
                    context.getResources().getDrawable(R.drawable.ic_clear),
                    null);
        else
            binding.productSearch.setCompoundDrawablesWithIntrinsicBounds(null,
                    null,
                    null,
                    null);
    }

    public void onHomeMenuClicked() {
        boolean isConsumed = viewModel.onHomeMenuClicked();
        if (isConsumed)
            startLoading();
    }

    private void openEditCellDialog() {
        if (editCellDialog == null)
            editCellDialog = new EditCellDialog(context, viewModel, this);

        if (!editCellDialog.isShowing())
            editCellDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK && result != null)
                editCellDialog.onImageSelected(result.getUri());
        }
    }

    @Override
    public void onAddClicked(int position) {
        CreateGridDialog subMenuDialog = new CreateGridDialog(context, position, viewModel);

        if (!subMenuDialog.isShowing())
            subMenuDialog.show();
    }

    @Override
    public void onEditClicked(Cell cell) {
        viewModel.setCellToEdit(cell);
        openEditCellDialog();
    }

    @Override
    public void onDeleteClicked(Cell cell) {
        openDeleteDialog(cell);
    }

    private void openDeleteDialog(Cell cell) {
        ConfirmationDialog confirmationDialog = new ConfirmationDialog(context, () -> {
            startLoading();
            viewModel.onDeleteClicked(cell, new BaseFormListener() {
                @Override
                public void onSuccess(String message) {
                    shortSuccess(message);
                }

                @Override
                public void onFallBack() {
                    stopLoading();
                }

                @Override
                public void onFail(String message) {
                    shortError(message);
                    stopLoading();
                }
            });
        });

        confirmationDialog.show(getString(R.string.delete_message,
                TextUtils.isEmpty(cell.getText()) ? getString(R.string.cell) + " " + cell.getPosition() : cell.getText(),
                getString(R.string.cell)));
    }

    private void openCellManager(int position, Cell cell) {

        if (confirmationDialog == null)
            confirmationDialog = new CellManagerDialog(context, this);

        if (!confirmationDialog.isShowing())
            confirmationDialog.show(position, cell);
    }

    @Override
    public void onCellClicked(Cell cell) {
        viewModel.onCellClicked(cell);
        startLoading();
    }

    @Override
    public void onCellLongClicked(int position, Cell cell) {
        openCellManager(position, cell);
    }

    @Override
    public void onProductDraggedOverCell(int position, Product product) {
        viewModel.onProductDraggedOverCell(position, product);
        startLoading();
    }

    @Override
    public void requestImage() {
        CropImage.activity()
                .setAspectRatio(1, 1)
                .setMaxCropResultSize(1280, 1280)
                .start(context, this);
    }
}
