package com.susoft.productmanager.ui.custom;

import android.graphics.Rect;
import android.view.View;

import com.susoft.productmanager.util.ViewUtil;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SpacingItemDecorator extends RecyclerView.ItemDecoration {

    private int halfSpace;

    public SpacingItemDecorator(int spaceInDp) {
        this.halfSpace = ViewUtil.toPx(spaceInDp);
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {

        outRect.top = halfSpace;
        outRect.bottom = halfSpace;
        outRect.left = halfSpace;
        outRect.right = halfSpace;
    }

}
