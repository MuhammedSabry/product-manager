package com.susoft.productmanager.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.textfield.TextInputLayout;
import com.susoft.productmanager.ProductApp;
import com.susoft.productmanager.R;
import com.susoft.productmanager.databinding.FragmentCreateProductBinding;
import com.susoft.productmanager.domain.model.Category;
import com.susoft.productmanager.domain.model.Product;
import com.susoft.productmanager.domain.model.ProductType;
import com.susoft.productmanager.model.BaseFeedbackListener;
import com.susoft.productmanager.model.CreateProductListener;
import com.susoft.productmanager.ui.adapter.dropdown.CategoriesSpinnerAdapter;
import com.susoft.productmanager.ui.adapter.dropdown.OptionsAdapter;
import com.susoft.productmanager.ui.adapter.dropdown.ProductCursorAdapter;
import com.susoft.productmanager.ui.adapter.dropdown.ProductTypeAdapter;
import com.susoft.productmanager.ui.dialog.ConfirmationDialog;
import com.susoft.productmanager.ui.dialog.CreateCategoryDialog;
import com.susoft.productmanager.util.ImageHelper;
import com.susoft.productmanager.util.InputUtils;
import com.susoft.productmanager.util.TextUtils;
import com.susoft.productmanager.util.ToastUtil;
import com.susoft.productmanager.util.ViewUtil;
import com.susoft.productmanager.viewmodel.CreateProductViewModel;
import com.theartofdev.edmodo.cropper.CropImage;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.susoft.productmanager.data.network.NetworkConstants.REQUEST_TIMEOUT_IN_MILLISECONDS;
import static com.susoft.productmanager.util.InputUtils.setText;
import static com.susoft.productmanager.util.ToastUtil.longError;
import static com.susoft.productmanager.util.ToastUtil.shortError;
import static com.susoft.productmanager.util.ToastUtil.shortSuccess;

/**
 * @author Muhammed Sbary
 */
public class CreateProductFragment extends BaseFragment
        implements OptionsAdapter.OnOptionSelectedListener,
        CategoriesSpinnerAdapter.OnCategorySelectedListener,
        CreateProductListener,
        CreateCategoryDialog.OnCategoryDataChangedListener {

    private FragmentCreateProductBinding binding;
    private TextInputLayout[] textInputLayouts;

    private ProductCursorAdapter searchAdapter;
    private CategoriesSpinnerAdapter categoriesSpinnerAdapter1;
    private CategoriesSpinnerAdapter categoriesSpinnerAdapter2;

    private CreateCategoryDialog createCategoryDialog;

    private CreateProductViewModel viewModel;

    private Context context;
    private OptionsAdapter optionsAdapter1;
    private OptionsAdapter optionsAdapter2;
    private ConfirmationDialog confirmationDialog;

    private Handler loadingHandler = new Handler();
    private Runnable loadingRunnable = () -> longError("Request timeout!, press refresh to retry");
    private ProductTypeAdapter productTypesAdapter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initViewModel();
    }

    private void initViewModel() {
        viewModel = ViewModelProviders.of(this, ProductApp.getViewModelFactory()).get(CreateProductViewModel.class);
        viewModel.acceptListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initDataBinding(inflater, container);
        return binding.getRoot();
    }

    private void initDataBinding(@NonNull LayoutInflater inflater, @Nullable ViewGroup container) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_product, container, false);
        binding.setHandler(this);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.create_product_menu, menu);

        MenuItem searchMenuItem = menu.findItem(R.id.search_action);
        setupSearchView(searchMenuItem);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (confirmationDialog != null)
            confirmationDialog.dismiss();
        if (createCategoryDialog != null)
            createCategoryDialog.dismiss();
        removeTimeoutRunnable();
    }

    private void setupSearchView(MenuItem searchMenuItem) {
        SearchView searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchAdapter = new ProductCursorAdapter(context, Collections.emptyList());
        searchView.setSuggestionsAdapter(searchAdapter);
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                onProductNameSelected(searchAdapter.getProduct(position));
                searchMenuItem.collapseActionView();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                hideKeyboard();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                viewModel.onSearchQueryChanged(newText);
                return false;
            }
        });

        AutoCompleteTextView searchAutoCompleteTextView = searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchAutoCompleteTextView.setThreshold(1);
        searchAutoCompleteTextView.setPadding(0, 0, 0, 0);
        searchAutoCompleteTextView.invalidate();

        searchView.setOnKeyListener((v, keyCode, event) ->
        {
            if (keyCode == KeyEvent.KEYCODE_BACK && isKeyboardVisible())
                return onBackPressed();
            return false;

        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh_action:
                updateData();
                return true;
            case R.id.search_action:
                viewModel.updateSearchProducts();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private static void hideSpinnerDropDown(Spinner spinner) {
        try {
            Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
            method.setAccessible(true);
            method.invoke(spinner);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createCategoryForLevel(Category category, int level) {

        if (createCategoryDialog == null)
            createCategoryDialog = new CreateCategoryDialog(context, viewModel, this);

        if (!createCategoryDialog.isShowing())
            createCategoryDialog.show(category, level);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeViews();
    }

    private void initializeViews() {
        updateData();
        setupOutsideViewsClickListener();
        createInputLayoutsList();
        initCategoryOptions();
        initSpinners();
        setErrorCanceller(textInputLayouts);
        subscribeToNameSearch();
        observeOnCategories();
        observeOnTypes();
        listenForLoadBarcodeClicks();
        listenForEnterButtonPress();
        setTakeawayFieldsVisibility();
    }

    private void setTakeawayFieldsVisibility() {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(binding.scrollViewContainer);
        if (viewModel.isRestaurant()) {
            constraintSet.connect(binding.abcCode.getId(), ConstraintSet.TOP, binding.barrier3.getId(), ConstraintSet.BOTTOM, ViewUtil.toPx(16));
            constraintSet.connect(binding.processLocation.getId(), ConstraintSet.TOP, binding.barrier3.getId(), ConstraintSet.BOTTOM, ViewUtil.toPx(16));
            constraintSet.applyTo(binding.scrollViewContainer);
            binding.takeawayPrice.setVisibility(View.VISIBLE);
            binding.takeawayVat.setVisibility(View.VISIBLE);
        } else {
            constraintSet.connect(binding.abcCode.getId(), ConstraintSet.TOP, binding.barrier2.getId(), ConstraintSet.BOTTOM, ViewUtil.toPx(16));
            constraintSet.connect(binding.processLocation.getId(), ConstraintSet.TOP, binding.barrier2.getId(), ConstraintSet.BOTTOM, ViewUtil.toPx(16));
            constraintSet.applyTo(binding.scrollViewContainer);
            binding.takeawayPrice.setVisibility(View.GONE);
            binding.takeawayVat.setVisibility(View.GONE);
        }
    }

    private void observeOnTypes() {
        viewModel.getProductTypes().observe(this, this::setProductTypes);
    }

    private void setProductTypes(List<ProductType> productTypes) {
        productTypesAdapter.setItems(productTypes);
    }

    private void updateData() {
        startLoading();
        viewModel.updateData(new BaseFeedbackListener() {
            @Override
            public void onSuccess(String message) {
                stopLoading();
                setupViewsValues();
            }

            @Override
            public void onFail(String message) {
                stopLoading();
                ToastUtil.shortError(message);
            }
        });
    }

    private void listenForEnterButtonPress() {
        binding.productBarcodeEditText.setOnEditorActionListener((v, actionId, event) -> {

            // If triggered by an enter key, this is the event; otherwise, this is null.
            if (event != null) {
                // if shift key is down, then we want to insert the '\n' char in the TextView;
                // otherwise, the default action is to send the message.
                if (!event.isShiftPressed()) {
                    onEnterPressed();
                    return true;
                }
                return false;
            } else if (actionId == EditorInfo.IME_ACTION_GO) {
                onEnterPressed();
                return true;
            }

            return true;
        });
    }

    private void onEnterPressed() {
        this.loadProductByBarcode(false);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void listenForLoadBarcodeClicks() {
        binding.productBarcodeEditText.setOnTouchListener((view, event) -> {
            final int DRAWABLE_RIGHT = 2;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (binding.productBarcodeEditText.getRight() - binding.productBarcodeEditText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    loadProductByBarcode(true);
                    return true;
                }
            }
            return false;
        });
    }

    private void loadProductByBarcode(boolean showError) {
        hideKeyboardAndClearFocus();
        if (viewModel.isValidBarcode()) {
            startLoading();
            viewModel.getProductByBarcode(new BaseFeedbackListener() {
                @Override
                public void onSuccess(String message) {
                    setupViewsValues();
                }

                @Override
                public void onFail(String message) {
                    if (showError)
                        ToastUtil.shortError(message);
                    stopLoading();
                }
            });
        }
    }

    private void initSpinners() {
        initFirstLevelSpinner();
        initSecondLevelSpinner();
        initProductTypesSpinner();
    }

    private void initProductTypesSpinner() {
        binding.productTypeSpinner.setOnSelectedListener(this::onProductTypeSelected);
        productTypesAdapter = new ProductTypeAdapter(context, new ArrayList<>());
        binding.productTypeSpinner.setAdapter(productTypesAdapter);
    }

    private void onProductTypeSelected(int position) {
        viewModel.onProductTypeSelected(position);
    }

    private void initFirstLevelSpinner() {
        categoriesSpinnerAdapter1 = new CategoriesSpinnerAdapter(context, new ArrayList<>(), Category.noCategory());
        binding.category1Spinner.setAdapter(categoriesSpinnerAdapter1);
    }

    private void initSecondLevelSpinner() {
        categoriesSpinnerAdapter2 = new CategoriesSpinnerAdapter(context, new ArrayList<>(), Category.noCategory());
        binding.category2Spinner.setAdapter(categoriesSpinnerAdapter2);
    }

    private void observeOnCategories() {
        viewModel.getFirstLevelCategories().observe(this, this::setFirstLevelCategories);
    }

    private void setFirstLevelCategories(List<Category> categories) {
        categoriesSpinnerAdapter1.setItems(categories);
        binding.category1Spinner.setSelection(viewModel.getCategoryPosition(1));
        //If we updated categories data we should recall onCategorySelected to make sure it has
        //the right value
        onCategorySelected(1, viewModel.getCategoryPosition(1));
    }

    private void setSecondLevelCategories(List<Category> categories) {
        categoriesSpinnerAdapter2.setItems(categories);
        binding.category2Spinner.setSelection(viewModel.getCategoryPosition(2));
        //If we updated categories data we should recall onCategorySelected to make sure it has
        //the right value
        onCategorySelected(2, viewModel.getCategoryPosition(2));
    }

    private void onProductNameSelected(Product product) {
        LiveData<Product> productLiveData = viewModel.onProductSelected(product);
        productLiveData.observe(this, new Observer<Product>() {
            @Override
            public void onChanged(Product product) {
                setupViewsValues();
                productLiveData.removeObserver(this);
            }
        });
        startLoading();
    }

    private void subscribeToNameSearch() {
        viewModel.getNameSearchProducts().observe(this, this::setProductsAdapter);
    }

    private void setProductsAdapter(List<Product> products) {
        if (searchAdapter != null)
            searchAdapter.setProducts(products);
    }

    private void setupViewsValues() {
        stopLoading();

        if (viewModel.isInEditMode()) {
            setupForEditMode();
            setupWithProduct(viewModel.getProduct());
        } else {
            setupForNormalMode();
            setupWithProduct(viewModel.getSavedState());
        }
        clearInputLayoutsErrors();
    }

    private void setupForEditMode() {
        binding.create.setText(getString(R.string.save_button_label));
        binding.productBarcode.setEnabled(false);
        binding.productMisc.setEnabled(false);
    }

    private void setupForNormalMode() {
        binding.create.setText(getString(R.string.create_button_label));
        binding.productBarcode.setEnabled(true);
        binding.productImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_image));
        binding.productMisc.setEnabled(true);
    }

    private void setupWithProduct(Product product) {

        binding.isActive.setChecked(product.isActive());
        if (product.getImage() != null)
            binding.productImage.setImageBitmap(ImageHelper.getImageFromBytes(product.getImage()));
        else
            binding.productImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_image));

        setText(binding.productName, product.getName());
        setText(binding.productBarcode, product.getBarcode());
        setText(binding.retailPrice, product.getRetailPrice());
        setText(binding.takeawayPrice, product.getTakeawayPrice());
        setText(binding.takeawayVat, product.getTakeawayVat());
        setText(binding.vat, product.getVat());
        setText(binding.description, product.getDescription());
        setText(binding.abcCode, product.getAbcCode());
        setText(binding.processLocation, product.getProcessLocation());

        binding.category1Spinner.setSelection(viewModel.getCategoryPosition(1));
        new Handler().post(() -> binding.category2Spinner.setSelection(viewModel.getCategoryPosition(2)));

        binding.productTypeSpinner.setSelection(viewModel.getProductTypeIndex(product.getType()));

        binding.isActive.setChecked(product.isActive());

        binding.productMisc.setChecked(product.isMiscellaneous());
    }

    private void resetButtonsAndSpinners() {
        binding.isActive.setChecked(true);
        onIsActiveChanged(true);
    }

    private void initCategoryOptions() {
        optionsAdapter1 = new OptionsAdapter(context, R.array.category_options, R.layout.itemview_selectable_text, 1, this);
        optionsAdapter2 = new OptionsAdapter(context, R.array.category_options, R.layout.itemview_selectable_text, 2, this);
        binding.categoryOptions1.setAdapter(optionsAdapter1);
        binding.categoryOptions2.setAdapter(optionsAdapter2);
    }

    @Override
    public void onCategorySelected(int level, int position) {

        boolean isValidCategory = position != 0;

        if (level == 1) {

            Category category1 = categoriesSpinnerAdapter1.getItem(position);

            binding.productCategoryLayout2.setEnabled(isValidCategory);
            binding.categoryOptions2.setEnabled(isValidCategory);

            binding.category2Indicator.setAlpha(isValidCategory ? 1.0f : 0.65f);

            if (category1 != null) {

                viewModel.setCategory1(category1);

                if (!category1.getServerId().equals(viewModel.getCategory(1).getServerId()))
                    viewModel.setCategory2(Category.noCategory());

                if (isValidCategory) {
                    enableCategoryModification(optionsAdapter1);
                    setSecondLevelCategories(category1.getChildren());
                } else {
                    disableCategoryModification(optionsAdapter1);
                    setSecondLevelCategories(new ArrayList<>());
                }

            } else
                disableCategoryModification(optionsAdapter1);
        } else {

            if (isValidCategory)
                enableCategoryModification(optionsAdapter2);
            else
                disableCategoryModification(optionsAdapter2);

            viewModel.setCategory2(categoriesSpinnerAdapter2.getItem(position));
        }
    }

    @Override
    public void onCategoryOptionsSelected(int position, int level) {

        hideSpinnerDropDown(binding.categoryOptions1);
        hideSpinnerDropDown(binding.categoryOptions2);

        final int ADD = 0;
        final int UPDATE = 1;
        final int DELETE = 2;

        switch (position) {
            case ADD:
                createCategoryForLevel(null, level);
                break;
            case UPDATE:
                createCategoryForLevel(viewModel.getCategory(level), level);
                break;
            case DELETE:
                openDeleteDialog(level);
                break;
        }

    }

    private void openDeleteDialog(int level) {
        confirmationDialog = new ConfirmationDialog(context,
                () -> viewModel.onCategoryDeleteClicked(level, new BaseFeedbackListener() {
                    @Override
                    public void onSuccess(String message) {
                        shortSuccess(message);
                        updateData();
                    }

                    @Override
                    public void onFail(String message) {
                        shortError(message);
                    }
                }));

        if (!confirmationDialog.isShowing())
            confirmationDialog.show(getString(R.string.delete_message,
                    viewModel.getCategory(level).getValue(),
                    getString(R.string.category)));

    }

    private void enableCategoryModification(OptionsAdapter adapter) {
        adapter.setEnabled(1, true);
        adapter.setEnabled(2, true);
    }

    private void disableCategoryModification(OptionsAdapter adapter) {
        adapter.setEnabled(1, false);
        adapter.setEnabled(2, false);
    }

    private void createInputLayoutsList() {
        textInputLayouts = new TextInputLayout[]{binding.productName,
                binding.productBarcode,
                binding.retailPrice,
                binding.takeawayPrice,
                binding.takeawayVat,
                binding.vat,
                binding.description,
                binding.abcCode,
                binding.processLocation};
    }

    private void setErrorCanceller(TextInputLayout[] inputLayouts) {
        if (inputLayouts != null)
            for (TextInputLayout inputLayout : inputLayouts)
                InputUtils.setErrorCanceller(inputLayout);
    }

    public void onClearClicked() {
        clearInputLayoutsText();
        resetButtonsAndSpinners();
        viewModel.onClearClicked();
        setupViewsValues();
    }

    private void clearInputLayoutsText() {
        for (TextInputLayout view : textInputLayouts)
            InputUtils.clearText(view);
    }

    private void clearInputLayoutsErrors() {
        for (TextInputLayout view : textInputLayouts)
            InputUtils.clearError(view);
    }

    public void onDescriptionTextChanged(Editable description) {
        viewModel.setDescription(TextUtils.getText(description));
    }

    public void onAbcCodeChanged(Editable abcCode) {
        viewModel.setAbcCode(TextUtils.getText(abcCode));
    }

    public void onProcessLocationChanged(Editable processLocation) {
        viewModel.setProcessLocation(TextUtils.getText(processLocation));
    }

    public void onNameChanged(Editable name) {
        viewModel.setName(TextUtils.getText(name));
    }

    public void onBarcodeChanged(Editable barcode) {
        String barcodeString = TextUtils.getText(barcode);
        viewModel.setBarcode(barcodeString);
        viewModel.isBarcodeAvailable(barcodeString, new BaseFeedbackListener() {
            @Override
            public void onSuccess(String message) {
            }

            @Override
            public void onFail(String message) {
                binding.productBarcode.setError(message);
            }
        });
    }

    public void onSelectImageClicked() {
        CropImage.activity()
                .setAspectRatio(1, 1)
                .start(context, this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK && result != null) {
                binding.productImage.setImageURI(result.getUri());
                viewModel.onProductImageSelected(((BitmapDrawable) binding.productImage.getDrawable()).getBitmap());
            }
        }
    }

    public void onIsActiveChanged(boolean isActive) {
        viewModel.setActive(isActive);
    }

    public void onRetailPriceChanged(Editable retailPrice) {
        viewModel.setRetailPrice(TextUtils.getText(retailPrice));
    }

    public void onVatChanged(Editable vat) {
        viewModel.setVat(TextUtils.getText(vat));
    }

    public void onTakeawayPriceChanged(Editable takeawayPrice) {
        viewModel.setTakeawayPrice(TextUtils.getText(takeawayPrice));
    }

    public void onTakeawayVatChanged(Editable takeawayVat) {
        viewModel.setTakeawayVat(TextUtils.getText(takeawayVat));
    }

    public void onMiscellaneousChanged(boolean isChecked) {
        viewModel.onMiscellaneousChanged(isChecked);
    }

    public void onCreateClicked() {
        startLoading();
        viewModel.createProduct();
    }

    private void startLoading() {
        binding.loadingScreen.setVisibility(View.VISIBLE);
        loadingHandler.postDelayed(loadingRunnable, REQUEST_TIMEOUT_IN_MILLISECONDS);
    }

    private void stopLoading() {
        binding.loadingScreen.setVisibility(View.GONE);
        removeTimeoutRunnable();
    }

    private void removeTimeoutRunnable() {
        loadingHandler.removeCallbacks(loadingRunnable);
    }

    @Override
    protected boolean onBackPressed() {
        if (isKeyboardVisible()) {
            hideKeyboard();
            return true;
        }
        return super.onBackPressed();
    }

    private void setupOutsideViewsClickListener() {
        binding.getRoot().setOnClickListener(v -> hideKeyboardAndClearFocus());
        binding.scrollView.getChildAt(0).setOnClickListener(v -> hideKeyboardAndClearFocus());
    }

    private void hideKeyboardAndClearFocus() {
        super.hideKeyboard();
        if (getActivity() != null && getActivity().getCurrentFocus() != null)
            getActivity().getCurrentFocus().clearFocus();
    }

    @Override
    public void onAbcCodeError(String message) {
        binding.abcCode.setError(message);
    }

    @Override
    public void onProcessLocationError(String message) {
        binding.processLocation.setError(message);
    }

    @Override
    public void onNameError(String message) {
        binding.productName.setError(message);
    }

    @Override
    public void onBarcodeError(String message) {
        binding.productBarcode.setError(message);
    }

    @Override
    public void onTakeawayPriceError(String message) {
        binding.takeawayPrice.setError(message);
    }

    @Override
    public void onTakeawayVatError(String message) {
        binding.takeawayVat.setError(message);
    }

    @Override
    public void onVatError(String message) {
        binding.vat.setError(message);
    }

    @Override
    public void onRetailPriceError(String message) {
        binding.retailPrice.setError(message);
    }

    @Override
    public void onSuccess(String message) {
        onClearClicked();
        shortSuccess(message);
        stopLoading();
    }

    @Override
    public void onFallBack() {
        stopLoading();
    }

    @Override
    public void onFail(String message) {
        shortError(message);
        stopLoading();
    }

    @Override
    public void onCategoryDataChanged() {
        updateData();
    }
}