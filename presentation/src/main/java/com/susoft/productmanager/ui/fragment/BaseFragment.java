package com.susoft.productmanager.ui.fragment;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.susoft.productmanager.util.InputUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

public class BaseFragment extends Fragment {

    private boolean isKeyboardVisible;
    private InputMethodManager inputMethodManager;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        FragmentActivity activity = getActivity();
        if (activity != null)
            this.inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect rect = new Rect();
            view.getWindowVisibleDisplayFrame(rect);
            int screenHeight = view.getRootView().getHeight();

            // rect.bottom is the position above soft keypad or device button.
            // if keypad is shown, the rect.bottom is smaller than that before.
            int keypadHeight = screenHeight - rect.bottom;

            // 0.15 ratio is perhaps enough to determine keypad height.
            isKeyboardVisible = keypadHeight > screenHeight * 0.15;
        });
        view.getRootView().setOnKeyListener((v, keyCode, event) ->
        {
            if (keyCode == KeyEvent.KEYCODE_BACK && isKeyboardVisible && inputMethodManager != null)
                return onBackPressed();
            return false;

        });
    }

    protected boolean onBackPressed() {
        Log.i("BaseFragment", "On back button pressed");
        return false;
    }

    boolean isKeyboardVisible() {
        return isKeyboardVisible;
    }

    void hideKeyboard() {
        if (getActivity() != null)
            InputUtils.hideSoftKeyboard(getActivity());
    }

}
