package com.susoft.productmanager.ui.adapter.dropdown;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class OptionsAdapter extends ArrayAdapter<String> {

    private SparseBooleanArray enabledMap = new SparseBooleanArray();
    private int level;
    private OnOptionSelectedListener listener;

    public OptionsAdapter(@NonNull Context context, int resource, int textViewResourceId, int level, OnOptionSelectedListener listener) {
        super(context, textViewResourceId, 0, Arrays.asList(context.getResources().getStringArray(resource)));
        this.level = level;
        this.listener = listener;

        for (int i = 0; i < context.getResources().getStringArray(resource).length; i++)
            enabledMap.put(i, true);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setText("");
        view.setVisibility(View.GONE);
        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView textView = (TextView) super.getDropDownView(position, convertView, parent);
        textView.setEnabled(isEnabled(position));
        if (!isEnabled(position))
            textView.setOnClickListener(null);
        else
            textView.setOnClickListener(v -> listener.onCategoryOptionsSelected(position, level));
        return textView;
    }

    @Override
    public boolean isEnabled(int position) {
        return enabledMap.get(position);
    }

    public void setEnabled(int position, boolean isEnabled) {
        enabledMap.put(position, isEnabled);
        notifyDataSetChanged();
    }

    public interface OnOptionSelectedListener {
        void onCategoryOptionsSelected(int position, int level);
    }
}
