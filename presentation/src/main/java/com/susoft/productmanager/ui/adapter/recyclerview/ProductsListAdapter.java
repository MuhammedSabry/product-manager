package com.susoft.productmanager.ui.adapter.recyclerview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.susoft.productmanager.R;
import com.susoft.productmanager.databinding.ItemviewProductsListBinding;
import com.susoft.productmanager.domain.model.Product;
import com.susoft.productmanager.ui.custom.ProductDragShadow;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author Muhammed Sabry
 */
public class ProductsListAdapter extends RecyclerView.Adapter<ProductsListAdapter.ProductsListViewHolder> {
    private List<Product> products;
    private Context context;

    public ProductsListAdapter(Context context, List<Product> products) {
        this.products = products;
        this.context = context;
    }

    @NonNull
    @Override
    public ProductsListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemviewProductsListBinding dataBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.itemview_products_list, parent, false);
        return new ProductsListViewHolder(dataBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsListViewHolder holder, int position) {
        holder.bind(products.get(position));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void setProducts(List<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    public class ProductsListViewHolder extends RecyclerView.ViewHolder {
        private ItemviewProductsListBinding binding;
        private Product product;

        @SuppressLint("ClickableViewAccessibility")
        ProductsListViewHolder(@NonNull ItemviewProductsListBinding productsListItemviewBinding) {
            super(productsListItemviewBinding.getRoot());
            this.binding = productsListItemviewBinding;
            this.binding.setHandler(this);
            this.binding.getRoot().setOnLongClickListener(v ->
                    {
                        onDragClick();
                        return true;
                    }
            );
        }

        void onDragClick() {
            View rootView = binding.getRoot();
            rootView.setBackgroundResource(R.color.notesColor);
            rootView.setOnDragListener((v, event) ->
            {
                if (event.getAction() == DragEvent.ACTION_DRAG_ENDED) {
                    rootView.setBackground(null);
                    rootView.setOnDragListener(null);
                    return true;
                }
                return false;
            });
            rootView.startDrag(null,
                    new ProductDragShadow(rootView),
                    product,
                    1);
        }

        void bind(Product product) {
            this.product = product;
            binding.productBarcode.setText(product.getBarcode());
            binding.productName.setText(product.getName());

            if (product.hasImage())
                binding.imageIndicator.setVisibility(View.VISIBLE);
            else
                binding.imageIndicator.setVisibility(View.INVISIBLE);
        }
    }
}
