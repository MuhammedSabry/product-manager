package com.susoft.productmanager.ui.dialog;

import android.content.Context;

import com.susoft.productmanager.R;
import com.susoft.productmanager.databinding.DialogConfirmationBinding;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

public class ConfirmationDialog extends AlertDialog {

    private final OnConfirmListener listener;
    private DialogConfirmationBinding binding;
    private String message;

    public ConfirmationDialog(@NonNull Context context, OnConfirmListener listener) {
        super(context);
        this.listener = listener;

        initDataBinding();
        setupDialog();
        initViews();
    }

    private void setupDialog() {
        setView(binding.getRoot());
        setCancelable(false);
    }

    private void initDataBinding() {
        binding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.dialog_confirmation,
                null,
                false);
        binding.setHandler(this);
    }

    private void initViews() {
        binding.dialogMessage.setText(message);
    }

    public void onCancelClicked() {
        dismiss();
    }

    public void onConfirmClicked() {
        listener.onConfirm();
        dismiss();
    }

    @Override
    public void show() {
    }

    public void show(String message) {
        super.show();
        this.message = message;
        initViews();
    }

    public interface OnConfirmListener {
        void onConfirm();
    }
}
