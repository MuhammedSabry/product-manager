package com.susoft.productmanager.ui.adapter;

import com.susoft.productmanager.ui.fragment.CreateProductFragment;
import com.susoft.productmanager.ui.fragment.QuickMenuFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * @author Muhammed Sabry
 */
public class MainPagerAdapter extends FragmentPagerAdapter {
    private QuickMenuFragment quickMenuFragment;
    private CreateProductFragment createProductFragment;

    public MainPagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
        quickMenuFragment = new QuickMenuFragment();
        createProductFragment = new CreateProductFragment();
    }

    @Override
    public int getCount() {
        return 2;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        if (position == 0)
            return createProductFragment;
        else
            return quickMenuFragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0)
            return "Create Product";
        else
            return "Quick Menus";
    }
}
