package com.susoft.productmanager.ui.adapter.recyclerview;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.susoft.productmanager.R;
import com.susoft.productmanager.databinding.ItemviewMenuCellBinding;
import com.susoft.productmanager.domain.model.Cell;
import com.susoft.productmanager.domain.model.Grid;
import com.susoft.productmanager.domain.model.Product;
import com.susoft.productmanager.util.ImageHelper;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author Muhammed Sabry
 */
public class CellListAdapter extends RecyclerView.Adapter<CellListAdapter.MenuViewHolder> {

    private SparseArray<Cell> cellMap = new SparseArray<>();

    private Context context;
    private CellListener cellListener;
    private Grid grid;

    public CellListAdapter(Grid grid, List<Cell> cells, Context context, CellListener cellListener) {
        setCells(grid, cells);
        this.context = context;
        this.cellListener = cellListener;
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemviewMenuCellBinding dataBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.itemview_menu_cell, parent, false);
        return new MenuViewHolder(dataBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuViewHolder holder, int position) {
        holder.bind(cellMap.get(position + 1));
    }

    @Override
    public int getItemCount() {
        return grid.getColumns() * grid.getRows();
    }

    public void setCells(Grid grid, List<Cell> cells) {
        this.grid = grid;

        cellMap.clear();

        for (Cell cell : cells)
            cellMap.put(cell.getPosition(), cell);

        notifyDataSetChanged();
    }

    public interface CellListener {
        void onCellLongClicked(int position, Cell cell);

        void onProductDraggedOverCell(int position, Product product);

        void onCellClicked(Cell cell);
    }

    class MenuViewHolder extends RecyclerView.ViewHolder implements View.OnDragListener {

        private ItemviewMenuCellBinding itemViewBinding;
        private boolean hasProductOrSubMenu;

        MenuViewHolder(@NonNull ItemviewMenuCellBinding binding) {
            super(binding.root);
            this.itemViewBinding = binding;
        }

        void bind(@Nullable Cell cell) {

            //Default setup for the views
            itemViewBinding.root.setOnClickListener(null);
            itemViewBinding.root.setOnDragListener(this);
            itemViewBinding.root.setOnLongClickListener(v -> {
                cellListener.onCellLongClicked(getIndex(), cell);
                return true;
            });

            itemViewBinding.title.setVisibility(View.INVISIBLE);
            itemViewBinding.title.setBackgroundResource(R.color.greyed_out);

            itemViewBinding.title.setTextSize(14);
            itemViewBinding.title.setMaxLines(4);
            itemViewBinding.image.setImageDrawable(null);

            ConstraintSet set = new ConstraintSet();
            set.clone(itemViewBinding.root);
            set.connect(itemViewBinding.title.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
            set.constrainHeight(itemViewBinding.title.getId(), ConstraintSet.MATCH_CONSTRAINT);
            set.applyTo(itemViewBinding.root);

            boolean isVirtual = cell == null;

            boolean hasProduct = !isVirtual && !TextUtils.isEmpty(cell.getProductId());

            boolean hasSubMenu = !hasProduct && !isVirtual;

            hasProductOrSubMenu = hasSubMenu || hasProduct;

            if (hasProductOrSubMenu) {

                byte[] image = cell.getImage();

                if (image != null) {
                    itemViewBinding.image.setImageBitmap(ImageHelper.getImageFromBytes(image));

                    set.clear(itemViewBinding.title.getId(), ConstraintSet.TOP);
                    set.constrainHeight(itemViewBinding.title.getId(), ConstraintSet.WRAP_CONTENT);
                    itemViewBinding.title.setTextSize(12);
                    itemViewBinding.title.setMaxLines(2);
                    set.applyTo(itemViewBinding.root);
                }

                itemViewBinding.title.setText(cell.getText());

                if (hasSubMenu) {
                    itemViewBinding.title.setBackgroundResource(R.color.sub_menu_color);
                    itemViewBinding.root.setOnClickListener(v -> cellListener.onCellClicked(cell));
                }

                itemViewBinding.title.setVisibility(View.VISIBLE);
            }
        }

        private int getIndex() {
            return getAdapterPosition() + 1;
        }

        @Override
        public boolean onDrag(View view, DragEvent event) {
            final int eventAction = event.getAction();
            View dragForeground = itemViewBinding.dragForeground;
            switch (eventAction) {
                case DragEvent.ACTION_DRAG_STARTED:

                    // As an example of what your application might do,
                    // applies a blue color tint to the View to indicate that it can accept
                    // data.
                    dragForeground.setVisibility(View.VISIBLE);

                    // returns true to indicate that the View can accept the dragged data.
                    return true;

                case DragEvent.ACTION_DRAG_ENTERED:

                    if (!hasProductOrSubMenu)
                        // Applies a green tint to the View. Return true; the return value is ignored.
                        dragForeground.setVisibility(View.INVISIBLE);

                    return true;

                case DragEvent.ACTION_DRAG_LOCATION:
                    // Ignore the event
                    return true;

                case DragEvent.ACTION_DRAG_EXITED:
                    // Re-sets the color tint to blue. Returns true; the return value is ignored.
                    dragForeground.setVisibility(View.VISIBLE);

                    return true;
                case DragEvent.ACTION_DROP:

                    if (!hasProductOrSubMenu) {
                        // Gets the item containing the dragged data
                        Product product = (Product) event.getLocalState();
                        updateCellProduct(product);
                    }

                    //Turns off any color tints
                    dragForeground.setVisibility(View.INVISIBLE);

                    // Returns true. DragEvent.getResult() will return true.
                    return true;

                case DragEvent.ACTION_DRAG_ENDED:

                    dragForeground.setVisibility(View.INVISIBLE);
                    return true;
            }

            return false;
        }

        private void updateCellProduct(Product product) {
            cellListener.onProductDraggedOverCell(getIndex(), product);
        }
    }
}
