package com.susoft.productmanager.ui.adapter.dropdown;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.susoft.productmanager.R;
import com.susoft.productmanager.domain.model.Category;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @author Muhammed Sabry
 */
public class CategoriesSpinnerAdapter extends ArrayAdapter<Category> {

    private List<Category> categories;
    private Category defaultCategory;

    public CategoriesSpinnerAdapter(@NonNull Context context, @NonNull List<Category> categories, Category defaultCategory) {
        super(context, R.layout.itemview_spinner_normal, categories);
        this.categories = categories;
        this.defaultCategory = defaultCategory;
        setItems(new ArrayList<>(categories));
        setDropDownViewResource(R.layout.itemview_spinner_dropdown);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        if (position >= 0 && position < categories.size())
            view.setText(categories.get(position).getValue());
        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);

        if (position >= 0 && position < categories.size()) {
            view.setText(categories.get(position).getValue());
        }

        return view;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    public void setItems(List<Category> categories) {
        this.categories.clear();
        this.categories.add(defaultCategory);
        this.categories.addAll(categories);
        notifyDataSetChanged();
    }

    public interface OnCategorySelectedListener {
        void onCategorySelected(int level, int position);
    }
}
